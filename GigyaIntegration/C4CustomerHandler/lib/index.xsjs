$.response.contentType = "application/json";
$.response.status = 200;

function fnMapCommerceToService(sMessage){
	var CommerceCust= sMessage;
	
	var customer	= {
			"RoleCode":"CRM000",					
			"LifeCycleStatusCode":"2",
			"FirstName":"",			
			"LastName":"",											
			"LanguageCode":"DE",			
			"CountryCode":"DE",								
			"HouseNumber":"",									
			"Street":"",
			"City": "",
			"StreetPostalCode":"",
			"TimeZoneCode":"CET",
			"Phone":"",
			"Email":"",
			"IBAN":"",
			"IndividualCustomerSalesData":[{
				"SalesOrganisationID":"SO_1000",
				"DistributionChannelCode":"01",
				"DivisionCode":"01",
				"CurrencyCode":"EUR",
				"PaymentTermsCode":"1001"
				}]
			};
	customer.Email=CommerceCust.contactEmail;
	customer.LastName=CommerceCust.name;
	
	 if(CommerceCust.defaultPaymentAddress){
	 	address=CommerceCust.defaultPaymentAddress;
	 	
	 	customer.FirstName = address.firstname;
	 	customer.MiddleName=address.middlename
	 	customer.LastName = address.lastname;
	 	customer.Street = address.streetname;
	 	customer.HouseNumber = address.streetnumber;
	 	customer.City = address.town;
	 	customer.StreetPostalCode=address.postalcode;
	 	customer.Phone = address.phone1;
	 	customer.IBAN = address.IBAN;
	 }
	return customer;
}

function getCSRFToken(headers){
	var token = "fetch";
	
	for (i = 0; i < headers.length; i++) {
	
		if(headers[i].name==="x-csrf-token"){
			token=headers[i].value;
		}
	}
	return token;
}

function fnCreateCustomer(sMessage){
	
	var destination;
	var client;
	var request;
	var post;
	
	try{
		//console.log("Try logon");
    	//console.log(destination);
    	//Creating HTTP Client
    	client = new $.net.http.Client();
    	//Creating Request
    	request = new $.web.WebRequest($.net.http.GET,"IndividualCustomerCollection?$format=json");
    	request.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    	request.headers.set("Accept", "application/json");
    	request.headers.set("content-type", "application/json");
		request.headers.set("X-CSRF-Token", "fetch");
    	client.request(request,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");
    
    	var token = getCSRFToken(client.getResponse().headers);
    	//console.log("Token:" + token);
		if(sMessage && sMessage.contactEmail){
			var oCustomer = fnMapCommerceToService(sMessage);
		
			post = new $.web.WebRequest($.net.http.POST,"IndividualCustomerCollection");
			post.cookies=client.getResponse().cookies;
    		post.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    		post.headers.set("Accept", "application/json");
    		post.headers.set("content-type", "application/json");
			post.headers.set("X-CSRF-Token", token);
			post.setBody(JSON.stringify(oCustomer));
			//console.log(post.headers);
    		client.request(post,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");
    		
    		var sData = client.getResponse().body.asString();
			//console.log(sData);
			var oJson = JSON.parse(sData);
    		var customer = oJson.d.results;
    		var customerID = customer.ObjectID;
    		console.log("Customer "+customerID+" created");
    		
	
			// close the connection
			client.close();
    		return customerID;
		} else{
        		$.response.status = 500;
        		$.response.setBody(JSON.stringify({
            		"Error":"No customer payload found"
            	}));	
        	}
		
	}
	catch(errorObj){
		console.log(errorObj.message);
		$.response.setBody(JSON.stringify({
			ERROR : errorObj.message
		}));
		
	}
	
	return customerID;
}

function fnUpdateCustomer(sMessage){
	
	var destination;
	var client;
	var request;
	var post;
	
	try{

		var id=sMessage.customerID;
		var iban=sMessage.SEPA;

    	//Creating HTTP Client
    	client = new $.net.http.Client();
    	//Creating Request
    	request = new $.web.WebRequest($.net.http.GET,"IndividualCustomerCollection('"+id+"')?$format=json");
    	request.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    	request.headers.set("Accept", "application/json");
    	request.headers.set("content-type", "application/json");
		request.headers.set("X-CSRF-Token", "fetch");
    	client.request(request,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");
    
    	var token = getCSRFToken(client.getResponse().headers);
    	//console.log("Token:" + token);
    	
    	var sData = client.getResponse().body.asString();
    	
    	
		var oJson = JSON.parse(sData);
    	var oCustomer = oJson.d.results;
		oCustomer.IBAN = iban;
		var customerID = oCustomer.ObjectID
    	
		
		post = new $.web.WebRequest($.net.http.PUT,"IndividualCustomerCollection('"+id+"')");
		post.cookies=client.getResponse().cookies;
    	post.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    	post.headers.set("Accept", "application/json");
    	post.headers.set("content-type", "application/json");
		post.headers.set("X-CSRF-Token", token);
		post.setBody(JSON.stringify(oCustomer));
		//console.log(post.headers);
    	client.request(post,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");
    	
    	/*var sData2 = client.getResponse().body.asString();
    	
    	console.log(sData2);
		var oJson2 = JSON.parse(sData2);
    	var customer = oJson2.d.results;
    	var customerID = customer.ObjectID;
    	console.log("Customer "+customerID+" created");*/
    		console.log("Customer "+customerID+" created");
	
			// close the connection
			client.close();
    		return customerID;
		
		
	}
	catch(errorObj){
		console.log(errorObj.message);
		$.response.setBody(JSON.stringify({
			ERROR : errorObj.message
		}));
		
	}
	
	return customerID;
}

try {
    switch ( $.request.method ) {
        //Handle your GET calls here
        case $.net.http.GET:
            $.response.setBody(JSON.stringify({"Result":"Customer retrieved"}));
            break;
        //Handle your POST calls here
        case $.net.http.POST:
        	
        	var sMessage = $.request.body.asString();
			//console.log(sMessage);
			var oMessage = JSON.parse(sMessage);
        	
        	var id = fnCreateCustomer(oMessage);
        	if(id && id!=""){
        		$.response.setBody(JSON.stringify({
            		"Result":"Customer created",
            		"ID":id
            	}));
        	} 
            
            break; 
        case $.net.http.PUT:
        	
        	var sMessage = $.request.body.asString();
			//console.log(sMessage);
			var oMessage = JSON.parse(sMessage);
			
			if(oMessage.customerID && oMessage.SEPA){
				var id = fnUpdateCustomer(oMessage);
			}
        	if(id&&id!=""){
        		
        	$.response.setBody(JSON.stringify({
            		"Result":"Customer updated",
            		"ID":id
            	}));	
        	}
            
            break;
        default:
            break;
    }
} catch (err) {
    $.response.setBody("Failed to execute action: " + err.toString());
}