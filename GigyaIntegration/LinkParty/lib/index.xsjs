$.response.contentType = "application/json";
$.response.status = 200;

function getCSRFToken(headers){
	var token = "fetch";
	
	for (i = 0; i < headers.length; i++) {
	
		if(headers[i].name==="x-csrf-token"){
			token=headers[i].value;
		}
	}
	return token;
}

function fnUpdateRelationship(token, cookies, type, first, second, client){
	
	
	
	var rel = {
		"RelationshipType":type,
		"FirstBusinessPartnerID":second,
		"SecondBusinessPartnerID":first
	}
	
	var	post = new $.web.WebRequest($.net.http.POST,"BusinessPartnerRelationshipCollection");
	post.cookies=cookies;
    post.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    post.headers.set("Accept", "application/json");
    post.headers.set("content-type", "application/json");
	post.headers.set("X-CSRF-Token", token);
	post.setBody(JSON.stringify(rel));

	client.request(post,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");

	if(client.getResponse().status==201){
		return "Success";
	}else{
		return "Error";
	}
}

function fnAddContractParty(token, cookies, oData, client){

	var	post = new $.web.WebRequest($.net.http.POST,"ContractPartyCollection");
	post.cookies=cookies;
    post.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    post.headers.set("Accept", "application/json");
    post.headers.set("content-type", "application/json");
	post.headers.set("X-CSRF-Token", token);
	post.setBody(JSON.stringify(oData));

    client.request(post,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/contract");
    console.log("PARTY");
	console.log(client.getResponse());
	if(client.getResponse().status==201){
		return "Success";
	}else{
		return "Error";
	}
}

function fnAddContractItem(token, cookies, oData ,client){
	
	var item = {
		"ParentObjectID":oData,
		"ProcessingTypeCode":"SCIT",
		"DeterminationRelevanceCode":"3",
		"ValidityStartDate":"2018-12-01T00:00:00",
		"ValidityEndDate":"2099-11-30T00:00:00",
		"InvoiceScheduleStartDate":"2018-12-01T00:00:00",
		"InvoiceScheduleEndDate":"2099-11-30T00:00:00",
		"InvoiceScheduleInvoicingInAdvanceIndicator":false,
		"ProductID":"PLU_PAR",
		"Quantity":"1.00000000000000",
		"QuantityUnitCode":"EA",
		"CashDiscountTermsCode":"1001",
		"PriceDateTime":"2018-12-01T00:00:00",
		"Commerce":true
	}
	
	var	post = new $.web.WebRequest($.net.http.POST,"ContractItemCollection");
	post.cookies=cookies;
    post.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    post.headers.set("Accept", "application/json");
    post.headers.set("content-type", "application/json");
	post.headers.set("X-CSRF-Token", token);
	post.setBody(JSON.stringify(item));

    client.request(post,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/contract");
	console.log("ITEM");
	console.log(client.getResponse());
	if(client.getResponse().status==201){
		return "Success";
	}else{
		return "Error";
	}
}

try {
    switch ( $.request.method ) {
        //Handle your POST calls here
        case $.net.http.POST:
        	
        	var sMessage = $.request.body.asString();
			//console.log(sMessage);
			var oMessage = JSON.parse(sMessage);
			
			var client = new $.net.http.Client();
			
			request = new $.web.WebRequest($.net.http.GET,"IndividualCustomerCollection('"+oMessage.PartyID+"')");
    		request.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    		request.headers.set("Accept", "application/json");
    		request.headers.set("content-type", "application/json");
			request.headers.set("X-CSRF-Token", "fetch");
    		client.request(request,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");
    	
    		var token = getCSRFToken(client.getResponse().headers);
    		var cookies = client.getResponse().cookies;
    		
    		//extract ObjectID of main party and new party
    		var sData = client.getResponse().body.asString();
			var oJson = JSON.parse(sData);
    		var oNewParty = oJson.d.results;
    		var sNewPartyObjectID = oMessage.PartyID;
    		var sMainPartyObjectID = oMessage.customerID;
    		
    		//extract ObjectID of the contract
    		var sContractObjectID = oMessage.ParentObjectID;
    		
    		//Format oMessage to correct format for fnAddContractParty
    		oMessage.PartyID=oNewParty.CustomerID;
    		delete oMessage.customerID;
    	
        	var item = fnAddContractItem(token, cookies, sContractObjectID,client);
        	var party=true;//fnAddContractParty(token, cookies, oMessage,client);
        	
        	//Get data for relationship update
        	var first=oNewParty.CustomerID;
        	
        	request2 = new $.web.WebRequest($.net.http.GET,"IndividualCustomerCollection('"+sMainPartyObjectID+"')");
    		request2.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    		request2.headers.set("Accept", "application/json");
    		request2.headers.set("content-type", "application/json");
			request2.headers.set("X-CSRF-Token", token);
    		client.request(request2,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");
    		
    		var sData = client.getResponse().body.asString();
			var oJson = JSON.parse(sData);
    		var oMainParty = oJson.d.results;
    		var second=oMainParty.CustomerID;

        	var type = "";
        	if(oMessage.RoleCode=="Z1"){
        		type = "Z001";
        	}else{
        		type = "Z002";
        	}

        	var rel=fnUpdateRelationship(token, cookies, type, first, second ,client);
        	
        	// close the connection
			client.close();
            $.response.setBody(JSON.stringify({"Relationship":rel,"Party":party,"Item":item}));
            break;
        case $.net.http.GET:
        case $.net.http.PUT:
    	default:
            $.response.setBody(JSON.stringify({"Error":"Method not supported"}));
            break;
    }
} catch (err) {
    $.response.setBody("Failed to execute action: " + err.toString());
}