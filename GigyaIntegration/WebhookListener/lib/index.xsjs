$.response.contentType = "application/json";
$.response.status = 200;
$.response.contentType = "text/plain";



//Implementation of GET call
function fnHandleGet() {
	
    return {"myResult":"success"};
}

//Implementation of POST call
function fnHandlePost() {
	
	var sMessage = $.request.body.asString();
	
	//console.log(sMessage);
	
	var oMessage = JSON.parse(sMessage);
	
	for (i = 0; i < oMessage.events.length; i++) {
		
		console.log("Event detected: " + oMessage.events[i].type + " for " + oMessage.events[i].data.uid);
		if(oMessage.events[i].data.uid!="8b93faaf975c483594e5c6442f79cd32"){
			fnHandleEvent(oMessage.events[i]);
		} else {
			console.log("Event filtered out: " + oMessage.events[i].data.uid)
		}
		
    	
	} 
	
	var iLength= oMessage.events.length;
	
	var response =iLength + " events detected";
	
    return {response};
}

function fnHandleEvent(oEvent){
	switch ( oEvent.type ){
		case "subscriptionUpdated":
			fnHandleSubscriptionUpdated(oEvent.data);
			break;
		default:
			console.log("Event type without handling detected: " + oEvent.type);
	}
}

function fnHandleSubscriptionUpdated(oSub){
	//handling of subscriptions
	
	
	if(oSub.subscription.mitgliedschaft){
		var subID = "00163E72111A1ED8BE8C0CAA6085297F";
		var bool = oSub.subscription.mitgliedschaft.email.isSubscribed;
	}else if(oSub.subscription.autovermietung){
		var subID = "00163E72111A1ED8BE8C0CAA6085697F";
		var bool = oSub.subscription.autovermietung.email.isSubscribed;
	}else if(oSub.subscription.versicherungen){
		var subID = "00163E72111A1ED8BE8C0CAA6085497F";
		var bool = oSub.subscription.versicherungen.email.isSubscribed;
	}else{
		var subID = "00163E72111A1ED8BCF82AD175C5D9FB";
		var bool = false;
		console.log("Andere: " + oSub.subscription);
	}
	
	var destination;
	var client;
	var request;
	var post;
	
	try{
		//console.log("Try logon");
    	//Reading the destination properties 
    	//destination = $.net.http.readDestination("EXTERNAL_HTTP");
    	//console.log(destination);
    	//Creating HTTP Client
    	client = new $.net.http.Client();
    	//Creating Request
    	request = new $.web.WebRequest($.net.http.GET,"CommunicationTypePermissionCollection('"+subID+"')?$format=json");
    	request.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    	request.headers.set("Accept", "application/json");
    	request.headers.set("content-type", "application/json");
		request.headers.set("X-CSRF-Token", "fetch");
    	client.request(request,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");
    
    	var token = getCSRFToken(client.getResponse().headers);

    	//console.log("Token:" + token);
		var sData = client.getResponse().body.asString();
		var oJson = JSON.parse(sData);
    	var event = oJson.d.results;
		event.SubscribedIndicator = bool;
		
		
		//Getting the response body and setting as output data
    	//$.response.setBody(client.getResponse().body.asString());
		
		post = new $.web.WebRequest($.net.http.PUT,"CommunicationTypePermissionCollection('"+subID+"')");
		post.cookies=client.getResponse().cookies;
    	post.headers.set("Authorization", "Basic YXJlbjAwMTpqM1JEQjNxM3duNUxIT3pkSFdmWA==");
    	post.headers.set("Accept", "application/json");
    	post.headers.set("content-type", "application/json");
		post.headers.set("X-CSRF-Token", token);
		post.setBody(JSON.stringify(event));
		//console.log(post.headers);
    	client.request(post,"https://my343121.crm.ondemand.com/sap/byd/odata/v1/c4codataapi");
    	
    	console.log("Subscription updated");
    	

		// close the connection
		client.close();
    	
	}
	catch(errorObj){
		console.log(errorObj.message);
		$.response.setBody(JSON.stringify({
			ERROR : errorObj.message
		}));
		
	}
}

function getCSRFToken(headers){
	var token = "fetch";
	
	for (i = 0; i < headers.length; i++) {
	
		if(headers[i].name==="x-csrf-token"){
			token=headers[i].value;
		}
	}
	return token;
}
        
try {
    switch ( $.request.method ) {
        //Handle your GET calls here
        case $.net.http.GET:
            $.response.setBody(JSON.stringify(fnHandleGet()));
            break;
        //Handle your POST calls here
        case $.net.http.POST:
            $.response.setBody(JSON.stringify(fnHandlePost()));
            break;            
        default:
            break;
    }
} catch (err) {
    $.response.setBody("Failed to execute action: " + err.toString());
}

