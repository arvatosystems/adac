# adac
# How to install the project
1.) install mysql database

2.) Download and extract following zip files from kolb4ftp server:
2.a) HYBRISCOMM180800P_3-70003534.ZIP
2.b) CXFINANCIALPACK00_0-80004164.ZIP
2.c) mysql-connector-java-5.0.8-bin.jar
2.d) mysqldump_initdata.sql.gzip to load as initial db system
2.e) media_initdata.tar.gz as intial media directory
2.f) ext-adac.tar.gz as extended ext-bin directory - changed after gigya installation
ftp server: ftp4.kolb-digital.de
User: : KD-Trans 
Password: Trans$$29

3.) link config and bin/custom into the local hybris installation
4.) copy mysql conector to platform/lib/dbdriver directory
5.) run ant clean all (db already loaded via mysql!)
