/**
 *
 */
package de.hybris.adac.commerceservices.customer.impl;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.adac.integrationservices.outbound.services.AdacOutboundIntegrationService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Resource;


/**
 * @author LIAW001
 *
 */
public class AdacDefaultCustomerAccountService extends DefaultCustomerAccountService
{

	@Resource(name = "adacOutboundCustomerIntegrationService")
	private AdacOutboundIntegrationService outboundCustomerIntegrationService;
	@Resource(name = "adacOutboundPartyCustomerIntegrationService")
	private AdacOutboundIntegrationService outboundPartyCustomerIntegrationService;

	@Override
	protected void registerCustomer(final CustomerModel customerModel, final String password) throws DuplicateUidException
	{
		super.registerCustomer(customerModel, password);

		if (customerModel instanceof PartyModel)
		{
			outboundPartyCustomerIntegrationService.execute(customerModel);
		}
		else
		{
			outboundCustomerIntegrationService.execute(customerModel);
		}

	}
}
