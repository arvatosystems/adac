/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.platform.financialservices.model.InsurancePolicyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.commons.lang3.StringUtils;


/**
 * @author POEN004
 *
 */
public class InsurancePolicyProductIDHandler implements DynamicAttributeHandler<String, InsurancePolicyModel>
{
	@Override
	public String get(final InsurancePolicyModel arg0)
	{
		while (arg0.getCoverages().iterator().hasNext())
		{
			final String code = arg0.getCoverages().iterator().next().getCoverageProduct().getCode();
			if (code.equals("MEMBERSHIP_BASIS_ANNUAL") || code.equals("MEMBERSHIP_PLUS_ANNUAL")
					|| code.equals("MEMBERSHIP_YOUNG_ANNUAL"))
			{
				return code;
			}
		}


		return StringUtils.EMPTY;
	}

	@Override
	public void set(final InsurancePolicyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();

	}
}
