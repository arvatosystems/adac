/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.platform.financialservices.model.InsurancePolicyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.commons.lang3.StringUtils;


/**
 * @author POEN004
 *
 */
public class InsurancePolicySAPCustomerIDHandler implements DynamicAttributeHandler<String, InsurancePolicyModel>
{
	@Override
	public String get(final InsurancePolicyModel arg0)
	{
		if (arg0.getUser() != null)
		{
			return arg0.getUser().getExternalId();
		}

		return StringUtils.EMPTY;
	}

	@Override
	public void set(final InsurancePolicyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();

	}
}
