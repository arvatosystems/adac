/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.commons.lang3.StringUtils;


/**
 * @author LIAW001
 *
 */
public class PartyContractIDHandler implements DynamicAttributeHandler<String, PartyModel>
{
	private static final String VALUE = "16";

	@Override
	public String get(final PartyModel arg0)
	{
		if (arg0.getInsurancePolicy() != null)
		{
			final String contractId = arg0.getInsurancePolicy().getMockContractID();

			if (!StringUtils.isEmpty(contractId))
			{
				return contractId;
			}
		}

		return VALUE;
	}

	@Override
	public void set(final PartyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();

	}

}
