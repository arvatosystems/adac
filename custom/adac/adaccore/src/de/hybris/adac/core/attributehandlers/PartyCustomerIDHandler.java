/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.commons.lang3.StringUtils;


/**
 * @author LIAW001
 *
 */
public class PartyCustomerIDHandler implements DynamicAttributeHandler<String, PartyModel>
{
	@Override
	public String get(final PartyModel arg0)
	{
		if (arg0.getInsurancePolicy() != null)
		{
			return arg0.getInsurancePolicy().getUser().getExternalId();
		}

		return StringUtils.EMPTY;
	}

	@Override
	public void set(final PartyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();

	}
}
