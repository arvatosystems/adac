/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.commons.lang3.StringUtils;


/**
 * @author LIAW001
 *
 */
public class PartyObjectIDHandler implements DynamicAttributeHandler<String, PartyModel>
{
	private static final String OBJECT_ID = "00163E72111A1EE8BCAFF1D571402338";

	@Override
	public String get(final PartyModel arg0)
	{
		if (arg0.getInsurancePolicy() != null)
		{
			final String object_id = arg0.getInsurancePolicy().getMockObjectID();

			if (!StringUtils.isEmpty(object_id))
			{
				return object_id;
			}
		}
		return OBJECT_ID;
	}

	@Override
	public void set(final PartyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();

	}

}
