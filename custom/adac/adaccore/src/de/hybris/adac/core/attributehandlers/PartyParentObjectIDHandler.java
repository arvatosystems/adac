/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.commons.lang3.StringUtils;


/**
 * @author LIAW001
 *
 */
public class PartyParentObjectIDHandler implements DynamicAttributeHandler<String, PartyModel>
{
	private static final String VALUE = "00163E72111A1EE8BCAFEAF205FC82AA";

	@Override
	public String get(final PartyModel arg0)
	{
		if (arg0.getInsurancePolicy() != null)
		{
			final String parentObjectID = arg0.getInsurancePolicy().getMockParentObjectID();
			if (!StringUtils.isEmpty(parentObjectID))
			{
				return parentObjectID;
			}
		}

		return VALUE;
	}

	@Override
	public void set(final PartyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();

	}

}
