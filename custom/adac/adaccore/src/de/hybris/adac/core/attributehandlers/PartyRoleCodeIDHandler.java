/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.adac.core.enums.PartyRoleCodeEnum;
import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.HashMap;
import java.util.Map;


/**
 * @author liaw001
 *
 */
public class PartyRoleCodeIDHandler implements DynamicAttributeHandler<String, PartyModel>
{
	private static Map<PartyRoleCodeEnum, String> roleCodeMap = new HashMap<PartyRoleCodeEnum, String>();

	static
	{
		roleCodeMap.put(PartyRoleCodeEnum.CUSTOMER, "1001");
		roleCodeMap.put(PartyRoleCodeEnum.CHILD, "Z2");
		roleCodeMap.put(PartyRoleCodeEnum.SHIPTO, "1005");
		roleCodeMap.put(PartyRoleCodeEnum.BILLTO, "10");
		roleCodeMap.put(PartyRoleCodeEnum.PAYER, "8");
		roleCodeMap.put(PartyRoleCodeEnum.PARTNER, "Z1");
	}


	@Override
	public String get(final PartyModel arg0)
	{
		return roleCodeMap.get(arg0.getRole());
	}

	@Override
	public void set(final PartyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();

	}

}
