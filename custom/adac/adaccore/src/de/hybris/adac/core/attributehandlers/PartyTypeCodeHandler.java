/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;


/**
 * @author liaw001
 *
 */
public class PartyTypeCodeHandler implements DynamicAttributeHandler<String, PartyModel>
{
	private static final String partyTypeCode = "147";

	@Override
	public String get(final PartyModel arg0)
	{
		return partyTypeCode;
	}

	@Override
	public void set(final PartyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();

	}

}
