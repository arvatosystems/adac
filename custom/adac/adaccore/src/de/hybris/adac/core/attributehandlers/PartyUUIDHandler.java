/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;


/**
 * @author LIAW001
 *
 */
public class PartyUUIDHandler implements DynamicAttributeHandler<String, PartyModel>
{
	@Override
	public String get(final PartyModel arg0)
	{
		return arg0.getCustomerID();
	}

	@Override
	public void set(final PartyModel arg0, final String arg1)
	{
		throw new UnsupportedOperationException();
	}

}
