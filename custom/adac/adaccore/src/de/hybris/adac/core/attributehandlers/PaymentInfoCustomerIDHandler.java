/**
 *
 */
package de.hybris.adac.core.attributehandlers;

import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.apache.commons.lang3.StringUtils;


/**
 * @author LIAW001
 *
 */
public class PaymentInfoCustomerIDHandler implements DynamicAttributeHandler<String, PaymentInfoModel>
{

	@Override
	public String get(final PaymentInfoModel paymentInfo)
	{
		if (paymentInfo instanceof DebitPaymentInfoModel)
		{
			return paymentInfo.getUser().getExternalId();
		}
		return StringUtils.EMPTY;
	}

	@Override
	public void set(final PaymentInfoModel paymentInfoModel, final String arg1)
	{
		throw new UnsupportedOperationException();

	}

}
