/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.adac.core.integrationservices.client.impl;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;


/**
 * The default rest template interceptor to add header attributes.
 */
public class AdacIntegrationRestTemplateHeaderInterceptor implements ClientHttpRequestInterceptor
{

	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution)
			throws IOException
	{
		request.getHeaders().add("id", UUID.randomUUID().toString().replaceAll("-", ""));
		request.getHeaders().add("uuid", UUID.randomUUID().toString());
		request.getHeaders().add("senderParty", "C4COMMERCE");
		request.getHeaders().add("recipientParty", "0M2ELD8");
		request.getHeaders().add("timestamp", new Timestamp(new Date().getTime()).toString());
		request.getHeaders().add("X-CSRF-Token", "");

		return execution.execute(request, body);
	}
}
