/**
 *
 */
package de.hybris.adac.core.interceptors;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;


/**
 * @author liaw001
 *
 */
public class PartyPrepareInterceptor implements PrepareInterceptor<PartyModel>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.interceptor.PrepareInterceptor#onPrepare(java.lang.Object,
	 * de.hybris.platform.servicelayer.interceptor.InterceptorContext)
	 */
	@Override
	public void onPrepare(final PartyModel arg0, final InterceptorContext arg1) throws InterceptorException
	{
		if (StringUtils.isEmpty(arg0.getCustomerID()))
		{
			arg0.setCustomerID(UUID.randomUUID().toString());
		}
	}


}
