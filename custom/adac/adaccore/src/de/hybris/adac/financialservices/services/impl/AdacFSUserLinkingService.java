/**
 *
 */
package de.hybris.adac.financialservices.services.impl;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.adac.integrationservices.outbound.services.AdacOutboundIntegrationService;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.financialservices.services.FSUserLinkingService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author LIAW001
 *
 */
public class AdacFSUserLinkingService implements FSUserLinkingService
{
	private KeyGenerator fsExternalIdKeyGenerator;
	private ModelService modelService;
	private UserService userService;
	private AdacOutboundIntegrationService outboundCustomerIntegrationService;
	private AdacOutboundIntegrationService outboundPartyCustomerIntegrationService;



	@Override
	public UserModel linkRegisteredUserWithExternalSystem(final UserModel userModel)
	{
		if (userModel != null && StringUtils.isEmpty(userModel.getExternalId()))
		{
			if (userModel instanceof PartyModel)
			{
				outboundPartyCustomerIntegrationService.execute(userModel);

			}
			else
			{
				outboundCustomerIntegrationService.execute(userModel);

			}
		}

		//		testOutboundCustomer();

		return userModel;
	}

	//	private void testOutboundCustomer()
	//	{
	//		final CustomerModel customer = modelService.create(CustomerModel.class);
	//		customer.setName("Richard Brandson");
	//		customer.setUid("richard.brandson@hotmail.com");
	//		customer.setDateOfBirth(new Date());
	//
	//		final CountryModel countryModel = modelService.create(CountryModel.class);
	//		countryModel.setIsocode("MY");
	//		countryModel.setName("Malaysia");
	//
	//		final AddressModel paymentAddress = modelService.create(AddressModel.class);
	//		paymentAddress.setAppartment("Villa Height");
	//		paymentAddress.setBuilding("G Tower");
	//		paymentAddress.setCellphone("+60129246108");
	//		paymentAddress.setCountry(countryModel);
	//		paymentAddress.setDistrict("KL");
	//		paymentAddress.setEmail("richard.brandson_pay@hotmail.com");
	//		paymentAddress.setFax("+60389765423");
	//		paymentAddress.setFirstname("Richard");
	//		paymentAddress.setGender(Gender.MALE);
	//		paymentAddress.setLastname("Brandson");
	//		paymentAddress.setLine1("This is line 1");
	//		paymentAddress.setLine2("This is line 2");
	//		paymentAddress.setMiddlename("Peter");
	//		paymentAddress.setPhone1("+60128974536");
	//		paymentAddress.setPhone2("+60124567898");
	//		paymentAddress.setPobox("56-10");
	//		paymentAddress.setPostalcode("47100");
	//		paymentAddress.setStreetname("Tun Razak");
	//		paymentAddress.setStreetnumber("78");
	//		paymentAddress.setTown("Ampang");
	//
	//		customer.setDefaultPaymentAddress(paymentAddress);
	//
	//		final RegionModel regionModel = modelService.create(RegionModel.class);
	//		regionModel.setIsocode("WM");
	//		regionModel.setIsocodeShort("WM");
	//		regionModel.setName("West Malaysia");
	//		regionModel.setCountry(countryModel);
	//		paymentAddress.setRegion(regionModel);
	//
	//		outboundIntegrationService.post(customer);
	//	}

	@Override
	public boolean isUserLinked(final UserModel userModel)
	{
		return StringUtils.isNotEmpty(userModel.getExternalId());
	}

	protected String createMockExternalId(final UserModel userModel)
	{
		if (StringUtils.isNotEmpty(userModel.getExternalId()))
		{
			return userModel.getExternalId();
		}
		return String.valueOf(getFsExternalIdKeyGenerator().generate());
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected KeyGenerator getFsExternalIdKeyGenerator()
	{
		return fsExternalIdKeyGenerator;
	}

	@Required
	public void setFsExternalIdKeyGenerator(final KeyGenerator fsExternalIdKeyGenerator)
	{
		this.fsExternalIdKeyGenerator = fsExternalIdKeyGenerator;
	}

	public AdacOutboundIntegrationService getOutboundCustomerIntegrationService()
	{
		return outboundCustomerIntegrationService;
	}

	@Required

	public void setOutboundCustomerIntegrationService(final AdacOutboundIntegrationService outboundCustomerIntegrationService)
	{
		this.outboundCustomerIntegrationService = outboundCustomerIntegrationService;
	}

	public AdacOutboundIntegrationService getOutboundPartyCustomerIntegrationService()
	{
		return outboundPartyCustomerIntegrationService;
	}

	@Required

	public void setOutboundPartyCustomerIntegrationService(
			final AdacOutboundIntegrationService outboundPartyCustomerIntegrationService)
	{
		this.outboundPartyCustomerIntegrationService = outboundPartyCustomerIntegrationService;
	}




}
