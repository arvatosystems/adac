/**
 *
 */
package de.hybris.adac.integrationservices.client;

import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel;
import de.hybris.platform.integrationservices.client.IntegrationRestTemplateCreator;

import java.util.Collection;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestOperations;


/**
 * @author LIAW001
 *
 */
public interface AdacIntegrationRestTemplateCreator extends IntegrationRestTemplateCreator
{
	/**
	 * Create a rest template by given consumed destination model.
	 *
	 * @param destination
	 *           consumed destination model
	 * @param list
	 *           of interceptors
	 * @return restTemplate
	 */
	RestOperations create(ConsumedDestinationModel destination, Collection<ClientHttpRequestInterceptor> interceptors);
}
