/**
 *
 */
package de.hybris.adac.integrationservices.client;

import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel;
import de.hybris.platform.integrationservices.client.IntegrationRestTemplateFactory;

import java.util.Collection;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestOperations;


/**
 * @author LIAW001
 *
 */
public interface AdacIntegrationRestTemplateFactory extends IntegrationRestTemplateFactory
{
	RestOperations create(ConsumedDestinationModel destination, Collection<ClientHttpRequestInterceptor> interceptors);
}
