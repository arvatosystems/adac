/**
 *
 */
package de.hybris.adac.integrationservices.client.impl;

import de.hybris.adac.integrationservices.client.AdacIntegrationRestTemplateCreator;
import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel;
import de.hybris.platform.integrationservices.client.impl.DefaultIntegrationOAuth2RestTemplateCreator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;


/**
 * @author LIAW001
 *
 */
public class AdacDefaultIntegrationOAuth2RestTemplateCreator extends DefaultIntegrationOAuth2RestTemplateCreator
		implements AdacIntegrationRestTemplateCreator
{

	@Override
	public RestOperations create(final ConsumedDestinationModel destination,
			final Collection<ClientHttpRequestInterceptor> interceptors)
	{
		final RestTemplate restTemplate = (RestTemplate) create(destination);
		List<ClientHttpRequestInterceptor> interceptorsList = restTemplate.getInterceptors();

		if (interceptorsList == null)
		{
			interceptorsList = new ArrayList<ClientHttpRequestInterceptor>();
		}

		interceptorsList.addAll(interceptors);
		return restTemplate;
	}

}
