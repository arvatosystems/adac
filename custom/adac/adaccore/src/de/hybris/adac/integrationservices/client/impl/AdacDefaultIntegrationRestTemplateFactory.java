/**
 *
 */
package de.hybris.adac.integrationservices.client.impl;

import de.hybris.adac.integrationservices.client.AdacIntegrationRestTemplateCreator;
import de.hybris.adac.integrationservices.client.AdacIntegrationRestTemplateFactory;
import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel;
import de.hybris.platform.integrationservices.client.impl.DefaultIntegrationRestTemplateFactory;
import de.hybris.platform.integrationservices.client.impl.UnsupportedRestTemplateException;

import java.util.Collection;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestOperations;

import com.google.common.base.Preconditions;


/**
 * @author LIAW001
 *
 */
public class AdacDefaultIntegrationRestTemplateFactory extends DefaultIntegrationRestTemplateFactory
		implements AdacIntegrationRestTemplateFactory
{

	@Override
	public RestOperations create(final ConsumedDestinationModel destination,
			final Collection<ClientHttpRequestInterceptor> interceptors)
	{
		Preconditions.checkArgument(destination != null, "Consumed destination model cannot be null.");

		final AdacIntegrationRestTemplateCreator creator = (AdacIntegrationRestTemplateCreator) getRestTemplateCreators().stream()
				.filter(s -> s.isApplicable(destination)).findFirst().orElseThrow(UnsupportedRestTemplateException::new);

		return creator.create(destination, interceptors);
	}

}
