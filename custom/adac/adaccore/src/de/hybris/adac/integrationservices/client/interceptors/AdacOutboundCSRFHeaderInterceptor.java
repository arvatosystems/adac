/**
 *
 */
package de.hybris.adac.integrationservices.client.interceptors;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;


/**
 * @author LIAW001
 *
 */
public abstract class AdacOutboundCSRFHeaderInterceptor implements ClientHttpRequestInterceptor
{
	private static final String X_CSRF_TOKEN_HEADER = "X-CSRF-Token";

	private String csrfToken;

	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution)
			throws IOException
	{
		request.getHeaders().add(X_CSRF_TOKEN_HEADER, getCsrfToken());

		return execution.execute(request, body);
	}

	/**
	 * @return the csrfToken
	 */
	public String getCsrfToken()
	{
		return csrfToken;
	}

	/**
	 * @param csrfToken
	 *           the csrfToken to set
	 */
	public void setCsrfToken(final String csrfToken)
	{
		this.csrfToken = csrfToken;
	}
}
