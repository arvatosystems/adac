/**
 *
 */
package de.hybris.adac.integrationservices.client.interceptors;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpResponse;


/**
 * @author LIAW001
 *
 */
public class AdacOutboundPartyHeaderInterceptor extends AdacOutboundCSRFHeaderInterceptor
{

	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution)
			throws IOException
	{
		return super.intercept(request, body, execution);
	}
}
