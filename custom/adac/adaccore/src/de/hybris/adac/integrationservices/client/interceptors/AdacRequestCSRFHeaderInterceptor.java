/**
 *
 */
package de.hybris.adac.integrationservices.client.interceptors;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;


/**
 * @author LIAW001
 *
 */
public class AdacRequestCSRFHeaderInterceptor implements ClientHttpRequestInterceptor
{
	private static final String X_CSRF_TOKEN_HEADER = "X-CSRF-Token";
	private static final String FETCH = "Fetch";

	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution)
			throws IOException
	{
		request.getHeaders().add(X_CSRF_TOKEN_HEADER, FETCH);

		return execution.execute(request, body);
	}
}
