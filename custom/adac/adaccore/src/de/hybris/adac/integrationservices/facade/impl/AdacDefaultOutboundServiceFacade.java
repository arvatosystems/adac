/**
 *
 */
package de.hybris.adac.integrationservices.facade.impl;

import de.hybris.adac.integrationservices.client.AdacIntegrationRestTemplateFactory;
import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.integrationservices.facade.impl.DefaultOutboundServiceFacade;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestOperations;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;


/**
 * @author LIAW001
 *
 */
public class AdacDefaultOutboundServiceFacade extends DefaultOutboundServiceFacade
{
	private AdacIntegrationRestTemplateFactory integrationRestTemplateFactory;

	private List<ClientHttpRequestInterceptor> requestInterceptors = Lists.newArrayList();

	public ResponseEntity<Map> execute(final ItemModel itemModel, final String integrationObjectCode, final String destinationId,
			final HttpMethod httpMethod)
	{
		Preconditions.checkArgument(itemModel != null, "itemModel cannot be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(integrationObjectCode), "integrationObjectCode cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(destinationId), "destination cannot be null or empty");

		final ConsumedDestinationModel destination = (ConsumedDestinationModel) getDestinationService()
				.getDestinationById(destinationId);

		final Map<String, Object> payload = buildPayload(itemModel, integrationObjectCode);

		final RestOperations restOperations = getIntegrationRestTemplateFactory().create(destination, getRequestInterceptors());

		final ResponseEntity<Map> response = restOperations.exchange(StringUtils.EMPTY, httpMethod, new HttpEntity<>(payload),
				Map.class);

		return response;
	}

	protected List<ClientHttpRequestInterceptor> getRequestInterceptors()
	{
		return requestInterceptors;
	}

	public void setRequestInterceptors(final List<ClientHttpRequestInterceptor> requestInterceptors)
	{
		this.requestInterceptors = requestInterceptors;
	}

	@Override
	protected AdacIntegrationRestTemplateFactory getIntegrationRestTemplateFactory()
	{
		return integrationRestTemplateFactory;
	}

	@Required
	public void setIntegrationRestTemplateFactory(final AdacIntegrationRestTemplateFactory integrationRestTemplateFactory)
	{
		this.integrationRestTemplateFactory = integrationRestTemplateFactory;
	}
}
