/**
 *
 */
package de.hybris.adac.integrationservices.outbound.services;

import de.hybris.platform.core.model.ItemModel;


/**
 * @author LIAW001
 *
 */
public interface AdacOutboundIntegrationService
{
	public void execute(ItemModel item);
}
