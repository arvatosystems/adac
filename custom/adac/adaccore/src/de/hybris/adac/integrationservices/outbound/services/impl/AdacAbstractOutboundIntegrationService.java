/**
 *
 */
package de.hybris.adac.integrationservices.outbound.services.impl;

import de.hybris.adac.integrationservices.facade.impl.AdacDefaultOutboundServiceFacade;
import de.hybris.adac.integrationservices.outbound.services.AdacOutboundIntegrationService;
import de.hybris.platform.core.model.ItemModel;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import com.google.common.collect.Lists;


/**
 * @author LIAW001
 *
 */
public abstract class AdacAbstractOutboundIntegrationService implements AdacOutboundIntegrationService
{
	private static final Logger LOG = Logger.getLogger(AdacAbstractOutboundIntegrationService.class);

	private AdacDefaultOutboundServiceFacade defaultServiceFacade;
	private List<ClientHttpRequestInterceptor> requestInterceptors = Lists.newArrayList();

	protected ResponseEntity<Map> execute(final ItemModel item, final String integrationObjectCode, final String destinationId,
			final HttpMethod httpMethod)
	{
		LOG.info("Sending " + item.getItemtype() + " to external system.");
		getDefaultServiceFacade().setRequestInterceptors(getRequestInterceptors());
		final ResponseEntity<Map> response = getDefaultServiceFacade().execute(item, integrationObjectCode, destinationId,
				httpMethod);
		LOG.info(item.getItemtype() + " sent.");

		return response;
	}

	/**
	 * @return the requestInterceptors
	 */
	public List<ClientHttpRequestInterceptor> getRequestInterceptors()
	{
		return requestInterceptors;
	}

	/**
	 * @param requestInterceptors
	 *           the requestInterceptors to set
	 */
	public void setRequestInterceptors(final List<ClientHttpRequestInterceptor> requestInterceptors)
	{
		this.requestInterceptors = requestInterceptors;
	}

	/**
	 * @return the defaultServiceFacade
	 */
	public AdacDefaultOutboundServiceFacade getDefaultServiceFacade()
	{
		return defaultServiceFacade;
	}

	/**
	 * @param defaultServiceFacade
	 *           the defaultServiceFacade to set
	 */
	@Required
	public void setDefaultServiceFacade(final AdacDefaultOutboundServiceFacade adacDefaultOutboundServiceFacade)
	{
		this.defaultServiceFacade = adacDefaultOutboundServiceFacade;
	}
}
