/**
 *
 */
package de.hybris.adac.integrationservices.outbound.services.impl;

import de.hybris.adac.integrationservices.client.AdacIntegrationRestTemplateFactory;
import de.hybris.adac.integrationservices.client.interceptors.AdacRequestCSRFHeaderInterceptor;
import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel;
import de.hybris.platform.apiregistryservices.services.DestinationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestOperations;


/**
 * @author LIAW001
 *
 */
public class AdacCSRFTokenService
{
	private static final Logger LOG = Logger.getLogger(AdacCSRFTokenService.class);

	private DestinationService destinationService;
	private AdacIntegrationRestTemplateFactory integrationRestTemplateFactory;

	public String getCSRFToken(final String destinationID)
	{
		LOG.info("Requesting CSRF token from external system...");

		final ConsumedDestinationModel destination = (ConsumedDestinationModel) getDestinationService()
				.getDestinationById(destinationID);

		final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
		interceptors.add(new AdacRequestCSRFHeaderInterceptor());
		final RestOperations restOperations = getIntegrationRestTemplateFactory().create(destination, interceptors);

		final ResponseEntity<Map> response = restOperations.getForEntity(StringUtils.EMPTY, Map.class);

		final List<String> tokens = response.getHeaders().get("x-csrf-token");

		if (CollectionUtils.isEmpty(tokens))
		{
			LOG.warn("No CSRF token returned.");

			return StringUtils.EMPTY;
		}

		final String token = tokens.iterator().next();
		LOG.info("CSRF token request suceeded.");


		return token;
	}

	protected DestinationService getDestinationService()
	{
		return destinationService;
	}

	@Required
	public void setDestinationService(final DestinationService destinationService)
	{
		this.destinationService = destinationService;
	}

	protected AdacIntegrationRestTemplateFactory getIntegrationRestTemplateFactory()
	{
		return integrationRestTemplateFactory;
	}

	@Required
	public void setIntegrationRestTemplateFactory(final AdacIntegrationRestTemplateFactory integrationRestTemplateFactory)
	{
		this.integrationRestTemplateFactory = integrationRestTemplateFactory;
	}
}
