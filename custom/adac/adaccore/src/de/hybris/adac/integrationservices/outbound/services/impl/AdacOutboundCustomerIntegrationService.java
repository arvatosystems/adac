/**
 *
 */
package de.hybris.adac.integrationservices.outbound.services.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.google.common.base.Preconditions;


/**
 * @author LIAW001
 *
 */
public class AdacOutboundCustomerIntegrationService extends AdacAbstractOutboundIntegrationService
{
	private static final Logger LOG = Logger.getLogger(AdacOutboundCustomerIntegrationService.class);

	private static final String INTEGRATION_OBJECT_CODE = "OutboundCustomer";
	private static final String DESTINATION_ID = "sap-c4csales-customer";

	private static final String RETURNED_ID = "ID";

	private ModelService modelService;


	@Override
	public void execute(final ItemModel item)
	{
		Preconditions.checkArgument(item != null, "itemModel cannot be null");
		Preconditions.checkArgument(item instanceof CustomerModel, "itemModel is not CustomerModel");

		LOG.info("Exporting customer " + item.getPk().getLongValueAsString());

		final ResponseEntity<Map> response = execute(item, INTEGRATION_OBJECT_CODE, DESTINATION_ID, HttpMethod.POST);
		afterPost((CustomerModel) item, response);
	}

	private void afterPost(final CustomerModel customerModel, final ResponseEntity<Map> response)
	{
		final String c4cSalesID = (String) response.getBody().get(RETURNED_ID);

		if (StringUtils.isEmpty(c4cSalesID))
		{
			LOG.warn("User linking to external system is not successful");
			return;
		}

		customerModel.setExternalId(c4cSalesID);
		getModelService().save(customerModel);
		LOG.info("Customer successfully linked with external system");
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


}
