/**
 *
 */
package de.hybris.adac.integrationservices.outbound.services.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.google.common.base.Preconditions;


/**
 * @author LIAW001
 *
 */
public class AdacOutboundDebitPaymentInfoIntegrationService extends AdacAbstractOutboundIntegrationService
{
	private static final Logger LOG = Logger.getLogger(AdacOutboundDebitPaymentInfoIntegrationService.class);

	private static final String INTEGRATION_OBJECT_CODE = "OutboundDebitPaymentInfo";
	private static final String DESTINATION_ID = "sap-c4csales-DebitPaymentInfo";

	@Override
	public void execute(final ItemModel item)
	{
		Preconditions.checkArgument(item != null, "itemModel cannot be null");
		Preconditions.checkArgument(item instanceof DebitPaymentInfoModel, "itemModel is not DebitPaymentInfoModel");

		LOG.info("Exporting Debit Payment Info " + item.getPk().getLongValueAsString());

		final ResponseEntity<Map> response = execute(item, INTEGRATION_OBJECT_CODE, DESTINATION_ID, HttpMethod.PUT);
	}

}
