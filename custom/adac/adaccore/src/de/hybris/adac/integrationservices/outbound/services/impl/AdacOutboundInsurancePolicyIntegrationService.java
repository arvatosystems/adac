/**
 *
 */
package de.hybris.adac.integrationservices.outbound.services.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.financialservices.model.InsurancePolicyModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.google.common.base.Preconditions;


/**
 * @author POEN004
 *
 */
public class AdacOutboundInsurancePolicyIntegrationService extends AdacAbstractOutboundIntegrationService
{
	private static final Logger LOG = Logger.getLogger(AdacOutboundInsurancePolicyIntegrationService.class);

	private static final String INTEGRATION_OBJECT_CODE = "OutboundInsurancePolicy";
	private static final String DESTINATION_ID = "sap-c4csales-insurancePolicy";

	private static final String RETURNED_ID = "ID";


	private ModelService modelService;

	@Override
	public void execute(final ItemModel item)
	{
		Preconditions.checkArgument(item != null, "itemModel cannot be null");
		Preconditions.checkArgument(item instanceof InsurancePolicyModel, "itemModel is not InsurancePolicyModel");
		LOG.info("Exporting InsurancePolicy " + item.getPk().getLongValueAsString());

		final ResponseEntity<Map> response = execute(item, INTEGRATION_OBJECT_CODE, DESTINATION_ID, HttpMethod.POST);
		afterPost((InsurancePolicyModel) item, response);
	}

	private void afterPost(final InsurancePolicyModel insurancePolicyModel, final ResponseEntity<Map> response)
	{
		final String c4serviceContractID = (String) response.getBody().get("ContractID");
		final String c4serviceObjectID = (String) response.getBody().get("ObjectID");
		final String c4serviceParentObjectID = (String) response.getBody().get("ParentObjectID");

		if (StringUtils.isEmpty(c4serviceContractID) || StringUtils.isEmpty(c4serviceObjectID)
				|| StringUtils.isEmpty(c4serviceParentObjectID))
		{
			LOG.warn("InsurancePolicy linking to external system is not successful");
			return;
		}

		insurancePolicyModel.setMockContractID(c4serviceContractID);
		insurancePolicyModel.setMockObjectID(c4serviceObjectID);
		insurancePolicyModel.setMockParentObjectID(c4serviceParentObjectID);
		getModelService().save(insurancePolicyModel);
		LOG.info("InsurancePolicy successfully linked with external system");
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
