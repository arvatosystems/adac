/**
 *
 */
package de.hybris.adac.integrationservices.outbound.services.impl;

import de.hybris.adac.core.model.PartyModel;
import de.hybris.platform.core.model.ItemModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;

import com.google.common.base.Preconditions;


/**
 * @author LIAW001
 *
 */
public class AdacOutboundPartyIntegrationService extends AdacAbstractOutboundIntegrationService
{
	private static final Logger LOG = Logger.getLogger(AdacOutboundPartyIntegrationService.class);

	private static final String INTEGRATION_OBJECT_CODE = "OutboundParty";
	private static final String DESTINATION_ID = "sap-c4csales-Party";

	private AdacCSRFTokenService tokenService;

	@Override
	public void execute(final ItemModel item)
	{
		Preconditions.checkArgument(item != null, "itemModel cannot be null");
		Preconditions.checkArgument(item instanceof PartyModel, "itemModel is not PartyModel");

		LOG.info("Linking Party " + item.getPk().getLongValueAsString());

		beforePost();
		execute(item, INTEGRATION_OBJECT_CODE, DESTINATION_ID, HttpMethod.POST);
	}

	private void beforePost()
	{
		//		final String token = tokenService.getCSRFToken(DESTINATION_ID);
		//
		//		if (!StringUtils.isEmpty(token))
		//		{
		//			final Iterator<ClientHttpRequestInterceptor> iter = getRequestInterceptors().iterator();
		//			while (iter.hasNext())
		//			{
		//				final ClientHttpRequestInterceptor interceptor = iter.next();
		//				if (interceptor instanceof AdacOutboundCSRFHeaderInterceptor)
		//				{
		//					((AdacOutboundCSRFHeaderInterceptor) interceptor).setCsrfToken(token);
		//				}
		//			}
		//		}
	}

	protected AdacCSRFTokenService getTokenService()
	{
		return tokenService;
	}

	@Required
	public void setTokenService(final AdacCSRFTokenService tokenService)
	{
		this.tokenService = tokenService;
	}
}
