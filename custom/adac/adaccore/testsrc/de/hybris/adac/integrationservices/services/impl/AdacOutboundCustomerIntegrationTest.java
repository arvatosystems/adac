/**
 *
 */
package de.hybris.adac.integrationservices.services.impl;

import de.hybris.adac.integrationservices.outbound.services.AdacOutboundIntegrationService;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Test;


/**
 * @author LIAW001
 *
 */
public class AdacOutboundCustomerIntegrationTest extends ServicelayerTransactionalTest
{
	@Resource(name = "adacOutboundCustomerIntegrationService")
	private AdacOutboundIntegrationService outboundIntegrationService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Test
	public void testOutBoundCustomer()
	{
		final CustomerModel customer = modelService.create(CustomerModel.class);
		customer.setName("Richard Brandson");
		customer.setUid("richard.brandson@hotmail.com");
		customer.setDateOfBirth(new Date());

		final CountryModel countryModel = modelService.create(CountryModel.class);
		countryModel.setIsocode("MY");
		countryModel.setName("Malaysia");

		final AddressModel paymentAddress = modelService.create(AddressModel.class);
		paymentAddress.setAppartment("Villa Height");
		paymentAddress.setBuilding("G Tower");
		paymentAddress.setCellphone("+60129246108");
		paymentAddress.setCountry(countryModel);
		paymentAddress.setDistrict("KL");
		paymentAddress.setEmail("richard.brandson_pay@hotmail.com");
		paymentAddress.setFax("+60389765423");
		paymentAddress.setFirstname("Richard");
		paymentAddress.setGender(Gender.MALE);
		paymentAddress.setLastname("Brandson");
		paymentAddress.setLine1("This is line 1");
		paymentAddress.setLine2("This is line 2");
		paymentAddress.setMiddlename("Peter");
		paymentAddress.setPhone1("+60128974536");
		paymentAddress.setPhone2("+60124567898");
		paymentAddress.setPobox("56-10");
		paymentAddress.setPostalcode("47100");
		paymentAddress.setStreetname("Tun Razak");
		paymentAddress.setStreetnumber("78");
		paymentAddress.setTown("Ampang");

		customer.setDefaultPaymentAddress(paymentAddress);

		final RegionModel regionModel = modelService.create(RegionModel.class);
		regionModel.setIsocode("WM");
		regionModel.setIsocodeShort("WM");
		regionModel.setName("West Malaysia");
		regionModel.setCountry(countryModel);
		paymentAddress.setRegion(regionModel);

		outboundIntegrationService.execute(customer);
	}
}
