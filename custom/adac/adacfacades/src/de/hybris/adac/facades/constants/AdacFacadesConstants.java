/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.adac.facades.constants;

/**
 * Global class for all AdacFacades constants.
 */
public class AdacFacadesConstants extends GeneratedAdacFacadesConstants
{
	public static final String EXTENSIONNAME = "adacfacades";

	private AdacFacadesConstants()
	{
		//empty
	}
}
