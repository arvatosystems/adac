/**
 *
 */
package de.hybris.adac.facades.financial.converter;

import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;


/**
 * @author MoleSoft-V
 *
 */
public class DebitPaymentInfoConverter
{

	public void convert(final DebitPaymentInfoModel source, final PaymentInfoData target)
	{

		if (source == null)
		{
			throw new IllegalArgumentException("Source must not be null");
		}

		if (target == null)
		{
			throw new IllegalArgumentException("Target must not be null");
		}

		if (source instanceof DebitPaymentInfoModel)
		{
			final DebitPaymentInfoModel paymentInfoModel = source;

			target.setAccountNumber(paymentInfoModel.getAccountNumber());
			target.setBankIDNumber(paymentInfoModel.getBankIDNumber());
			target.setBank(paymentInfoModel.getBank());
			target.setBaOwner(paymentInfoModel.getBaOwner());
			target.setCountryCode(paymentInfoModel.getCode());
		}

	}

}
