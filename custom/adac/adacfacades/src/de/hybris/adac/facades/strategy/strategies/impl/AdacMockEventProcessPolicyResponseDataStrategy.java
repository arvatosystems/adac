/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.facades.strategy.strategies.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.financialfacades.strategies.ProcessPolicyResponseDataStrategy;
import de.hybris.platform.financialservices.enums.InsuredObjectType;
import de.hybris.platform.financialservices.model.InsurancePolicyModel;
import de.hybris.platform.financialservices.model.InsuredObjectModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Calendar;
import java.util.Date;

import javax.xml.xpath.XPath;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.google.common.collect.Lists;



/**
 * Process data retrieved from policy facade and populate policy model and corresponding insured object for Event insurance.
 */
public class AdacMockEventProcessPolicyResponseDataStrategy extends ProcessPolicyResponseDataStrategy
{

	protected static final String XPATH_EVENT_HOLDER = "/form/eventDetails";

	private static final String LABEL_VENUE = "venue";
	private static final String LABEL_ADDRESS = "address";
	private static final String LABEL_CITY = "city";
	private static final String LABEL_COUNTRY = "country";

	@Override
	protected void populatePolicyCategorySpecificData(final InsurancePolicyModel policyModel, final OrderModel orderModel)
	{

		//final DateTimeFormatter formatter = DateTimeFormat.forPattern(FinancialfacadesConstants.INSURANCE_GENERIC_DATE_FORMAT);
		final InsuredObjectModel insuredObject = new InsuredObjectModel();
		insuredObject.setInsuredObjectType(InsuredObjectType.EVENT);
		insuredObject.setInsuredObjectItems(Lists.newArrayList());

		if (orderModel.getEntries().size() > 0 && orderModel.getEntries().get(0).getYFormData() != null)
		{
			final String yFormData = orderModel.getEntries().get(0).getYFormData().get(0).getContent();
			final Document document = XmlUtilTransformer.createDocument(yFormData);
			final XPath xpath = XmlUtilTransformer.createXPath();

			// need to recalculate new from ADAC form objects!
			//final String eventVenue = XmlUtilTransformer.getTextValue(xpath, document, XPATH_EVENT_HOLDER + "/venue");
			//final String eventAddress = XmlUtilTransformer.getTextValue(xpath, document, XPATH_EVENT_HOLDER + "/address");
			//final String eventCity = XmlUtilTransformer.getTextValue(xpath, document, XPATH_EVENT_HOLDER + "/city");
			//final String eventCountryCode = XmlUtilTransformer.getTextValue(xpath, document, XPATH_EVENT_HOLDER + "/country");
			//final String eventDate = XmlUtilTransformer.getTextValue(xpath, document, XPATH_EVENT_HOLDER + "/date");
			//final Date policyEventDate = formatter.parseDateTime(eventDate).toDate();
			final String eventVenue = "Office 365";
			final String eventAddress ="Eiserstr. 78";
			final String eventCity = "Verl";
			final String eventCountryCode = "DE";
			final Date dat = new Date();
			final String eventDate = "Datum  :  "+dat.getDay()+"."+dat.getMonth()+"."+dat.getYear();
			final Date policyEventDate = dat;

			final Calendar cal = Calendar.getInstance();
			final Date today = cal.getTime();
			cal.add(Calendar.YEAR, 1);
			final Date nextYear = cal.getTime();

			policyModel.setPolicyStartDate(policyEventDate);
			policyModel.setPolicyExpiryDate(nextYear);
			policyModel.setPolicyEffectiveDate(policyEventDate);

			if (StringUtils.isNotEmpty(eventVenue))
			{
				insuredObject.getInsuredObjectItems().add(buildInsuredObjectItem(
						eventVenue, LABEL_VENUE, 1));
			}

			if (StringUtils.isNotEmpty(eventAddress))
			{
				insuredObject.getInsuredObjectItems().add(buildInsuredObjectItem(
						eventAddress, LABEL_ADDRESS, 2));
			}

			if (StringUtils.isNotEmpty(eventCity))
			{
				insuredObject.getInsuredObjectItems().add(buildInsuredObjectItem(
						eventCity, LABEL_CITY, 3));
			}

			if (StringUtils.isNotEmpty(eventCountryCode))
			{
				final String eventCountry = getEventCountryByCode(eventCountryCode);
				insuredObject.getInsuredObjectItems().add(buildInsuredObjectItem(
						eventCountry, LABEL_COUNTRY, 4));
			}
		}
		policyModel.getInsuredObjects().add(insuredObject);
	}

	private String getEventCountryByCode(final String eventCountryCode)
	{
		String eventCountryName = "";
		try
		{
			final CountryModel country = getCommonI18NService().getCountry(eventCountryCode);
			if (country != null)
			{
				eventCountryName = country.getName();
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.error(ex.getMessage(), ex);
		}

		return eventCountryName;
	}
}
