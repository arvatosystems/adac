/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.checkout.step.strategy;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.adac.financial.checkout.step.FinancialServicesCheckoutStep;
import de.hybris.adac.financial.controllers.pages.checkout.steps.AbstractCheckoutStepController;

import java.util.List;


public interface DynamicFormCheckoutStrategy
{
	/**
	 * Create dynamic form progress checkout steps based on the cart entries. <br/>
	 * Depends on the content of the cart, dynamically create the form steps and defined the navigation between the
	 * forms.
	 *
	 * @param formCheckoutStepPlaceholder
	 */
	List<FinancialServicesCheckoutStep> createDynamicFormSteps(final CheckoutStep formCheckoutStepPlaceholder);


	/**
	 * Combine the Form Checkout Steps to the original Checkout steps for display the at the progress bar.
	 *
	 * @param formCheckoutStepPlaceholder
	 * @param originalCheckoutSteps
	 */
	List<AbstractCheckoutStepController.CheckoutSteps> combineFormCheckoutStepProgressBar(CheckoutStep formCheckoutStepPlaceholder,
			List<AbstractCheckoutStepController.CheckoutSteps> originalCheckoutSteps);

	/**
	 * Get all the Form HTMLs to display on the given formPageId
	 *
	 * @param formPageId
	 */
	List<String> getFormsInlineHtmlByFormPageId(final String formPageId);
}
