/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.controllers.cms;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.adac.financial.controllers.ControllerConstants;
import de.hybris.platform.financialfacades.facades.AgentFacade;
import de.hybris.platform.financialfacades.findagent.data.AgentData;
import de.hybris.platform.financialservices.model.components.CMSAgentRootComponentModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller("CMSAgentRootComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.CMSAgentRootComponent)
public class CMSAgentRootComponentController extends SubstitutingCMSAddOnComponentController<CMSAgentRootComponentModel>
{

	private static final Logger LOG = Logger.getLogger(CMSAgentRootComponentController.class);

	private static final String ACTIVE_CATEGORY = "activeCategory";
	@Resource(name = "agentFacade")
	private AgentFacade agentFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CMSAgentRootComponentModel component)
	{
		if (StringUtils.isNotEmpty(component.getAgentRootCategory()))
		{
			try
			{
				final List<AgentData> agents = getAgentFacade().getAgentsByCategory(component.getAgentRootCategory());
				if (CollectionUtils.isNotEmpty(agents))
				{
					model.addAttribute("agents", agents);
					final CategoryData categoryData = agents.stream().findFirst().get().getCategories().stream()
							.filter(c -> component.getAgentRootCategory().equals(c.getCode())).findFirst().get();
					model.addAttribute("category", categoryData);
				}
				final boolean isActiveCategory = request.getAttribute(ACTIVE_CATEGORY) != null && StringUtils
						.equals(request.getAttribute(ACTIVE_CATEGORY).toString(), component.getAgentRootCategory());
				model.addAttribute("isActiveCategory", isActiveCategory);

			}
			catch (IllegalArgumentException ex)
			{
				LOG.error(ex.getMessage(), ex);
			}
		}
	}

	protected AgentFacade getAgentFacade()
	{
		return agentFacade;
	}
}
