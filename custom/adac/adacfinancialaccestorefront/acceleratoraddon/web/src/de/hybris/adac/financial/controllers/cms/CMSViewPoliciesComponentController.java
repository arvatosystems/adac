/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.controllers.cms;

import de.hybris.platform.commercefacades.insurance.data.InsurancePolicyListingData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.adac.financial.controllers.ControllerConstants;
import de.hybris.platform.financialfacades.facades.InsuranceCartFacade;
import de.hybris.platform.financialfacades.facades.InsurancePolicyFacade;
import de.hybris.platform.financialservices.model.components.CMSViewPoliciesComponentModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller("CMSViewPoliciesComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.CMSViewPoliciesComponent)
public class CMSViewPoliciesComponentController extends SubstitutingCMSAddOnComponentController<CMSViewPoliciesComponentModel>
{
	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "policyFacade")
	private InsurancePolicyFacade insurancePolicyFacade;

	@Resource(name = "cartFacade")
	private InsuranceCartFacade cartFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CMSViewPoliciesComponentModel component)
	{
		if (getUserFacade().isAnonymousUser())
		{
			model.addAttribute("isAnonymousUser", true);
		}
		else
		{
			final List<InsurancePolicyListingData> policiesForCurrentCustomer = getInsurancePolicyFacade()
					.findEffectivePoliciesForCurrentCustomer();

			model.addAttribute("numDisplayablePolicies", component.getNumberOfPoliciesToDisplay());
			model.addAttribute("policies", policiesForCurrentCustomer);

			if (getCartFacade().getSavedCartsForCurrentUser() == null || getCartFacade().getSavedCartsForCurrentUser().isEmpty()
					|| !getCartFacade().hasEntries())
			{
				model.addAttribute("quotesExists", false);
			}
			else
			{
				model.addAttribute("quotesExists", true);
			}
		}
	}

	protected UserFacade getUserFacade()
	{
		return userFacade;
	}

	protected InsurancePolicyFacade getInsurancePolicyFacade()
	{
		return insurancePolicyFacade;
	}

	protected InsuranceCartFacade getCartFacade()
	{
		return cartFacade;
	}
}
