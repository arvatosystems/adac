/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.controllers.exceptions;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;


/**
 * The class of CommerceCartModificationUpperLimitRearchedException.
 */
public class CommerceCartModificationUpperLimitReachedException extends CommerceCartModificationException //NOSONAR
{
	public CommerceCartModificationUpperLimitReachedException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
