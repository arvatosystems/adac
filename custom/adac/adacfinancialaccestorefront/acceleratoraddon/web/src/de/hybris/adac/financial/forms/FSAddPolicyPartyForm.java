/**
 *
 */
package de.hybris.adac.financial.forms;

import de.hybris.adac.core.enums.PartyRoleCodeEnum;


/**
 * @author admin
 *
 */
public class FSAddPolicyPartyForm
{
	private String titleCode;
	private String firstName;
	private String lastName;
	private String dateOfBirth;
	private String email;
	private PartyRoleCodeEnum roleCode;

	/**
	 * @return the roleCode
	 */
	public PartyRoleCodeEnum getRoleCode()
	{
		return roleCode;
	}

	/**
	 * @param roleCode
	 *           the roleCode to set
	 */
	public void setRoleCode(final PartyRoleCodeEnum roleCode)
	{
		this.roleCode = roleCode;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth()
	{
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *           the dateOfBirth to set
	 */
	public void setDateOfBirth(final String dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}


	/**
	 * @return the titleCode
	 */
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}


	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

}
