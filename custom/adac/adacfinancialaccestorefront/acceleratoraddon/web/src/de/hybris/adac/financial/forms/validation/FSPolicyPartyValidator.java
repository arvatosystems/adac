/**
 *
 */
package de.hybris.adac.financial.forms.validation;

import de.hybris.adac.core.enums.PartyRoleCodeEnum;
import de.hybris.adac.financial.forms.FSAddPolicyPartyForm;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component("fsPolicyPartyValidator")
public class FSPolicyPartyValidator implements Validator
{
	public static final Pattern EMAIL_REGEX = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");
	@Resource
	private FSDateOfBirthValidator fsDateOfBirthValidator;

	protected FSDateOfBirthValidator getDateOfBirthValidator()
	{
		return fsDateOfBirthValidator;
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return FSAddPolicyPartyForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final FSAddPolicyPartyForm addPolicyPartyForm = (FSAddPolicyPartyForm) object;
		final String titleCode = addPolicyPartyForm.getTitleCode();
		final PartyRoleCodeEnum roleCode = addPolicyPartyForm.getRoleCode();
		final String firstName = addPolicyPartyForm.getFirstName();
		final String lastName = addPolicyPartyForm.getLastName();
		final String email = addPolicyPartyForm.getEmail();

		validateTitleCode(errors, titleCode);
		validateRoleCode(errors, roleCode);
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateEmail(errors, email);

		if (object instanceof FSAddPolicyPartyForm)
		{
			final FSAddPolicyPartyForm addPartyForm = (FSAddPolicyPartyForm) object;
			final String dateOfBirth = addPartyForm.getDateOfBirth();

			getDateOfBirthValidator().validate(dateOfBirth, errors);
		}
	}

	protected void validateEmail(final Errors errors, final String email)
	{
		if (StringUtils.isEmpty(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
		else if (StringUtils.length(email) > 255 || !validateEmailAddress(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
	}

	protected void validateName(final Errors errors, final String name, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(name))
		{
			errors.rejectValue(propertyName, property);
		}
		else if (StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, property);
		}
	}

	protected void validateTitleCode(final Errors errors, final String titleCode)
	{
		if (StringUtils.isEmpty(titleCode))
		{
			errors.rejectValue("titleCode", "register.title.invalid");
		}
		else if (StringUtils.length(titleCode) > 255)
		{
			errors.rejectValue("titleCode", "register.title.invalid");
		}
	}

	private void validateRoleCode(final Errors errors, final PartyRoleCodeEnum roleCode)
	{
		if (roleCode == null)
		{
			errors.rejectValue("roleCode", "profile.role.invalid");
		}

	}

	public boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = EMAIL_REGEX.matcher(email);
		return matcher.matches();
	}

}
