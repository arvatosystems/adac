/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ProfileValidator;
import de.hybris.adac.financial.forms.FSUpdateProfileForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.annotation.Resource;


@Component("fsProfileValidator")
public class FSProfileValidator extends ProfileValidator
{

	@Resource
	private FSDateOfBirthValidator fsDateOfBirthValidator;

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final FSUpdateProfileForm updateProfileForm = (FSUpdateProfileForm) object;
		final String dateOfBirth = updateProfileForm.getDateOfBirth();

		getDateOfBirthValidator().validate(dateOfBirth, errors);
		super.validate(object, errors);

	}

	protected FSDateOfBirthValidator getDateOfBirthValidator()
	{
		return fsDateOfBirthValidator;
	}
}
