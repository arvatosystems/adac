/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.strategies.impl;

import de.hybris.adac.financial.checkout.step.FinancialServicesCheckoutStep;
import de.hybris.adac.financial.strategies.StepTransitionStrategy;
import net.sourceforge.pmd.util.StringUtil;


public class DefaultStepTransitionStrategy implements StepTransitionStrategy
{

	@Override
	public void setVisited(final FinancialServicesCheckoutStep checkoutStep, final String currentURL)
	{
		final String activeUrl = checkoutStep.getActiveStep();
		final String alternativeUrl = checkoutStep.getAlternativeActiveStep();
		if ((StringUtil.isNotEmpty(activeUrl) && currentURL.contains(activeUrl))
				|| (StringUtil.isNotEmpty(alternativeUrl) && currentURL.contains(alternativeUrl)))
		{
			setVisited(checkoutStep);
		}
	}

	@Override
	public void setVisited(final FinancialServicesCheckoutStep checkoutStep)
	{
		checkoutStep.setVisited(true);
	}

}
