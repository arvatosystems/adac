<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartTitle" required="false" type="java.lang.String" %>
<%@ attribute name="messageKey" required="false" type="java.lang.String" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<spring:theme code='checkout.cart.empty.text' var="defaultText" />

<div class="cart-data-items">
    <div class="section-header__heading">
        <spring:theme code="checkout.multi.quoteReview.title.${fn:toLowerCase(cmsSite.channel)}" text="Application/Quote {0}" arguments="${cartTitle}"/>
    </div>
    <div id="desktop-spinner" class="spinner spinner--desktop js-spinner"></div>
    <div class="cart-data-items__items">
        <div class="cart-data-items__item">
            <span class="cart-data-items__name col-xs-12"><spring:theme code="${messageKey}" text="${defaultText}"/></span>
        </div>

    </div>

    <div id="orderTotals" class="cart-data-items__order-total">

    </div>

</div>
