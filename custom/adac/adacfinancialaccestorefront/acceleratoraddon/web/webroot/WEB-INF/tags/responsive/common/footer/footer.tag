<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<footer>
    <div class="l-footer-center ">
        <div class="m-layout-footer" data-t-name="LayoutFooter">
            <div class="m-left">

            <img src="${fn:escapeXml(themeResourcePath)}/images/footerSocials.png" />
            <div class="followUs">Folgen Sie uns</div>
            </div>
            <div class="m-right">
                <ul class="m-right-list">
                    <li> <a href="/datenschutz-dsgvo/online/" class=" " title="Datenschutz">Datenschutz</a>
                    </li>
                    <li> <a href="/impressum-ev/" class=" js-impressum" title="Impressum">Impressum</a>
                    </li>
                    <li> <a href="https://karriere.adac.de/" target="_blank" class=" " title="Karriere" data-external="true">Karriere</a>
                    </li>
                    <li> <a href="https://presse.adac.de/home/index.html" target="_blank" class=" " title="Presse"
                            data-external="true">Presse</a>
                    </li>
                    <li> <a href="/lob-kritik/default.aspx" target="_blank" class=" " title="Lob &amp; Kritik">Lob
                            &amp; Kritik</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

