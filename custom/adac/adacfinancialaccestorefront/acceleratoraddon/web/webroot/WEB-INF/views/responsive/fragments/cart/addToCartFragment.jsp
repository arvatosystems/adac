<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
{"result": {
    "success": "${addToCartSuccess}",
    "cartUpperLimitReached": "${cartUpperLimitReached}",
    "sameProductGroup": "${sameProductGroup}",
    "recalculateOnly": "${recalculateOnly}"
}}