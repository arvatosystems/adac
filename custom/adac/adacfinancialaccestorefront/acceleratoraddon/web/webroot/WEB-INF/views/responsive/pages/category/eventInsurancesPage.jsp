<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/adacfinancialaccestorefront/responsive/product" %>
<%@ taglib prefix="financialCart" tagdir="/WEB-INF/tags/addons/adacfinancialaccestorefront/responsive/cart" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}">
	<div class="boxed-content-wrapper">
        <cms:pageSlot position="Section1" var="feature">
            <cms:component component="${feature}" element="div" class="col-xs-12"/>
        </cms:pageSlot>

        <cms:pageSlot position="Section2" var="feature">
            <cms:component component="${feature}" element="div" class="col-xs-12"/>
        </cms:pageSlot>

        <div class="col-xs-12">
            <div id="tab_content" class="comparison-table comparison-table__body">
                    <%--TODO the columns parameter need implement by using configurable properties.--%>
                <cms:pageSlot position="ImageSlot" var="component" />
                <product:productComparison columns="4" searchPageData="${searchPageData}" imageComponent="${component}" hideOptionProducts="false" addToCartBtn_label_key="basket.add.to.basket.getaquote" />
            </div>
        </div>
        <financialCart:changePlanConfirmPopup confirmActionButtonId="addNewPlanConfirmButton" cartData="${cartData}"/>
    </div>
</template:page>
