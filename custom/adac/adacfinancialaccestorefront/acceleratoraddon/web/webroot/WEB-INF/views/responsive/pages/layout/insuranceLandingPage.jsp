<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/adacfinancialaccestorefront/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">

	<div class="full-width-content-wrapper">
			<aside class="o-layout-main-stage o-layout-main-stage--cluster" data-t-name="LayoutMainStage" data-speed="700"
			 data-speed-d="1500" data-t-id="9">
				<div class="swiper-container">

					<div class="swiper-wrapper cf">

						<div class="swiper-slide">
									<img src="${fn:escapeXml(themeResourcePath)}/images/IT_Arvato.jpg"
									 data-src="${fn:escapeXml(themeResourcePath)}/images/IT_Arvato.jpg"
									 class="oo-img swiper-lazy lazyloaded" alt="">
	<form id="addToCartForm" class="add_to_cart_form" action="/adacstorefront/insurance/en/cart/add" method="post">
							<div class="asideContent">
								<div class="oo-box-childwrapper">

									<h1 class="oo-boxheadline">
										<spring:theme code="landing.page.title" htmlEscape="false"/>
									</h1>

									<div class="asideLeft landingPageContent">
										<div>

											<div class="h-space-s"><strong><spring:theme code="landing.page.subtitle" /></strong></div>


											<%-- <div class="a-basic-input-radio h-space-s" >
												<input type="radio" id="TRA_ANNUAL_SILVER"  name="AUTO_MONTHLY" value="TRA_ANNUAL_SILVER" checked="checked" class="landingRadio">
												 <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
												 <input type="hidden" name="productCodePost" id="productCodePost" value="TRA_ANNUAL_SILVER">
													<label for="TRA_ANNUAL_SILVER">
														<spring:theme code="landing.page.plan.one" />
													</label>
											</div> --%>

											<%-- <div class="a-basic-input-radio h-space-s" >
												<input type="radio" id="AUTO_MONTHLY" name="AUTO_MONTHLY" value="AUTO_MONTHLY" class="landingRadio" >
													<label for="AUTO_MONTHLY">
														<spring:theme code="landing.page.plan.two" />
													</label>
											</div> --%>

											<%-- <div class="a-basic-input-radio h-space-s">
												<input type="radio" id="SAVINGS_SAFE_AND_STEADY" name="AUTO_MONTHLY" value="SAVINGS_SAFE_AND_STEADY" class="landingRadio">
													<label for="SAVINGS_SAFE_AND_STEADY">
														<spring:theme code="landing.page.plan.three" />
													</label>
											</div> --%>
													<br/>
											<a href="Insurance-Products/Event-Main/c/insurance_main_event" class="primary-button primary-button__default primary-button__default" >
												<spring:theme code="landing.page.join.now" />
											</a>
										</div>
									</div>
									<%-- <div class="asideRight">
									<br/><br/><br/><br/>
									 <a class="primary-button primary-button__default primary-button__default" >
												In den warenkorb 
									</a>
									 <button id="addToCartButton" type="submit" class="btn btn-primary btn-block js-add-to-cart js-enable-btn btn-icon glyphicon-shopping-cart">
										<spring:theme code="basket.add.to.basket" /></button>
									</div>   --%>
								</div>
							</div>
							</form>	
						</div>

					</div>
				</div>
			</aside>
			<br/><br/><br/>
		<%-- <cms:pageSlot position="Section1" var="feature">
			<cms:component component="${feature}" element="div" />
		</cms:pageSlot> --%>

		<div class="main-width-wrapper row flex flex--wrap-center">
			<cms:pageSlot position="Section2A" var="feature">
				<cms:component component="${feature}" element="div" class="col-xs-6 col-sm-4 col-md-3 service-links" />
			</cms:pageSlot>

		</div>

		<cms:pageSlot position="Section2B" var="feature">
			<cms:component component="${feature}" element="div" />
		</cms:pageSlot>

		<div class="main-width-wrapper review review__wrapper">
			<div class="row">
				<cms:pageSlot position="Section2C" var="feature">
					<cms:component component="${feature}" element="div" class="col-xs-12 col-sm-6" />
				</cms:pageSlot>
			</div>
		</div>

		<cms:pageSlot position="Section3" var="feature">
			<cms:component component="${feature}" element="div" />
		</cms:pageSlot>

		<cms:pageSlot position="Section4" var="feature">
			<cms:component component="${feature}" element="div" class="col-xs-12" />
		</cms:pageSlot>

		<cms:pageSlot position="Section5" var="feature">
			<cms:component component="${feature}" element="div" class="col-xs-12" />
		</cms:pageSlot>
	</div>
</template:page>