/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 22.11.2018 12:15:58                         ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAdacfinancialaccestorefrontConstants
{
	public static final String EXTENSIONNAME = "adacfinancialaccestorefront";
	public static class Attributes
	{
		public static class CMSAllOurServicesComponent
		{
			public static final String LINKIMAGES = "linkImages".intern();
		}
		public static class CMSLinkImageComponent
		{
			public static final String COLLECTION = "collection".intern();
		}
		public static class ComparisonPanelCMSComponent
		{
			public static final String MANDATORYBUNDLEPRODUCTS = "mandatoryBundleProducts".intern();
			public static final String PRODUCTS = "products".intern();
		}
		public static class Product
		{
			public static final String COMPARISONPANELCMSCOMPONENTS = "comparisonPanelCMSComponents".intern();
			public static final String COMPARISONPANELPRODUCTS = "comparisonPanelProducts".intern();
		}
	}
	public static class Relations
	{
		public static final String ALLOURSERVICECOMPONENT2LINKIMGREL = "AllOurServiceComponent2LinkImgRel".intern();
		public static final String COMPARISONPANELCMSCOMPONENTS2PRODUCTREL = "ComparisonPanelCMSComponents2ProductRel".intern();
		public static final String CPANELPRODUCT2PARAGRAPHCOMPONENT = "CPanelProduct2ParagraphComponent".intern();
	}
	
	protected GeneratedAdacfinancialaccestorefrontConstants()
	{
		// private constructor
	}
	
	
}
