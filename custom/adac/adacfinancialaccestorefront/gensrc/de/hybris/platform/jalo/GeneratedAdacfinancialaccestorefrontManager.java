/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 22.11.2018 12:15:58                         ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.jalo;

import de.hybris.platform.cms2.jalo.contents.components.CMSLinkComponent;
import de.hybris.platform.cms2.jalo.contents.components.SimpleCMSComponent;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.financialservices.jalo.components.CMSAllOurServicesComponent;
import de.hybris.platform.financialservices.jalo.components.CMSLinkImageComponent;
import de.hybris.platform.financialservices.jalo.components.ComparisonPanelCMSComponent;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.util.Utilities;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type <code>AdacfinancialaccestorefrontManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedAdacfinancialaccestorefrontManager extends Extension
{
	/** Relation ordering override parameter constants for ComparisonPanelCMSComponents2ProductRel from ((adacfinancialaccestorefront))*/
	protected static String COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED = "relation.ComparisonPanelCMSComponents2ProductRel.source.ordered";
	protected static String COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_TGT_ORDERED = "relation.ComparisonPanelCMSComponents2ProductRel.target.ordered";
	/** Relation disable markmodifed parameter constants for ComparisonPanelCMSComponents2ProductRel from ((adacfinancialaccestorefront))*/
	protected static String COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_MARKMODIFIED = "relation.ComparisonPanelCMSComponents2ProductRel.markmodified";
	/** Relation ordering override parameter constants for CPanelProduct2ParagraphComponent from ((adacfinancialaccestorefront))*/
	protected static String CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED = "relation.CPanelProduct2ParagraphComponent.source.ordered";
	protected static String CPANELPRODUCT2PARAGRAPHCOMPONENT_TGT_ORDERED = "relation.CPanelProduct2ParagraphComponent.target.ordered";
	/** Relation disable markmodifed parameter constants for CPanelProduct2ParagraphComponent from ((adacfinancialaccestorefront))*/
	protected static String CPANELPRODUCT2PARAGRAPHCOMPONENT_MARKMODIFIED = "relation.CPanelProduct2ParagraphComponent.markmodified";
	/** Relation ordering override parameter constants for AllOurServiceComponent2LinkImgRel from ((adacfinancialaccestorefront))*/
	protected static String ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED = "relation.AllOurServiceComponent2LinkImgRel.source.ordered";
	protected static String ALLOURSERVICECOMPONENT2LINKIMGREL_TGT_ORDERED = "relation.AllOurServiceComponent2LinkImgRel.target.ordered";
	/** Relation disable markmodifed parameter constants for AllOurServiceComponent2LinkImgRel from ((adacfinancialaccestorefront))*/
	protected static String ALLOURSERVICECOMPONENT2LINKIMGREL_MARKMODIFIED = "relation.AllOurServiceComponent2LinkImgRel.markmodified";
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CMSLinkImageComponent.collection</code> attribute.
	 * @return the collection
	 */
	public Collection<CMSAllOurServicesComponent> getCollection(final SessionContext ctx, final CMSLinkImageComponent item)
	{
		final List<CMSAllOurServicesComponent> items = item.getLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			"CMSAllOurServicesComponent",
			null,
			Utilities.getRelationOrderingOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CMSLinkImageComponent.collection</code> attribute.
	 * @return the collection
	 */
	public Collection<CMSAllOurServicesComponent> getCollection(final CMSLinkImageComponent item)
	{
		return getCollection( getSession().getSessionContext(), item );
	}
	
	public long getCollectionCount(final SessionContext ctx, final CMSLinkImageComponent item)
	{
		return item.getLinkedItemsCount(
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			"CMSAllOurServicesComponent",
			null
		);
	}
	
	public long getCollectionCount(final CMSLinkImageComponent item)
	{
		return getCollectionCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CMSLinkImageComponent.collection</code> attribute. 
	 * @param value the collection
	 */
	public void setCollection(final SessionContext ctx, final CMSLinkImageComponent item, final Collection<CMSAllOurServicesComponent> value)
	{
		item.setLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			null,
			value,
			Utilities.getRelationOrderingOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CMSLinkImageComponent.collection</code> attribute. 
	 * @param value the collection
	 */
	public void setCollection(final CMSLinkImageComponent item, final Collection<CMSAllOurServicesComponent> value)
	{
		setCollection( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to collection. 
	 * @param value the item to add to collection
	 */
	public void addToCollection(final SessionContext ctx, final CMSLinkImageComponent item, final CMSAllOurServicesComponent value)
	{
		item.addLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to collection. 
	 * @param value the item to add to collection
	 */
	public void addToCollection(final CMSLinkImageComponent item, final CMSAllOurServicesComponent value)
	{
		addToCollection( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from collection. 
	 * @param value the item to remove from collection
	 */
	public void removeFromCollection(final SessionContext ctx, final CMSLinkImageComponent item, final CMSAllOurServicesComponent value)
	{
		item.removeLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from collection. 
	 * @param value the item to remove from collection
	 */
	public void removeFromCollection(final CMSLinkImageComponent item, final CMSAllOurServicesComponent value)
	{
		removeFromCollection( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.comparisonPanelCMSComponents</code> attribute.
	 * @return the comparisonPanelCMSComponents
	 */
	public Collection<ComparisonPanelCMSComponent> getComparisonPanelCMSComponents(final SessionContext ctx, final Product item)
	{
		final List<ComparisonPanelCMSComponent> items = item.getLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			"ComparisonPanelCMSComponent",
			null,
			Utilities.getRelationOrderingOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.comparisonPanelCMSComponents</code> attribute.
	 * @return the comparisonPanelCMSComponents
	 */
	public Collection<ComparisonPanelCMSComponent> getComparisonPanelCMSComponents(final Product item)
	{
		return getComparisonPanelCMSComponents( getSession().getSessionContext(), item );
	}
	
	public long getComparisonPanelCMSComponentsCount(final SessionContext ctx, final Product item)
	{
		return item.getLinkedItemsCount(
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			"ComparisonPanelCMSComponent",
			null
		);
	}
	
	public long getComparisonPanelCMSComponentsCount(final Product item)
	{
		return getComparisonPanelCMSComponentsCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.comparisonPanelCMSComponents</code> attribute. 
	 * @param value the comparisonPanelCMSComponents
	 */
	public void setComparisonPanelCMSComponents(final SessionContext ctx, final Product item, final Collection<ComparisonPanelCMSComponent> value)
	{
		item.setLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			null,
			value,
			Utilities.getRelationOrderingOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.comparisonPanelCMSComponents</code> attribute. 
	 * @param value the comparisonPanelCMSComponents
	 */
	public void setComparisonPanelCMSComponents(final Product item, final Collection<ComparisonPanelCMSComponent> value)
	{
		setComparisonPanelCMSComponents( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to comparisonPanelCMSComponents. 
	 * @param value the item to add to comparisonPanelCMSComponents
	 */
	public void addToComparisonPanelCMSComponents(final SessionContext ctx, final Product item, final ComparisonPanelCMSComponent value)
	{
		item.addLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to comparisonPanelCMSComponents. 
	 * @param value the item to add to comparisonPanelCMSComponents
	 */
	public void addToComparisonPanelCMSComponents(final Product item, final ComparisonPanelCMSComponent value)
	{
		addToComparisonPanelCMSComponents( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from comparisonPanelCMSComponents. 
	 * @param value the item to remove from comparisonPanelCMSComponents
	 */
	public void removeFromComparisonPanelCMSComponents(final SessionContext ctx, final Product item, final ComparisonPanelCMSComponent value)
	{
		item.removeLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from comparisonPanelCMSComponents. 
	 * @param value the item to remove from comparisonPanelCMSComponents
	 */
	public void removeFromComparisonPanelCMSComponents(final Product item, final ComparisonPanelCMSComponent value)
	{
		removeFromComparisonPanelCMSComponents( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.comparisonPanelProducts</code> attribute.
	 * @return the comparisonPanelProducts
	 */
	public Collection<ComparisonPanelCMSComponent> getComparisonPanelProducts(final SessionContext ctx, final Product item)
	{
		final List<ComparisonPanelCMSComponent> items = item.getLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			"ComparisonPanelCMSComponent",
			null,
			Utilities.getRelationOrderingOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.comparisonPanelProducts</code> attribute.
	 * @return the comparisonPanelProducts
	 */
	public Collection<ComparisonPanelCMSComponent> getComparisonPanelProducts(final Product item)
	{
		return getComparisonPanelProducts( getSession().getSessionContext(), item );
	}
	
	public long getComparisonPanelProductsCount(final SessionContext ctx, final Product item)
	{
		return item.getLinkedItemsCount(
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			"ComparisonPanelCMSComponent",
			null
		);
	}
	
	public long getComparisonPanelProductsCount(final Product item)
	{
		return getComparisonPanelProductsCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.comparisonPanelProducts</code> attribute. 
	 * @param value the comparisonPanelProducts
	 */
	public void setComparisonPanelProducts(final SessionContext ctx, final Product item, final Collection<ComparisonPanelCMSComponent> value)
	{
		item.setLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			null,
			value,
			Utilities.getRelationOrderingOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.comparisonPanelProducts</code> attribute. 
	 * @param value the comparisonPanelProducts
	 */
	public void setComparisonPanelProducts(final Product item, final Collection<ComparisonPanelCMSComponent> value)
	{
		setComparisonPanelProducts( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to comparisonPanelProducts. 
	 * @param value the item to add to comparisonPanelProducts
	 */
	public void addToComparisonPanelProducts(final SessionContext ctx, final Product item, final ComparisonPanelCMSComponent value)
	{
		item.addLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to comparisonPanelProducts. 
	 * @param value the item to add to comparisonPanelProducts
	 */
	public void addToComparisonPanelProducts(final Product item, final ComparisonPanelCMSComponent value)
	{
		addToComparisonPanelProducts( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from comparisonPanelProducts. 
	 * @param value the item to remove from comparisonPanelProducts
	 */
	public void removeFromComparisonPanelProducts(final SessionContext ctx, final Product item, final ComparisonPanelCMSComponent value)
	{
		item.removeLinkedItems( 
			ctx,
			false,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from comparisonPanelProducts. 
	 * @param value the item to remove from comparisonPanelProducts
	 */
	public void removeFromComparisonPanelProducts(final Product item, final ComparisonPanelCMSComponent value)
	{
		removeFromComparisonPanelProducts( getSession().getSessionContext(), item, value );
	}
	
	@Override
	public String getName()
	{
		return AdacfinancialaccestorefrontConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CMSAllOurServicesComponent.linkImages</code> attribute.
	 * @return the linkImages
	 */
	public List<CMSLinkImageComponent> getLinkImages(final SessionContext ctx, final CMSAllOurServicesComponent item)
	{
		final List<CMSLinkImageComponent> items = item.getLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			"CMSLinkImageComponent",
			null,
			Utilities.getRelationOrderingOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CMSAllOurServicesComponent.linkImages</code> attribute.
	 * @return the linkImages
	 */
	public List<CMSLinkImageComponent> getLinkImages(final CMSAllOurServicesComponent item)
	{
		return getLinkImages( getSession().getSessionContext(), item );
	}
	
	public long getLinkImagesCount(final SessionContext ctx, final CMSAllOurServicesComponent item)
	{
		return item.getLinkedItemsCount(
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			"CMSLinkImageComponent",
			null
		);
	}
	
	public long getLinkImagesCount(final CMSAllOurServicesComponent item)
	{
		return getLinkImagesCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CMSAllOurServicesComponent.linkImages</code> attribute. 
	 * @param value the linkImages
	 */
	public void setLinkImages(final SessionContext ctx, final CMSAllOurServicesComponent item, final List<CMSLinkImageComponent> value)
	{
		item.setLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			null,
			value,
			Utilities.getRelationOrderingOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CMSAllOurServicesComponent.linkImages</code> attribute. 
	 * @param value the linkImages
	 */
	public void setLinkImages(final CMSAllOurServicesComponent item, final List<CMSLinkImageComponent> value)
	{
		setLinkImages( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to linkImages. 
	 * @param value the item to add to linkImages
	 */
	public void addToLinkImages(final SessionContext ctx, final CMSAllOurServicesComponent item, final CMSLinkImageComponent value)
	{
		item.addLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to linkImages. 
	 * @param value the item to add to linkImages
	 */
	public void addToLinkImages(final CMSAllOurServicesComponent item, final CMSLinkImageComponent value)
	{
		addToLinkImages( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from linkImages. 
	 * @param value the item to remove from linkImages
	 */
	public void removeFromLinkImages(final SessionContext ctx, final CMSAllOurServicesComponent item, final CMSLinkImageComponent value)
	{
		item.removeLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.ALLOURSERVICECOMPONENT2LINKIMGREL,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(ALLOURSERVICECOMPONENT2LINKIMGREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from linkImages. 
	 * @param value the item to remove from linkImages
	 */
	public void removeFromLinkImages(final CMSAllOurServicesComponent item, final CMSLinkImageComponent value)
	{
		removeFromLinkImages( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ComparisonPanelCMSComponent.mandatoryBundleProducts</code> attribute.
	 * @return the mandatoryBundleProducts
	 */
	public List<Product> getMandatoryBundleProducts(final SessionContext ctx, final ComparisonPanelCMSComponent item)
	{
		final List<Product> items = item.getLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			"Product",
			null,
			Utilities.getRelationOrderingOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ComparisonPanelCMSComponent.mandatoryBundleProducts</code> attribute.
	 * @return the mandatoryBundleProducts
	 */
	public List<Product> getMandatoryBundleProducts(final ComparisonPanelCMSComponent item)
	{
		return getMandatoryBundleProducts( getSession().getSessionContext(), item );
	}
	
	public long getMandatoryBundleProductsCount(final SessionContext ctx, final ComparisonPanelCMSComponent item)
	{
		return item.getLinkedItemsCount(
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			"Product",
			null
		);
	}
	
	public long getMandatoryBundleProductsCount(final ComparisonPanelCMSComponent item)
	{
		return getMandatoryBundleProductsCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ComparisonPanelCMSComponent.mandatoryBundleProducts</code> attribute. 
	 * @param value the mandatoryBundleProducts
	 */
	public void setMandatoryBundleProducts(final SessionContext ctx, final ComparisonPanelCMSComponent item, final List<Product> value)
	{
		item.setLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			null,
			value,
			Utilities.getRelationOrderingOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ComparisonPanelCMSComponent.mandatoryBundleProducts</code> attribute. 
	 * @param value the mandatoryBundleProducts
	 */
	public void setMandatoryBundleProducts(final ComparisonPanelCMSComponent item, final List<Product> value)
	{
		setMandatoryBundleProducts( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to mandatoryBundleProducts. 
	 * @param value the item to add to mandatoryBundleProducts
	 */
	public void addToMandatoryBundleProducts(final SessionContext ctx, final ComparisonPanelCMSComponent item, final Product value)
	{
		item.addLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to mandatoryBundleProducts. 
	 * @param value the item to add to mandatoryBundleProducts
	 */
	public void addToMandatoryBundleProducts(final ComparisonPanelCMSComponent item, final Product value)
	{
		addToMandatoryBundleProducts( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from mandatoryBundleProducts. 
	 * @param value the item to remove from mandatoryBundleProducts
	 */
	public void removeFromMandatoryBundleProducts(final SessionContext ctx, final ComparisonPanelCMSComponent item, final Product value)
	{
		item.removeLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.CPANELPRODUCT2PARAGRAPHCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(CPANELPRODUCT2PARAGRAPHCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from mandatoryBundleProducts. 
	 * @param value the item to remove from mandatoryBundleProducts
	 */
	public void removeFromMandatoryBundleProducts(final ComparisonPanelCMSComponent item, final Product value)
	{
		removeFromMandatoryBundleProducts( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ComparisonPanelCMSComponent.products</code> attribute.
	 * @return the products
	 */
	public List<Product> getProducts(final SessionContext ctx, final ComparisonPanelCMSComponent item)
	{
		final List<Product> items = item.getLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			"Product",
			null,
			Utilities.getRelationOrderingOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ComparisonPanelCMSComponent.products</code> attribute.
	 * @return the products
	 */
	public List<Product> getProducts(final ComparisonPanelCMSComponent item)
	{
		return getProducts( getSession().getSessionContext(), item );
	}
	
	public long getProductsCount(final SessionContext ctx, final ComparisonPanelCMSComponent item)
	{
		return item.getLinkedItemsCount(
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			"Product",
			null
		);
	}
	
	public long getProductsCount(final ComparisonPanelCMSComponent item)
	{
		return getProductsCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ComparisonPanelCMSComponent.products</code> attribute. 
	 * @param value the products
	 */
	public void setProducts(final SessionContext ctx, final ComparisonPanelCMSComponent item, final List<Product> value)
	{
		item.setLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			null,
			value,
			Utilities.getRelationOrderingOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ComparisonPanelCMSComponent.products</code> attribute. 
	 * @param value the products
	 */
	public void setProducts(final ComparisonPanelCMSComponent item, final List<Product> value)
	{
		setProducts( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to products. 
	 * @param value the item to add to products
	 */
	public void addToProducts(final SessionContext ctx, final ComparisonPanelCMSComponent item, final Product value)
	{
		item.addLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to products. 
	 * @param value the item to add to products
	 */
	public void addToProducts(final ComparisonPanelCMSComponent item, final Product value)
	{
		addToProducts( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from products. 
	 * @param value the item to remove from products
	 */
	public void removeFromProducts(final SessionContext ctx, final ComparisonPanelCMSComponent item, final Product value)
	{
		item.removeLinkedItems( 
			ctx,
			true,
			AdacfinancialaccestorefrontConstants.Relations.COMPARISONPANELCMSCOMPONENTS2PRODUCTREL,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(COMPARISONPANELCMSCOMPONENTS2PRODUCTREL_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from products. 
	 * @param value the item to remove from products
	 */
	public void removeFromProducts(final ComparisonPanelCMSComponent item, final Product value)
	{
		removeFromProducts( getSession().getSessionContext(), item, value );
	}
	
}
