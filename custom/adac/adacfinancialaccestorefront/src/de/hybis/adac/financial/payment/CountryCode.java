/**
 *
 */
package de.hybis.adac.financial.payment;

import java.util.HashMap;
import java.util.Map;


/**
 * @author MoleSoft-V
 *
 */
public enum CountryCode
{


	DE("Germany", "DEU");

	private static final Map<String, CountryCode> alpha3Map = new HashMap<String, CountryCode>();

	static
	{
		for (final CountryCode cc : values())
		{
			alpha3Map.put(cc.getAlpha3(), cc);
		}
	}


	private final String name;


	private final String alpha3;

	private CountryCode(final String name, final String alpha3)
	{
		this.name = name;
		this.alpha3 = alpha3;
	}


	public String getName()
	{
		return name;
	}

	public String getAlpha2()
	{
		return name();
	}

	public String getAlpha3()
	{
		return alpha3;
	}

	public static CountryCode getByCode(final String code)
	{
		if (code == null)
		{
			return null;
		}

		switch (code.length())
		{
			case 2:
				return getByAlpha2Code(code.toUpperCase());

			case 3:
				return getByAlpha3Code(code.toUpperCase());

			default:
				return null;
		}
	}

	private static CountryCode getByAlpha2Code(final String code)
	{
		try
		{
			return Enum.valueOf(CountryCode.class, code);
		}
		catch (final IllegalArgumentException e)
		{
			return null;
		}
	}

	private static CountryCode getByAlpha3Code(final String code)
	{
		return alpha3Map.get(code);
	}

}
