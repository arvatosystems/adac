/**
 *
 */
package de.hybis.adac.financial.payment;

/**
 * @author MoleSoft-V
 *
 */
public enum IBANEntryType
{
	bank_code, branch_code, account_number, national_check_digit, account_type, owner_account_number, identification_number
}
