
package de.hybis.adac.financial.payment;




/**
 * @author MoleSoft-V
 *
 */

import java.util.List;
import java.util.Random;

import de.hybis.adac.financial.payment.IbanFormatException.IbanFormatViolation;


public final class IBANSplitter
{

	static final String DEFAULT_CHECK_DIGIT = "00";

	// Cache string value of the Iban
	private final String value;

	/**
     * Creates Iban instance.
     *
     * @param value String
     */
	private IBANSplitter(final String value)
	{
        this.value = value;
    }

	/**
	 * Returns Iban's country code.
	 *
	 * @return countryCode CountryCode
	 */
	public CountryCode getCountryCode()
	{
		return CountryCode.getByCode(IBANUtil.getCountryCode(value));
	}

	/**
	 * Returns Iban's check digit.
	 *
	 * @return checkDigit String
	 */
	public String getCheckDigit()
	{
		return IBANUtil.getCheckDigit(value);
	}

	/**
	 * Returns Iban's account number.
	 *
	 * @return accountNumber String
	 */
	public String getAccountNumber()
	{
		return IBANUtil.getAccountNumber(value);
	}

	/**
	 * Returns Iban's bank code.
	 *
	 * @return bankCode String
	 */
	public String getBankCode()
	{
		return IBANUtil.getBankCode(value);
	}

	/**
	 * Returns Iban's branch code.
	 *
	 * @return branchCode String
	 */
	public String getBranchCode()
	{
		return IBANUtil.getBranchCode(value);
	}

	/**
	 * Returns Iban's national check digit.
	 *
	 * @return nationalCheckDigit String
	 */
	public String getNationalCheckDigit()
	{
		return IBANUtil.getNationalCheckDigit(value);
	}

	/**
	 * Returns Iban's account type.
	 *
	 * @return accountType String
	 */
	public String getAccountType()
	{
		return IBANUtil.getAccountType(value);
	}

	/**
	 * Returns Iban's owner account type.
	 *
	 * @return ownerAccountType String
	 */
	public String getOwnerAccountType()
	{
		return IBANUtil.getOwnerAccountType(value);
	}

	/**
	 * Returns Iban's identification number.
	 *
	 * @return identificationNumber String
	 */
	public String getIdentificationNumber()
	{
		return IBANUtil.getIdentificationNumber(value);
	}

	/**
	 * Returns Iban's bban (Basic Bank Account Number).
	 *
	 * @return bban String
	 */
	public String getBban()
	{
		return IBANUtil.getBban(value);
	}

	/**
	 * Returns an Iban object holding the value of the specified String.
	 *
	 * @param Iban
	 *           the String to be parsed.
	 * @return an Iban object holding the value represented by the string argument.
	 * @throws IbanFormatException
	 *            if the String doesn't contain parsable Iban InvalidCheckDigitException if Iban has invalid check digit
	 *            UnsupportedCountryException if Iban's Country is not supported.
	 *
	 */
	public static IBANSplitter valueOf(final String Iban)
			throws IbanFormatException, InvalidCheckDigitException, UnsupportedCountryException
	{
		IBANUtil.validate(Iban);
		return new IBANSplitter(Iban);
	}

	@Override
	public String toString()
	{
		return value;
	}

	/**
	 * Returns formatted version of Iban.
	 *
	 * @return A string representing formatted Iban for printing.
	 */
	public String toFormattedString()
	{
		final StringBuilder IbanBuffer = new StringBuilder(value);
		final int length = IbanBuffer.length();

		for (int i = 0; i < length / 4; i++)
		{
			IbanBuffer.insert((i + 1) * 4 + i, ' ');
		}

		return IbanBuffer.toString().trim();
	}

	public static IBANSplitter random()
	{
		return new IBANSplitter.Builder().buildRandom();
	}

	public static IBANSplitter random(final CountryCode cc)
	{
		return new IBANSplitter.Builder().countryCode(cc).buildRandom();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (obj instanceof IBANSplitter)
		{
			return value.equals(((IBANSplitter) obj).value);
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		return value.hashCode();
	}

	/**
	 * Iban Builder Class
	 */
	public final static class Builder
	{

		private CountryCode countryCode;
		private String bankCode;
		private String branchCode;
		private String nationalCheckDigit;
		private String accountType;
		private String accountNumber;
		private String ownerAccountType;
		private String identificationNumber;

		private final Random random = new Random();

		/**
		 * Creates an Iban Builder instance.
		 */
		public Builder()
		{
		}

		/**
		 * Sets Iban's country code.
		 *
		 * @param countryCode
		 *           CountryCode
		 * @return builder Builder
		 */
		public Builder countryCode(final CountryCode countryCode)
		{
			this.countryCode = countryCode;
			return this;
		}

		/**
		 * Sets Iban's bank code.
		 *
		 * @param bankCode
		 *           String
		 * @return builder Builder
		 */
		public Builder bankCode(final String bankCode)
		{
			this.bankCode = bankCode;
			return this;
		}

		/**
		 * Sets Iban's branch code.
		 *
		 * @param branchCode
		 *           String
		 * @return builder Builder
		 */
		public Builder branchCode(final String branchCode)
		{
			this.branchCode = branchCode;
			return this;
		}

		/**
		 * Sets Iban's account number.
		 *
		 * @param accountNumber
		 *           String
		 * @return builder Builder
		 */
		public Builder accountNumber(final String accountNumber)
		{
			this.accountNumber = accountNumber;
			return this;
		}

		/**
		 * Sets Iban's national check digit.
		 *
		 * @param nationalCheckDigit
		 *           String
		 * @return builder Builder
		 */
		public Builder nationalCheckDigit(final String nationalCheckDigit)
		{
			this.nationalCheckDigit = nationalCheckDigit;
			return this;
		}

		/**
		 * Sets Iban's account type.
		 *
		 * @param accountType
		 *           String
		 * @return builder Builder
		 */
		public Builder accountType(final String accountType)
		{
			this.accountType = accountType;
			return this;
		}

		/**
		 * Sets Iban's owner account type.
		 *
		 * @param ownerAccountType
		 *           String
		 * @return builder Builder
		 */
		public Builder ownerAccountType(final String ownerAccountType)
		{
			this.ownerAccountType = ownerAccountType;
			return this;
		}

		/**
		 * Sets Iban's identification number.
		 *
		 * @param identificationNumber
		 *           String
		 * @return builder Builder
		 */
		public Builder identificationNumber(final String identificationNumber)
		{
			this.identificationNumber = identificationNumber;
			return this;
		}

		/**
		 * Builds new Iban instance. This methods validates the generated Iban.
		 *
		 * @return new Iban instance.
		 * @throws IbanFormatException,
		 *            UnsupportedCountryException if values are not parsable by Iban Specification
		 *            <a href="http://en.wikipedia.org/wiki/ISO_13616">ISO_13616</a>
		 */
		public IBANSplitter build() throws IbanFormatException, IllegalArgumentException, UnsupportedCountryException
		{
			return build(true);
		}

		/**
		 * Builds new Iban instance.
		 *
		 * @param validate
		 *           boolean indicates if the generated Iban needs to be validated after generation
		 * @return new Iban instance.
		 * @throws IbanFormatException,
		 *            UnsupportedCountryException if values are not parsable by Iban Specification
		 *            <a href="http://en.wikipedia.org/wiki/ISO_13616">ISO_13616</a>
		 */
		public IBANSplitter build(final boolean validate)
				throws IbanFormatException, IllegalArgumentException, UnsupportedCountryException
		{

			// null checks
			require(countryCode, bankCode, accountNumber);

			// Iban is formatted with default check digit.
			final String formattedIban = formatIban();

			final String checkDigit = IBANUtil.calculateCheckDigit(formattedIban);

			// replace default check digit with calculated check digit
			final String IbanValue = IBANUtil.replaceCheckDigit(formattedIban, checkDigit);

			if (validate)
			{
				IBANUtil.validate(IbanValue);
			}
			return new IBANSplitter(IbanValue);
		}

		public IBANSplitter buildRandom() throws IbanFormatException, IllegalArgumentException, UnsupportedCountryException
		{
			if (countryCode == null)
			{
				final List<CountryCode> countryCodes = IBANStructure.supportedCountries();
				this.countryCode(countryCodes.get(random.nextInt(countryCodes.size())));
			}
			fillMissingFieldsRandomly();
			return build();
		}

		private String formatBban()
		{
			final StringBuilder sb = new StringBuilder();
			final IBANStructure structure = IBANStructure.forCountry(countryCode);

			if (structure == null)
			{
				throw new UnsupportedCountryException(countryCode.toString(), "Country code is not supported.");
			}

			for (final IBANStructureEntry entry : structure.getEntries())
			{
				switch (entry.getEntryType())
				{
					case bank_code:
						sb.append(bankCode);
						break;
					case branch_code:
						sb.append(branchCode);
						break;
					case account_number:
						sb.append(accountNumber);
						break;
					case national_check_digit:
						sb.append(nationalCheckDigit);
						break;
					case account_type:
						sb.append(accountType);
						break;
					case owner_account_number:
						sb.append(ownerAccountType);
						break;
					case identification_number:
						sb.append(identificationNumber);
						break;
				}
			}
			return sb.toString();
		}

		/**
		 * Returns formatted Iban string with default check digit.
		 */
		private String formatIban()
		{
			final StringBuilder sb = new StringBuilder();
			sb.append(countryCode.getAlpha2());
			sb.append(DEFAULT_CHECK_DIGIT);
			sb.append(formatBban());
			return sb.toString();
		}

		private void require(final CountryCode countryCode, final String bankCode, final String accountNumber)
				throws IbanFormatException
		{
			if (countryCode == null)
			{
				throw new IbanFormatException(IbanFormatViolation.COUNTRY_CODE_NOT_NULL,
						"countryCode is required; it cannot be null");
			}

			if (bankCode == null)
			{
				throw new IbanFormatException(IbanFormatViolation.BANK_CODE_NOT_NULL, "bankCode is required; it cannot be null");
			}

			if (accountNumber == null)
			{
				throw new IbanFormatException(IbanFormatViolation.ACCOUNT_NUMBER_NOT_NULL,
						"accountNumber is required; it cannot be null");
			}
		}

		private void fillMissingFieldsRandomly()
		{
			final IBANStructure structure = IBANStructure.forCountry(countryCode);

			if (structure == null)
			{
				throw new UnsupportedCountryException(countryCode.toString(), "Country code is not supported.");
			}

			for (final IBANStructureEntry entry : structure.getEntries())
			{
				switch (entry.getEntryType())
				{
					case bank_code:
						if (bankCode == null)
						{
							bankCode = entry.getRandom();
						}
						break;
					case branch_code:
						if (branchCode == null)
						{
							branchCode = entry.getRandom();
						}
						break;
					case account_number:
						if (accountNumber == null)
						{
							accountNumber = entry.getRandom();
						}
						break;
					case national_check_digit:
						if (nationalCheckDigit == null)
						{
							nationalCheckDigit = entry.getRandom();
						}
						break;
					case account_type:
						if (accountType == null)
						{
							accountType = entry.getRandom();
						}
						break;
					case owner_account_number:
						if (ownerAccountType == null)
						{
							ownerAccountType = entry.getRandom();
						}
						break;
					case identification_number:
						if (identificationNumber == null)
						{
							identificationNumber = entry.getRandom();
						}
						break;
				}
			}
		}

	}

}
