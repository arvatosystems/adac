/**
 *
 */
package de.hybis.adac.financial.payment;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;


/**
 * @author MoleSoft-V
 *
 */
public class IBANStructure
{

	private final IBANStructureEntry[] entries;

	private IBANStructure(final IBANStructureEntry... entries)
	{
		this.entries = entries;
	}


	private static final EnumMap<CountryCode, IBANStructure> structures;

	static
	{
		structures = new EnumMap<CountryCode, IBANStructure>(CountryCode.class);


		structures.put(CountryCode.DE,
				new IBANStructure(IBANStructureEntry.bankCode(8, 'n'), IBANStructureEntry.accountNumber(10, 'n')));



	}

	/**
	 * @param countryCode
	 *           the country code.
	 * @return IBANStructure for specified country or null if country is not supported.
	 */
	public static IBANStructure forCountry(final CountryCode countryCode)
	{
		return structures.get(countryCode);
	}

	public List<IBANStructureEntry> getEntries()
	{
		return Collections.unmodifiableList(Arrays.asList(entries));
	}

	public static List<CountryCode> supportedCountries()
	{
		final List<CountryCode> countryCodes = new ArrayList<CountryCode>(structures.size());
		countryCodes.addAll(structures.keySet());
		return Collections.unmodifiableList(countryCodes);
	}

	/**
	 * Returns the length of bban.
	 *
	 * @return int length
	 */
	public int getBbanLength()
	{
		int length = 0;

		for (final IBANStructureEntry entry : entries)
		{
			length += entry.getLength();
		}

		return length;
	}



}
