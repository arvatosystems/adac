/**
 *
 */
package de.hybis.adac.financial.payment;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * @author MoleSoft-V
 *
 */
public class IBANStructureEntry
{

	private final IBANEntryType entryType;
	private final EntryCharacterType characterType;
	private final int length;

	private static Map<EntryCharacterType, char[]> charByCharacterType;
	private final Random random = new Random();

	static
	{
		charByCharacterType = new HashMap<EntryCharacterType, char[]>();
		final StringBuilder charTypeN = new StringBuilder();
		for (char ch = '0'; ch <= '9'; ch++)
		{
			charTypeN.append(ch);
		}
		charByCharacterType.put(EntryCharacterType.n, charTypeN.toString().toCharArray());

		final StringBuilder charTypeA = new StringBuilder();
		for (char ch = 'A'; ch <= 'Z'; ++ch)
		{
			charTypeA.append(ch);
		}
		charByCharacterType.put(EntryCharacterType.a, charTypeA.toString().toCharArray());

		charByCharacterType.put(EntryCharacterType.c, (charTypeN.toString() + charTypeA.toString()).toCharArray());
	}

	private IBANStructureEntry(final IBANEntryType entryType,
                      final EntryCharacterType characterType,
                      final int length) {
       this.entryType = entryType;
       this.characterType = characterType;
       this.length = length;
   }

	public static IBANStructureEntry bankCode(final int length, final char characterType)
	{
		return new IBANStructureEntry(IBANEntryType.bank_code, EntryCharacterType.valueOf(String.valueOf(characterType)), length);
	}

	public static IBANStructureEntry branchCode(final int length, final char characterType)
	{
		return new IBANStructureEntry(IBANEntryType.branch_code, EntryCharacterType.valueOf(String.valueOf(characterType)), length);
	}

	public static IBANStructureEntry accountNumber(final int length, final char characterType)
	{
		return new IBANStructureEntry(IBANEntryType.account_number, EntryCharacterType.valueOf(String.valueOf(characterType)),
				length);
	}

	public static IBANStructureEntry nationalCheckDigit(final int length, final char characterType)
	{
		return new IBANStructureEntry(IBANEntryType.national_check_digit, EntryCharacterType.valueOf(String.valueOf(characterType)),
				length);
	}

	public static IBANStructureEntry accountType(final int length, final char characterType)
	{
		return new IBANStructureEntry(IBANEntryType.account_type, EntryCharacterType.valueOf(String.valueOf(characterType)),
				length);
	}

	public static IBANStructureEntry ownerAccountNumber(final int length, final char characterType)
	{
		return new IBANStructureEntry(IBANEntryType.owner_account_number, EntryCharacterType.valueOf(String.valueOf(characterType)),
				length);
	}

	public static IBANStructureEntry identificationNumber(final int length, final char characterType)
	{
		return new IBANStructureEntry(IBANEntryType.identification_number,
				EntryCharacterType.valueOf(String.valueOf(characterType)), length);
	}

	public IBANEntryType getEntryType()
	{
		return entryType;
	}

	public EntryCharacterType getCharacterType()
	{
		return characterType;
	}

	public int getLength()
	{
		return length;
	}

	public enum EntryCharacterType
	{
		n, // Digits (numeric characters 0 to 9 only)
		a, // Upper case letters (alphabetic characters A-Z only)
		c // upper and lower case alphanumeric characters (A-Z, a-z and 0-9)
	}

	public String getRandom()
	{
		final StringBuilder s = new StringBuilder("");
		final char[] charChoices = charByCharacterType.get(characterType);
		if (charChoices == null)
		{
			throw new RuntimeException(
					String.format("programmer has not implemented choices for character type %s", characterType.name()));
		}
		for (int i = 0; i < getLength(); i++)
		{
			s.append(charChoices[random.nextInt(charChoices.length)]);
		}
		return s.toString();
	}
}
