/**
 *
 */
package de.hybis.adac.financial.payment;

import de.hybis.adac.financial.payment.IbanFormatException.IbanFormatViolation;


/**
 * @author MoleSoft-V
 *
 */

public class IBANUtil
{

	private static final int MOD = 97;
	private static final long MAX = 999999999;

	private static final int COUNTRY_CODE_INDEX = 0;
	private static final int COUNTRY_CODE_LENGTH = 2;
	private static final int CHECK_DIGIT_INDEX = COUNTRY_CODE_LENGTH;
	private static final int CHECK_DIGIT_LENGTH = 2;
	private static final int BBAN_INDEX = CHECK_DIGIT_INDEX + CHECK_DIGIT_LENGTH;

	private static final String ASSERT_UPPER_LETTERS = "[%s] must contain only upper case letters.";
	private static final String ASSERT_DIGITS_AND_LETTERS = "[%s] must contain only digits or letters.";
	private static final String ASSERT_DIGITS = "[%s] must contain only digits.";


	/**
	 * Calculates Iban.
	 *
	 * @param iban
	 *           string value
	 * @throws IbanFormatException
	 *            if iban contains invalid character.
	 *
	 * @return check digit as String
	 */
	public static String calculateCheckDigit(final String iban) throws IbanFormatException
	{
		final String reformattedIban = replaceCheckDigit(iban, IBANSplitter.DEFAULT_CHECK_DIGIT);
		final int modResult = calculateMod(reformattedIban);
		final int checkDigitIntValue = (98 - modResult);
		final String checkDigit = Integer.toString(checkDigitIntValue);
		return checkDigitIntValue > 9 ? checkDigit : "0" + checkDigit;
	}

	public static void validate(final String iban)
			throws IbanFormatException, InvalidCheckDigitException, UnsupportedCountryException
	{
		try
		{
			validateEmpty(iban);
			validateCountryCode(iban);
			validateCheckDigitPresence(iban);

			final IBANStructure structure = getIBANStructure(iban);

			validateBbanLength(iban, structure);
			validateBbanEntries(iban, structure);

			validateCheckDigit(iban);
		}
		catch (final Iban4jException e)
		{
			throw e;
		}
		catch (final RuntimeException e)
		{
			throw new IbanFormatException(IbanFormatViolation.UNKNOWN, e.getMessage());
		}
	}


	public static boolean isSupportedCountry(final CountryCode countryCode)
	{
		return IBANStructure.forCountry(countryCode) != null;
	}

	public static int getIbanLength(final CountryCode countryCode)
	{
		final IBANStructure structure = getIBANStructure(countryCode);
		return COUNTRY_CODE_LENGTH + CHECK_DIGIT_LENGTH + structure.getBbanLength();
	}

	public static String getCheckDigit(final String iban)
	{
		return iban.substring(CHECK_DIGIT_INDEX, CHECK_DIGIT_INDEX + CHECK_DIGIT_LENGTH);
	}

	public static String getCountryCode(final String iban)
	{
		return iban.substring(COUNTRY_CODE_INDEX, COUNTRY_CODE_INDEX + COUNTRY_CODE_LENGTH);
	}

	public static String getCountryCodeAndCheckDigit(final String iban)
	{
		return iban.substring(COUNTRY_CODE_INDEX, COUNTRY_CODE_INDEX + COUNTRY_CODE_LENGTH + CHECK_DIGIT_LENGTH);
	}

	public static String getBban(final String iban)
	{
		return iban.substring(BBAN_INDEX);
	}

	public static String getAccountNumber(final String iban)
	{
		return extractBbanEntry(iban, IBANEntryType.account_number);
	}

	public static String getBankCode(final String iban)
	{
		return extractBbanEntry(iban, IBANEntryType.bank_code);
	}

	static String getBranchCode(final String iban)
	{
		return extractBbanEntry(iban, IBANEntryType.branch_code);
	}

	static String getNationalCheckDigit(final String iban)
	{
		return extractBbanEntry(iban, IBANEntryType.national_check_digit);
	}

	static String getAccountType(final String iban)
	{
		return extractBbanEntry(iban, IBANEntryType.account_type);
	}

	static String getOwnerAccountType(final String iban)
	{
		return extractBbanEntry(iban, IBANEntryType.owner_account_number);
	}

	static String getIdentificationNumber(final String iban)
	{
		return extractBbanEntry(iban, IBANEntryType.identification_number);
	}

	static String calculateCheckDigit(final IBANSplitter iban)
	{
		return calculateCheckDigit(iban.toString());
	}

	static String replaceCheckDigit(final String iban, final String checkDigit)
	{
		return getCountryCode(iban) + checkDigit + getBban(iban);
	}



	private static void validateCheckDigit(final String iban)
	{
		if (calculateMod(iban) != 1)
		{
			final String checkDigit = getCheckDigit(iban);
			final String expectedCheckDigit = calculateCheckDigit(iban);
			throw new InvalidCheckDigitException(checkDigit, expectedCheckDigit, String.format(
					"[%s] has invalid check digit: %s, " + "expected check digit is: %s", iban, checkDigit, expectedCheckDigit));
		}
	}

	private static void validateEmpty(final String iban)
	{
		if (iban == null)
		{
			throw new IbanFormatException(IbanFormatViolation.IBAN_NOT_NULL, "Null can't be a valid Iban.");
		}

		if (iban.length() == 0)
		{
			throw new IbanFormatException(IbanFormatViolation.IBAN_NOT_EMPTY, "Empty string can't be a valid Iban.");
		}
	}

	private static void validateCountryCode(final String iban)
	{
		// check if iban contains 2 char country code
		if (iban.length() < COUNTRY_CODE_LENGTH)
		{
			throw new IbanFormatException(IbanFormatViolation.COUNTRY_CODE_TWO_LETTERS, iban,
					"Iban must contain 2 char country code.");
		}

		final String countryCode = getCountryCode(iban);

		// check case sensitivity
		if (!countryCode.equals(countryCode.toUpperCase()) || !Character.isLetter(countryCode.charAt(0))
				|| !Character.isLetter(countryCode.charAt(1)))
		{
			throw new IbanFormatException(IbanFormatViolation.COUNTRY_CODE_UPPER_CASE_LETTERS, countryCode,
					"Iban country code must contain upper case letters.");
		}

		if (CountryCode.getByCode(countryCode) == null)
		{
			throw new IbanFormatException(IbanFormatViolation.COUNTRY_CODE_EXISTS, countryCode,
					"Iban contains non existing country code.");
		}

		// check if country is supported
		final IBANStructure structure = IBANStructure.forCountry(CountryCode.getByCode(countryCode));
		if (structure == null)
		{
			throw new UnsupportedCountryException(countryCode, "Country code is not supported.");
		}
	}

	private static void validateCheckDigitPresence(final String iban)
	{
		// check if iban contains 2 digit check digit
		if (iban.length() < COUNTRY_CODE_LENGTH + CHECK_DIGIT_LENGTH)
		{
			throw new IbanFormatException(IbanFormatViolation.CHECK_DIGIT_TWO_DIGITS, iban.substring(COUNTRY_CODE_LENGTH),
					"Iban must contain 2 digit check digit.");
		}

		final String checkDigit = getCheckDigit(iban);

		// check digits
		if (!Character.isDigit(checkDigit.charAt(0)) || !Character.isDigit(checkDigit.charAt(1)))
		{
			throw new IbanFormatException(IbanFormatViolation.CHECK_DIGIT_ONLY_DIGITS, checkDigit,
					"Iban's check digit should contain only digits.");
		}
	}

	private static void validateBbanLength(final String iban, final IBANStructure structure)
	{
		final int expectedBbanLength = structure.getBbanLength();
		final String bban = getBban(iban);
		final int bbanLength = bban.length();
		if (expectedBbanLength != bbanLength)
		{
			throw new IbanFormatException(IbanFormatViolation.BBAN_LENGTH, bbanLength, expectedBbanLength,
					String.format("[%s] length is %d, expected BBAN length is: %d", bban, bbanLength, expectedBbanLength));
		}
	}

	private static void validateBbanEntries(final String iban, final IBANStructure structure)
	{
		final String bban = getBban(iban);
		int bbanEntryOffset = 0;
		for (final IBANStructureEntry entry : structure.getEntries())
		{
			final int entryLength = entry.getLength();
			final String entryValue = bban.substring(bbanEntryOffset, bbanEntryOffset + entryLength);

			bbanEntryOffset = bbanEntryOffset + entryLength;

			// validate character type
			validateBbanEntryCharacterType(entry, entryValue);
		}
	}

	private static void validateBbanEntryCharacterType(final IBANStructureEntry entry, final String entryValue)
	{
		switch (entry.getCharacterType())
		{
			case a:
				for (final char ch : entryValue.toCharArray())
				{
					if (!Character.isUpperCase(ch))
					{
						throw new IbanFormatException(IbanFormatViolation.BBAN_ONLY_UPPER_CASE_LETTERS, entry.getEntryType(),
								entryValue, ch,
								String.format(ASSERT_UPPER_LETTERS, entryValue));
					}
				}
				break;
			case c:
				for (final char ch : entryValue.toCharArray())
				{
					if (!Character.isLetterOrDigit(ch))
					{
						throw new IbanFormatException(IbanFormatViolation.BBAN_ONLY_DIGITS_OR_LETTERS, entry.getEntryType(), entryValue,
								ch,
								String.format(ASSERT_DIGITS_AND_LETTERS, entryValue));
					}
				}
				break;
			case n:
				for (final char ch : entryValue.toCharArray())
				{
					if (!Character.isDigit(ch))
					{
						throw new IbanFormatException(IbanFormatViolation.BBAN_ONLY_DIGITS, entry.getEntryType(), entryValue, ch,
								String.format(ASSERT_DIGITS, entryValue));
					}
				}
				break;
		}
	}

	private static int calculateMod(final String iban)
	{
		final String reformattedIban = getBban(iban) + getCountryCodeAndCheckDigit(iban);
		long total = 0;
		for (int i = 0; i < reformattedIban.length(); i++)
		{
			final int numericValue = Character.getNumericValue(reformattedIban.charAt(i));
			if (numericValue < 0 || numericValue > 35)
			{
				throw new IbanFormatException(IbanFormatViolation.IBAN_VALID_CHARACTERS, null, null, reformattedIban.charAt(i),
						String.format("Invalid Character[%d] = '%d'", i, numericValue));
			}
			total = (numericValue > 9 ? total * 100 : total * 10) + numericValue;

			if (total > MAX)
			{
				total = (total % MOD);
			}

		}
		return (int) (total % MOD);
	}

	private static IBANStructure getIBANStructure(final String iban)
	{
		final String countryCode = getCountryCode(iban);
		return getIBANStructure(CountryCode.getByCode(countryCode));
	}

	private static IBANStructure getIBANStructure(final CountryCode countryCode)
	{
		return IBANStructure.forCountry(countryCode);
	}

	private static String extractBbanEntry(final String iban, final IBANEntryType entryType)
	{
		final String bban = getBban(iban);
		final IBANStructure structure = getIBANStructure(iban);
		int bbanEntryOffset = 0;
		for (final IBANStructureEntry entry : structure.getEntries())
		{
			final int entryLength = entry.getLength();
			final String entryValue = bban.substring(bbanEntryOffset, bbanEntryOffset + entryLength);

			bbanEntryOffset = bbanEntryOffset + entryLength;
			if (entry.getEntryType() == entryType)
			{
				return entryValue;
			}
		}
		return null;
	}

}
