/**
 *
 */
package de.hybris.adac.financial.banking.facades;

import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;

import java.util.List;


/**
 * @author MoleSoft-V
 *
 */
public interface DebitPaymentInfoFacade
{

	List<PaymentInfoData> getDebitPaymentInfos(boolean saved);

}
