/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */


package de.hybris.adac.financial.banking.facades;

import de.hybris.platform.bankingfacades.data.FSBankAccountData;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;


/**
 * The interface Fs bank account facade.
 */
public interface FSBankAccountFacade
{

	/**
	 * Returns the backAccount list of the particular customer logged in to the site
	 *
	 * @param accountType    the account type
	 * @param paginationData the pagination data
	 * @return SearchPageData<FSBankAccountData> bank account list for current customer
	 */
	SearchPageData<FSBankAccountData> getBankAccountListForCurrentCustomer(String accountType, PaginationData paginationData);


}