/**
 *
 */
package de.hybris.adac.financial.banking.facades.impl;

import de.hybris.adac.facades.financial.converter.DebitPaymentInfoConverter;
import de.hybris.adac.financial.banking.facades.DebitPaymentInfoFacade;
import de.hybris.adac.financial.service.ADACCustomerAccountService;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author MoleSoft-V
 *
 */
public class DebitPaymentInfoFacadeImpl implements DebitPaymentInfoFacade
{

	@Autowired
	private ADACCustomerAccountService adacCustomerAccountService;

	@Autowired
	private UserService userService;

	//private Converter<DebitPaymentInfoModel, PaymentInfoData> debitPaymentInfoConverter;

	@Override
	public List<PaymentInfoData> getDebitPaymentInfos(final boolean saved)
	{

		final CustomerModel currentCustomer = (CustomerModel) userService.getCurrentUser();

		final List<DebitPaymentInfoModel> debitPaymentInfo = adacCustomerAccountService.getDebitCardPaymentInfos(currentCustomer,
				saved);

		final List<PaymentInfoData> paymentInfos = new ArrayList<PaymentInfoData>();

		final PaymentInfoModel defaultPaymentInfoModel = currentCustomer.getDefaultPaymentInfo();

		for (final DebitPaymentInfoModel debitPaymentInfoModel : debitPaymentInfo)
		{
			final DebitPaymentInfoConverter paymentConverter = new DebitPaymentInfoConverter();
			//final PaymentInfoData paymentInfoData = getDebitPaymentInfoConverter().convert(debitPaymentInfoModel);
			final PaymentInfoData paymentInfoData = new PaymentInfoData();
			paymentConverter.convert(debitPaymentInfoModel, paymentInfoData);

				if (debitPaymentInfoModel.equals(defaultPaymentInfoModel))
				{
					paymentInfos.add(0, paymentInfoData);
				}
				else
				{
					paymentInfos.add(paymentInfoData);
				}

		}

		return paymentInfos;

	}


	//	/**
	//	 * @return the debitPaymentInfoConverter
	//	 */
	//	public Converter<DebitPaymentInfoModel, PaymentInfoData> getDebitPaymentInfoConverter()
	//	{
	//		return debitPaymentInfoConverter;
	//	}
	//
	//	/**
	//	 * @param debitPaymentInfoConverter
	//	 *           the debitPaymentInfoConverter to set
	//	 */
	//	public void setDebitPaymentInfoConverter(final Converter<DebitPaymentInfoModel, PaymentInfoData> debitPaymentInfoConverter)
	//	{
	//		this.debitPaymentInfoConverter = debitPaymentInfoConverter;
	//	}



}
