/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.banking.facades.impl;

import de.hybris.platform.bankingfacades.data.FSBankAccountData;
import de.hybris.platform.bankingfacades.data.FSBankAccountRequestData;
import de.hybris.platform.bankingfacades.data.FSBankAccountResponseData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.adac.financial.banking.facades.FSBankAccountFacade;
import de.hybris.adac.financial.banking.facades.FSBankAccountOverviewIntegration;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;



/**
 * This class returns the list of bank accounts of the current customer logged in to the site
 */
public class DefaultFSBankAccountFacade implements FSBankAccountFacade
{

	private UserService userService;
	private Converter<FSBankAccountResponseData, FSBankAccountData> bankAccountIntegrationConverter;
	private FSBankAccountOverviewIntegration fsBankAccountOverviewIntegration;

	/**
	 * Retrieves the bank account details from the integration service based on the current customer and account category
	 *
	 * @param customerId
	 * @param accountCategory
	 * @param paginationData
	 * @return SearchPageData<FSBankAccountResponseData>
	 */
	private SearchPageData<FSBankAccountResponseData> retrieveBankAccountList(final String customerId,
			final String accountCategory, final PaginationData paginationData)
	{
		final FSBankAccountRequestData requestData = new FSBankAccountRequestData();
		requestData.setCustomerId(customerId);
		requestData.setAccountCategory(accountCategory);
		final SearchPageData<FSBankAccountResponseData> bankAccountResponseData = getFsBankAccountOverviewIntegration()
				.getBankAccounts(requestData, paginationData);
		return bankAccountResponseData;
	}

	/**
	 * Fetches the bank account response data and converts it to the bank account data (non-Javadoc)
	 *
	 * @param accountCategory
	 * @param paginationData
	 * @return SearchPageData<FSBankAccountData>
	 */
	@Override
	public SearchPageData<FSBankAccountData> getBankAccountListForCurrentCustomer(final String accountType,
			final PaginationData paginationData)
	{
		final UserModel user = getUserService().getCurrentUser();
		final SearchPageData<FSBankAccountResponseData> bankAccountSearchData = retrieveBankAccountList(user.getUid(), accountType,
				paginationData);
		final List<FSBankAccountData> bankAccountDetails = new ArrayList<FSBankAccountData>();
		bankAccountDetails.addAll(Converters.convertAll(bankAccountSearchData.getResults(), getBankAccountIntegrationConverter()));
		final SearchPageData<FSBankAccountData> result = new SearchPageData<>();
		if (bankAccountSearchData.getPagination() != null)
		{
			result.setPagination(bankAccountSearchData.getPagination());
		}
		Collections.sort(bankAccountDetails, new Comparator<FSBankAccountData>()
		{
			@Override
			public int compare(final FSBankAccountData bankAccount1, final FSBankAccountData bankAccount2)
			{
				return bankAccount1.getAccountType().compareTo(bankAccount2.getAccountType());
			}
		});

		result.setResults(bankAccountDetails);
		return result;
	}

	protected Converter<FSBankAccountResponseData, FSBankAccountData> getBankAccountIntegrationConverter()
	{
		return bankAccountIntegrationConverter;
	}


	public void setBankAccountIntegrationConverter(
			final Converter<FSBankAccountResponseData, FSBankAccountData> bankAccountIntegrationConverter)
	{
		this.bankAccountIntegrationConverter = bankAccountIntegrationConverter;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	protected FSBankAccountOverviewIntegration getFsBankAccountOverviewIntegration()
	{
		return fsBankAccountOverviewIntegration;
	}

	public void setFsBankAccountOverviewIntegration(final FSBankAccountOverviewIntegration fsBankAccountOverviewIntegration)
	{
		this.fsBankAccountOverviewIntegration = fsBankAccountOverviewIntegration;
	}



}