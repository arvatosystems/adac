/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.banking.facades.impl;

import de.hybris.platform.bankingfacades.data.FSBankAccountRequestData;
import de.hybris.platform.bankingfacades.data.FSBankAccountResponseData;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.adac.financial.banking.facades.FSBankAccountOverviewIntegration;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class MockFSBankAccountOverviewIntegration implements FSBankAccountOverviewIntegration
{
	protected static final String XML_DISABLE_DOCTYPE_DECL = "http://apache.org/xml/features/disallow-doctype-decl";
	protected static final String XML_DISABLE_EXTERNAL_GENERAL_ENTITIES = "http://xml.org/sax/features/external-general-entities";
	protected static final String XML_DISABLE_EXTERNAL_PARAMETER_ENTITIES = "http://xml.org/sax/features/external-parameter-entities";
	private static final String BANK_ACCOUNTOVERVIEW_MOCK_DATA = "bank.accountoverview.mock.data";
	private static final Logger LOG = Logger.getLogger(MockFSBankAccountOverviewIntegration.class);
	private ConfigurationService configurationService;
	private String dateFormatForDisplay;

	private static String getStringValue(final String tag, final Element element)
	{
		if (element != null && element.getElementsByTagName(tag).getLength() > 0)
		{
			final NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
			if (nodes.getLength() > 0 && nodes.item(0) != null)
			{
				return nodes.item(0).getNodeValue();
			}
		}
		return null;
	}

	public static void safeClose(final InputStream fis)
	{
		if (fis != null)
		{
			try
			{
				fis.close();
			}
			catch (final IOException e)
			{
				LOG.warn("Could not close fileInputStream" + e);
			}
		}
	}

	/**
	 * sets bank account details based on the account type
	 *
	 * @param element
	 * @param accountType
	 * @return FSBankAccountResponseData
	 */
	private FSBankAccountResponseData getAccountData(final Element element, final String accountType)
	{
		final FSBankAccountResponseData responseData = new FSBankAccountResponseData();
		responseData.setAccountNumber(getStringValue("AccountNumber", element));
		responseData.setAccountType(accountType);
		responseData.setLimit(getStringValue("Limit", element));
		responseData.setAvailableBalance(getStringValue("AvailableBalance", element));
		responseData.setCurrentBalance(getStringValue("CurrentBalance", element));
		final LocalDate fmtNextStatementDate = getFormmatedDate(getStringValue("NextStatementDate", element));
		if (fmtNextStatementDate != null)
		{
			responseData.setNextStatementDate(Date.valueOf(fmtNextStatementDate));
		}
		responseData.setAvailableCredit(getStringValue("AvailableCredit", element));
		responseData.setUnbilledAmount(getStringValue("UnbilledAmount", element));
		final LocalDate fmtNextDueDate = getFormmatedDate(getStringValue("NextDueDate", element));
		if (fmtNextDueDate != null)
		{
			responseData.setNextDueDate(Date.valueOf(fmtNextDueDate));
		}
		responseData.setInstallmentAmount(getStringValue("InstallmentAmount", element));
		responseData.setEffectiveCapital(getStringValue("EffectiveCapital", element));
		responseData.setCurrencyCode(getStringValue("CurrencyCode", element));
		return responseData;
	}

	/**
	 * Fetches the mock bank account data from the xml file based on the current customer and account category
	 *
	 * @param bankAccountRequestData
	 * @param paginationData
	 * @return FSBankAccountResponseData
	 */
	@Override
	public SearchPageData<FSBankAccountResponseData> getBankAccounts(final FSBankAccountRequestData bankAccountRequestData,
			final PaginationData paginationData)
	{
		InputStream fsaTxtInputStream = null;
		Document doc = null;
		validateParameterNotNullStandardMessage("FSBankAccountRequestData cannot be null.", bankAccountRequestData);
		final List<FSBankAccountResponseData> bankAccountResponseData = new ArrayList<FSBankAccountResponseData>();
		try
		{
			fsaTxtInputStream = new DataInputStream(MockFSBankAccountOverviewIntegration.class
					.getResourceAsStream(getConfigurationService().getConfiguration().getString(BANK_ACCOUNTOVERVIEW_MOCK_DATA)));
			final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = null;
			try
			{
				dbFactory.setFeature(XML_DISABLE_DOCTYPE_DECL, true);
				dbFactory.setFeature(XML_DISABLE_EXTERNAL_GENERAL_ENTITIES, false);
				dbFactory.setFeature(XML_DISABLE_EXTERNAL_PARAMETER_ENTITIES, false);
				dBuilder = dbFactory.newDocumentBuilder();
				doc = dBuilder.parse(fsaTxtInputStream);
			}
			catch (final ParserConfigurationException | SAXException | IOException e)
			{
				LOG.error(e.getMessage(), e);
			}
		}
		finally
		{
			if (fsaTxtInputStream != null)
			{
				safeClose(fsaTxtInputStream);
			}
		}
		try
		{
			if (doc != null)
			{
				doc.getDocumentElement().normalize();
				final NodeList nodes = doc.getElementsByTagName("FSBankAccountResponseData");
				for (int i = 0; i < nodes.getLength(); i++)
				{
					final Node node = nodes.item(i);
					if (Node.ELEMENT_NODE == node.getNodeType())
					{
						final Element element = (Element) node;
						final String accountType = getStringValue("AccountType", element);
						final String customerId = getStringValue("CustomerId", element);
						if ((accountType != null && accountType.contains(bankAccountRequestData.getAccountCategory()))
								|| bankAccountRequestData.getAccountCategory()
								.equalsIgnoreCase(AdacfinancialaccestorefrontConstants.ALL)
								&& (customerId != null && customerId.equalsIgnoreCase(bankAccountRequestData.getCustomerId())))
						{
							final FSBankAccountResponseData responseData = getAccountData(element, accountType);
							bankAccountResponseData.add(responseData);
						}
					}
				}
			}
		}
		catch (final NullPointerException e)
		{
			LOG.error("invalid xml" + e);
		}

		if (paginationData != null && paginationData.getPageSize() != 0)
		{

			final SearchPageData<FSBankAccountResponseData> result = new SearchPageData<>();
			final List<FSBankAccountResponseData> responseData = bankAccountResponseData.stream()
					.skip(paginationData.getCurrentPage() * (long) paginationData.getPageSize()).limit(paginationData.getPageSize())
					.collect(Collectors.toList());
			result.setPagination(paginationData);
			result.setResults(responseData);
			return result;
		}
		else
		{
			final SearchPageData<FSBankAccountResponseData> result = new SearchPageData<>();
			result.setResults(bankAccountResponseData);
			return result;
		}
	}

	public void setDateFormatForDisplay(final String dateFormatForDisplay)
	{
		this.dateFormatForDisplay = dateFormatForDisplay;
	}

	private DateTimeFormatter getDateTimeFormatter()
	{
		return DateTimeFormatter.ofPattern(dateFormatForDisplay);
	}

	private LocalDate getFormmatedDate(final String date)
	{
		try
		{
			if (StringUtils.isNotEmpty(date))
			{
				return LocalDate.parse(date, getDateTimeFormatter());
			}
		}
		catch (final DateTimeParseException ex)
		{
			LOG.error("Failed to parse dates.", ex);
		}
		return null;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
