/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.banking.populators;

import de.hybris.platform.bankingfacades.data.FSBankAccountData;
import de.hybris.platform.bankingfacades.data.FSBankAccountResponseData;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class FSBankAccountIntegrationPopulator implements Populator<FSBankAccountResponseData, FSBankAccountData>
{
	private static final Logger LOG = Logger.getLogger(FSBankAccountIntegrationPopulator.class);

	private SimpleDateFormat simpleDateFormat;
	private String dateFormatForDisplay;
	private PriceDataFactory priceDataFactory;
	private CategoryService categoryService;
	private Converter<MediaModel, ImageData> imageConverter;
	private Converter<CategoryModel, CategoryData> categoryUrlConverter;


	@Override
	public void populate(final FSBankAccountResponseData source, final FSBankAccountData target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);

		try
		{
			if (StringUtils.isNotEmpty(source.getAccountNumber()))
			{
				target.setAccountNumber(source.getAccountNumber());
			}
			if (StringUtils.isNotEmpty(source.getAccountType()))
			{
				target.setAccountType(source.getAccountType());
				final CategoryModel category = getCategoryService().getCategoryForCode(source.getAccountType());
				final CategoryData categoryData = categoryUrlConverter.convert(category);
				final MediaModel image = category.getThumbnail();
				if (image != null)
				{
					categoryData.setImage(getImageConverter().convert(image));
				}
				target.setCategoryData(categoryData);
			}
			if (StringUtils.isNotEmpty(source.getLimit()))
			{
				final PriceData limit = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(Float.valueOf(source.getLimit())), source.getCurrencyCode());
				target.setLimit(limit);
			}
			if (StringUtils.isNotEmpty(source.getCurrentBalance()))
			{
				final PriceData currentBalance = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(Float.valueOf(source.getCurrentBalance())), source.getCurrencyCode());
				target.setCurrentBalance(currentBalance);
			}
			if (StringUtils.isNotEmpty(source.getAvailableBalance()))
			{
				final PriceData availableBalance = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(Float.valueOf(source.getAvailableBalance())), source.getCurrencyCode());
				target.setAvailableBalance(availableBalance);
			}
			if (StringUtils.isNotEmpty(source.getAvailableCredit()))
			{
				final PriceData availableCredit = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(Float.valueOf(source.getAvailableCredit())), source.getCurrencyCode());
				target.setAvailableCredit(availableCredit);
			}
			if (StringUtils.isNotEmpty(source.getUnbilledAmount()))
			{
				final PriceData unbilledAmount = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(Float.valueOf(source.getUnbilledAmount())), source.getCurrencyCode());
				target.setUnbilledAmount(unbilledAmount);
			}
			if (null != source.getNextStatementDate())
			{
				target.setNextStatementDate(getSimpleDateFormat().format(source.getNextStatementDate()));
			}
			if (StringUtils.isNotEmpty(source.getEffectiveCapital()))
			{
				final PriceData effectiveCapital = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(Float.valueOf(source.getEffectiveCapital())), source.getCurrencyCode());
				target.setEffectiveCapital(effectiveCapital);
			}
			if (StringUtils.isNotEmpty(source.getInstallmentAmount()))
			{
				final PriceData installmentAmount = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(Float.valueOf(source.getInstallmentAmount())), source.getCurrencyCode());
				target.setInstallmentAmount(installmentAmount);
			}
			if (null != source.getNextDueDate())
			{
				target.setNextDueDate(getSimpleDateFormat().format(source.getNextDueDate()));
			}
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.error("category code is incorrect" + e.getMessage());
		}
		catch (final NumberFormatException e)
		{
			LOG.error("invalid price data" + e.getMessage());
		}

		catch (final Exception e)
		{
			LOG.error("Execption Occured", e);
		}

	}

	private SimpleDateFormat getSimpleDateFormat()
	{
		if (simpleDateFormat == null)
		{
			simpleDateFormat = new SimpleDateFormat(this.dateFormatForDisplay);
		}
		return simpleDateFormat;
	}


	public String getDateFormatForDisplay()
	{
		return dateFormatForDisplay;
	}

	@Required
	public void setDateFormatForDisplay(final String dateFormatForDisplay)
	{
		this.dateFormatForDisplay = dateFormatForDisplay;
		simpleDateFormat = new SimpleDateFormat(dateFormatForDisplay);
	}


	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	protected CategoryService getCategoryService()
	{
		return categoryService;
	}

	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	protected Converter<MediaModel, ImageData> getImageConverter()
	{
		return imageConverter;
	}

	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter)
	{
		this.imageConverter = imageConverter;
	}

	/**
	 * @return the categoryUrlConverter
	 */
	protected Converter<CategoryModel, CategoryData> getCategoryUrlConverter()
	{
		return categoryUrlConverter;
	}

	/**
	 * @param categoryUrlConverter the categoryUrlConverter to set
	 */
	public void setCategoryUrlConverter(final Converter<CategoryModel, CategoryData> categoryUrlConverter)
	{
		this.categoryUrlConverter = categoryUrlConverter;
	}



}
