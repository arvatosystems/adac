/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.financialservices.model.InsuranceQuoteModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import java.util.Locale;
import java.util.Map;


public class BankingPersonalDetailsTransformerYFormPreprocessorStrategy extends InsuranceYFormDataPreprocessorStrategy
{
	private CartService cartService;
	private ModelService modelService;

	/**
	 * Applies the actual transformation to a formData
	 */
	@Override
	protected String transform(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		final String xmlString = super.transform(xmlContent, params);
		final CartModel cartModel = getCartService().getSessionCart();
		final InsuranceQuoteModel quoteModel = cartModel.getInsuranceQuote();

		if (!validation(params) || quoteModel == null || MapUtils.isEmpty(quoteModel.getProperties()))
		{
			return xmlString;
		}

		final Map<String, Object> bankingPersonalDetailsParams = Maps.newHashMap();
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();

		if (!getUserService().isAnonymousUser(currentUser))
		{
			prefillCustomerData(currentUser, bankingPersonalDetailsParams, xmlString);
		}

		if (quoteModel.getProperties().containsKey(AdacfinancialaccestorefrontConstants.NUMBER_OF_APPLICANTS))
		{

			final String numberOfApplicants = MapUtils.getString(quoteModel.getProperties(),
					AdacfinancialaccestorefrontConstants.NUMBER_OF_APPLICANTS);

			bankingPersonalDetailsParams.put("/form/number-of-applicants", numberOfApplicants);
		}

		return XmlUtilTransformer.updateXmlContent(xmlString, bankingPersonalDetailsParams);
	}


	protected void prefillCustomerData(CustomerModel currentUser, Map<String, Object> bankingPersonalDetailsParams,
			String xmlString)
	{

		final String[] fullName = getCustomerNameStrategy().splitName(currentUser.getName());
		final Document document = XmlUtilTransformer.createDocument(xmlString);
		final XPath xpath = XmlUtilTransformer.createXPath();

		if (XmlUtilTransformer.getTextValue(xpath, document, "/form/personal-details/title").isEmpty()
				&& currentUser.getTitle() != null)
		{
			final String currentLanguage = currentUser.getSessionLanguage().getIsocode();
			bankingPersonalDetailsParams.put("/form/personal-details/title",
					currentUser.getTitle().getName(Locale.forLanguageTag(currentLanguage)));
		}

		if (XmlUtilTransformer.getTextValue(xpath, document, "/form/personal-details/first-name").isEmpty())
		{
			bankingPersonalDetailsParams.put("/form/personal-details/first-name", fullName[0]);
		}
		if (XmlUtilTransformer.getTextValue(xpath, document, "/form/personal-details/last-name").isEmpty())
		{
			bankingPersonalDetailsParams.put("/form/personal-details/last-name", fullName[1]);
		}
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
