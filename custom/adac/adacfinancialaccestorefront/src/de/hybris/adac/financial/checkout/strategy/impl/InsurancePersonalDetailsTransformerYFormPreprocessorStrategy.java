/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import java.util.Locale;
import java.util.Map;


/**
 * The class of InsurancePersonalDetailsTransformerYFormPreprocessorStrategy.
 */
public class InsurancePersonalDetailsTransformerYFormPreprocessorStrategy extends InsuranceYFormDataPreprocessorStrategy
{

	private static final String FORM_PERSONAL_DETAILS = "/form/personal-details";
	private static final String TITLE_XPATH = "/title";
	private static final String FIRST_NAME_XPATH = "/first-name";
	private static final String LAST_NAME_XPATH = "/last-name";
	private static final String EMAIL_XPATH = "/email";

	/**
	 * Applies the actual transformation to a formData
	 */
	@Override
	protected String transform(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		final String xmlString = super.transform(xmlContent, params);

		if (!validation(params))
		{
			return xmlString;
		}

		final Map<String, Object> propertyInsuranceParams = Maps.newHashMap();

		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();

		if (!getUserService().isAnonymousUser(currentUser))
		{
			prefillCustomerData(currentUser, propertyInsuranceParams, xmlString);
		}

		return XmlUtilTransformer.updateXmlContent(xmlString, propertyInsuranceParams);
	}

	protected void prefillCustomerData(CustomerModel currentUser, Map<String, Object> propertyInsuranceParams, String xmlString)
	{

		final String[] fullName = getCustomerNameStrategy().splitName(currentUser.getName());

		final Document document = XmlUtilTransformer.createDocument(xmlString);
		final XPath xpath = XmlUtilTransformer.createXPath();

		if (XmlUtilTransformer.getTextValue(xpath, document, FORM_PERSONAL_DETAILS + TITLE_XPATH).isEmpty()
				&& currentUser.getTitle() != null)
		{
			final String currentLanguage = currentUser.getSessionLanguage().getIsocode();
			propertyInsuranceParams.put(FORM_PERSONAL_DETAILS + TITLE_XPATH,
					currentUser.getTitle().getName(Locale.forLanguageTag(currentLanguage)));
		}

		if (XmlUtilTransformer.getTextValue(xpath, document, FORM_PERSONAL_DETAILS + FIRST_NAME_XPATH).isEmpty())
		{
			propertyInsuranceParams.put(FORM_PERSONAL_DETAILS + FIRST_NAME_XPATH, fullName[0]);
		}

		if (XmlUtilTransformer.getTextValue(xpath, document, FORM_PERSONAL_DETAILS + LAST_NAME_XPATH).isEmpty())
		{
			propertyInsuranceParams.put(FORM_PERSONAL_DETAILS + LAST_NAME_XPATH, fullName[1]);
		}

		if (XmlUtilTransformer.getTextValue(xpath, document, FORM_PERSONAL_DETAILS + EMAIL_XPATH).isEmpty())
		{
			propertyInsuranceParams.put(FORM_PERSONAL_DETAILS + EMAIL_XPATH, currentUser.getContactEmail());
		}
	}

}

