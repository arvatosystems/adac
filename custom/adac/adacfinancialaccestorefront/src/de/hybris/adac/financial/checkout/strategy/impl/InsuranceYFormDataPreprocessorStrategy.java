/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.checkout.strategy.impl;

import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storefront.form.data.FormDetailData;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.ReferenceIdTransformerYFormPreprocessorStrategy;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;


/**
 * The class of InsuranceYFormDataPreprocessorStrategy.
 */
public class InsuranceYFormDataPreprocessorStrategy extends ReferenceIdTransformerYFormPreprocessorStrategy
{

	public static final String FORM_DETAIL_DATA = "formDetailData";

	private UserService userService;
	private CustomerNameStrategy customerNameStrategy;


	protected boolean validation(final Map<String, Object> params)
	{
		return params.containsKey(FORM_DETAIL_DATA) && params.get(FORM_DETAIL_DATA) instanceof FormDetailData;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CustomerNameStrategy getCustomerNameStrategy()
	{
		return customerNameStrategy;
	}

	@Required
	public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy)
	{
		this.customerNameStrategy = customerNameStrategy;
	}
}
