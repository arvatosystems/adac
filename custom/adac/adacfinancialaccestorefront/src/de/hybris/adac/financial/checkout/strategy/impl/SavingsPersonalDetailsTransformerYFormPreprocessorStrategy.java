/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import de.hybris.platform.order.CartService;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Map;


/**
 * The type Savings insurance personal details transformer y form preprocessor strategy.
 */
public class SavingsPersonalDetailsTransformerYFormPreprocessorStrategy
		extends InsurancePersonalDetailsTransformerYFormPreprocessorStrategy
{
	private static final String SAVINGS_SURVIVOR_PENSION = "SAVINGS_SURVIVOR_PENSION";
	private static final String FORM_SAVING_SURVIVOR_PENSION = "/form/saving-survivor-pension";
	private static final String SAVINGS_DEPENDENT_CHILDREN_PENSION = "SAVINGS_DEPENDENT_CHILDREN_PENSION";
	private static final String FORM_SAVINGS_DEPENDENT_CHILDREN_PENSION = "/form/savings-dependent-children-pension";

	private CartService cartService;

	/**
	 * Applies the actual transformation to a formData
	 */
	@Override
	protected String transform(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		final String xmlString = super.transform(xmlContent, params);

		final Map<String, Object> savingsPersonalDetailsParams = Maps.newHashMap();
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();

		if (!getUserService().isAnonymousUser(currentUser))
		{
			prefillCustomerData(currentUser, savingsPersonalDetailsParams, xmlString);
		}

		final List<AbstractOrderEntryModel> entries = getCartService().getSessionCart().getEntries();

		resetSelectedAttributes(savingsPersonalDetailsParams);

		if (CollectionUtils.isNotEmpty(entries))
		{
			entries.stream().filter(entry -> entry.getProduct() != null).forEach(product -> {
				final String productCode = product.getProduct().getCode();

				if (SAVINGS_SURVIVOR_PENSION.equalsIgnoreCase(productCode))
				{
					savingsPersonalDetailsParams.put(FORM_SAVING_SURVIVOR_PENSION, true);
				}
				else if (SAVINGS_DEPENDENT_CHILDREN_PENSION.equalsIgnoreCase(productCode))
				{
					savingsPersonalDetailsParams.put(FORM_SAVINGS_DEPENDENT_CHILDREN_PENSION, true);
				}
			});
		}

		return XmlUtilTransformer.updateXmlContent(xmlString, savingsPersonalDetailsParams);
	}

	protected void resetSelectedAttributes(final Map<String, Object> savingsPersonalDetailsParams)
	{
		savingsPersonalDetailsParams.put(FORM_SAVING_SURVIVOR_PENSION, false);
		savingsPersonalDetailsParams.put(FORM_SAVINGS_DEPENDENT_CHILDREN_PENSION, false);
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
