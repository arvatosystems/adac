/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.financialservices.model.InsuranceQuoteModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Required;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * The class of TravelInsuranceTransformerYFormPreprocessorStrategy.
 */
public class TravelInsuranceTransformerYFormPreprocessorStrategy extends InsuranceYFormDataPreprocessorStrategy
{
	private CartService cartService;
	private ModelService modelService;

	/**
	 * Applies the actual transformation to a formData
	 */
	@Override
	protected String transform(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		final String xmlString = super.transform(xmlContent, params);
		final CartModel cartModel = getCartService().getSessionCart();
		final InsuranceQuoteModel quoteModel = cartModel.getInsuranceQuote();

		if (!validation(params) || quoteModel == null || MapUtils.isEmpty(quoteModel.getProperties()))
		{
			return xmlString;
		}

		final Map<String, Object> travelInsuranceParams = Maps.newHashMap();
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();

		if (!getUserService().isAnonymousUser(currentUser))
		{
			prefillCustomerData(currentUser, travelInsuranceParams, xmlString);
		}

		if (quoteModel.getProperties().containsKey(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_NO_OF_TRAVELLERS))
		{
			final Integer noOfTravellers = MapUtils.getInteger(quoteModel.getProperties(),
					AdacfinancialaccestorefrontConstants.TRIP_DETAILS_NO_OF_TRAVELLERS, NumberUtils.INTEGER_ZERO);
			travelInsuranceParams.put("/form/number-of-travellers", noOfTravellers);

			if (quoteModel.getProperties()
					.containsKey(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_TRAVELLER_AGES_FOR_PRE_FORM_POPULATE)
					&& quoteModel.getProperties()
					.get(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_TRAVELLER_AGES_FOR_PRE_FORM_POPULATE) instanceof List)
			{
				final List<String> ages = (List<String>) quoteModel.getProperties()
						.get(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_TRAVELLER_AGES_FOR_PRE_FORM_POPULATE);
				travelInsuranceParams.put("/form/personal-details/age", ages.get(NumberUtils.INTEGER_ZERO));

				for (int i = NumberUtils.INTEGER_ONE; i < ages.size(); i++)
				{
					travelInsuranceParams.put("/form/traveller-" + i + "/age-" + i, ages.get(i));
				}
				removeAgesForPreFormPopulate(quoteModel);
			}
		}
		return XmlUtilTransformer.updateXmlContent(xmlString, travelInsuranceParams);
	}

	protected void removeAgesForPreFormPopulate(final InsuranceQuoteModel quoteModel)
	{
		if (quoteModel.getProperties()
				.containsKey(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_TRAVELLER_AGES_FOR_PRE_FORM_POPULATE)
				&& quoteModel.getProperties()
				.get(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_TRAVELLER_AGES_FOR_PRE_FORM_POPULATE) instanceof List)
		{
			final Map<String, Object> newMap = Maps.newHashMap(quoteModel.getProperties());

			newMap.remove(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_TRAVELLER_AGES_FOR_PRE_FORM_POPULATE);
			quoteModel.setProperties(newMap);

			getModelService().save(quoteModel);
		}
	}

	protected void prefillCustomerData(CustomerModel currentUser, Map<String, Object> travelInsuranceParams, String xmlString)
	{

		final String[] fullName = getCustomerNameStrategy().splitName(currentUser.getName());

		final Document document = XmlUtilTransformer.createDocument(xmlString);
		final XPath xpath = XmlUtilTransformer.createXPath();

		if (XmlUtilTransformer.getTextValue(xpath, document, "/form/personal-details/title").isEmpty()
				&& currentUser.getTitle() != null)
		{
			final String currentLanguage = currentUser.getSessionLanguage().getIsocode();
			travelInsuranceParams
					.put("/form/personal-details/title", currentUser.getTitle().getName(Locale.forLanguageTag(currentLanguage)));
		}

		if (XmlUtilTransformer.getTextValue(xpath, document, "/form/personal-details/first-name").isEmpty())
		{
			travelInsuranceParams.put("/form/personal-details/first-name", fullName[0]);
		}

		if (XmlUtilTransformer.getTextValue(xpath, document, "/form/personal-details/last-name").isEmpty())
		{
			travelInsuranceParams.put("/form/personal-details/last-name", fullName[1]);
		}

		if (XmlUtilTransformer.getTextValue(xpath, document, "/form/personal-details/email").isEmpty())
		{
			travelInsuranceParams.put("/form/personal-details/email", currentUser.getContactEmail());
		}
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
