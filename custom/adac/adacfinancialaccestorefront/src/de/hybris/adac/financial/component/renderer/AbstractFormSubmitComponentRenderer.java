/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.component.renderer;

import de.hybris.platform.commercefacades.quotation.InsuranceQuoteData;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.financialfacades.facades.InsuranceCartFacade;
import de.hybris.platform.financialservices.model.components.CMSFormSubmitComponentModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformsservices.enums.YFormDataTypeEnum;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;
import javax.xml.xpath.XPath;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;


/**
 * The class of FormSubmitComponentRenderer.
 */
public abstract class AbstractFormSubmitComponentRenderer<C extends CMSFormSubmitComponentModel>
		extends DefaultAddOnSubstitutingCMSComponentRenderer<C> //NOSONAR
{

	protected static final String VIEW_STATUS = "viewStatus";
	protected static final String VIEW_STATUS_VIEW = "view";
	private static final Logger LOG = Logger.getLogger(AbstractFormSubmitComponentRenderer.class);
	private YFormFacade yformFacade;
	private InsuranceCartFacade insuranceCartFacade;
	private SessionService sessionService;
	private String dateFormatForDisplay;

	@Override
	public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException
	{
		final String viewStatus = pageContext.getRequest().getParameter(VIEW_STATUS);
		final String sessionFormDataId = getSessionFormId(pageContext, component);
		final String inlineHtml;
		final String applicationId = component.getApplicationId();
		final String formId = component.getFormId();

		try
		{
			// assumes we must have  session ID otherwise we would need to show a form
			if (viewStatus != null && viewStatus.equals(VIEW_STATUS_VIEW))
			{
				final YFormDataData yfdd = getYformFacade().getYFormData(sessionFormDataId, YFormDataTypeEnum.DATA);
				setSessionAttributes(yfdd.getContent(), pageContext, component);
			}
			else
			// this is in the edit view
			{
				// if I have a sessionFormDataId
				if (sessionFormDataId != null && getYformFacade().getYFormData(sessionFormDataId, YFormDataTypeEnum.DATA) !=
						null)
				{
					// attempt to get an existing trip form if possible
					inlineHtml = getYformFacade().getInlineFormHtml(component.getApplicationId(), component.getFormId(),
							sessionFormDataId);
					writeContent(pageContext, inlineHtml);
				}
				else if (sessionFormDataId == null)
				// this is where it's edit view and have never filled in a form
				{
					final String newSessionFormDataId = getYformFacade().getNewFormDataId();
					storeSessionFormId(pageContext, newSessionFormDataId, component);
					final YFormDataData yfdd = getYformFacade().createYFormData(applicationId, formId, newSessionFormDataId,
							YFormDataTypeEnum.DATA, null, null);
					final String content = getYformFacade().getInlineFormHtml(applicationId, formId, yfdd.getId());

					writeContent(pageContext, content);
				}
			}
		}
		catch (final YFormServiceException e)
		{
			LOG.error(e.getMessage(), e);
		}
		super.renderComponent(pageContext, component);
	}

	protected abstract void setSessionAttributes(final String pString, final PageContext pPageContext, final C component);

	protected String getSessionFormId(final PageContext pageContext, final C component)
	{
		String yFormId;
		final String category = getComponentCategoryCode(component);
		final String categoryFormId = category.concat(AdacfinancialaccestorefrontConstants.FORM_ID);
		final InsuranceQuoteData quoteData = getInsuranceCartFacade().getInsuranceQuoteFromSessionCart();
		if (quoteData != null)
		{
			final String selectedCategory = getInsuranceCartFacade()
					.getSelectedInsuranceCategory() != null ? getInsuranceCartFacade().getSelectedInsuranceCategory()
					.getCode() : StringUtils.EMPTY;

			if (quoteData.getFormId() != null && category.equals(selectedCategory))
			{
				yFormId = quoteData.getFormId();
				storeSessionFormId(pageContext, yFormId, component);
				return yFormId;
			}
		}

		return getSessionService().getAttribute(categoryFormId);
	}

	protected void storeSessionFormId(final PageContext pageContext, final String newSessionFormDataId, final C component)
	{
		final String categoryFormId = getComponentCategoryCode(component).concat(AdacfinancialaccestorefrontConstants
				.FORM_ID);
		getSessionService().setAttribute(categoryFormId, newSessionFormDataId);
	}

	protected void writeContent(final PageContext pageContext, final String value) throws IOException
	{
		pageContext.getOut().write(value);
	}

	protected String buildAttributeKey(final String str)
	{
		if (str != null)
		{
			return str.replaceAll("[^A-Za-z0-9]", "_").toLowerCase(Locale.ROOT);
		}
		return null;
	}

	protected void setSessionAttribute(final String pString, final C component, final String key,
			final Map<String, String> sessionMap)
	{
		if (key != null)
		{
			//Create DOM document
			final Document document = XmlUtilTransformer.createDocument(pString);

			// Create XPath object
			final XPath xpath = XmlUtilTransformer.createXPath();
			final NodeList nodes = XmlUtilTransformer.getNodeList(xpath, document, key + XmlUtilTransformer.XPATH_TEXT);
			String value = StringUtils.EMPTY;

			if (nodes != null && nodes.getLength() >= NumberUtils.INTEGER_ONE)
			{
				value = StringEscapeUtils.escapeXml11(nodes.item(0).getNodeValue());
			}

			final String cleanedKey = buildAttributeKey(getComponentCategoryCode(component) + key);

			sessionMap.put(cleanedKey, value);
		}
	}

	protected String getComponentCategoryCode(final C component)
	{
		String prefix = StringUtils.EMPTY;
		if (component.getCategory() != null)
		{
			prefix = component.getCategory().getCode();
		}
		return prefix;
	}

	/**
	 * Places the data temporarily in the session so that it can be picked up once the item is actually added to the cart
	 *
	 * @param sessionKey key for session
	 * @param nodeList   xml
	 */
	protected void safelyStashNodeDataInSession(final PageContext pageContext, final String sessionKey, final NodeList nodeList)
	{
		if (nodeList.item(0) != null)
		{
			final String nodeValue = StringEscapeUtils.escapeXml11(nodeList.item(0).getNodeValue());
			if (StringUtils.isNotEmpty(nodeValue))
			{
				pageContext.getSession().setAttribute(sessionKey, nodeValue);
				getSessionService().setAttribute(sessionKey, nodeValue);
			}
		}
		else
		{
			getSessionService().removeAttribute(sessionKey);
		}
	}

	protected String getDateFormatForDisplay()
	{
		return dateFormatForDisplay;
	}

	@Required
	public void setDateFormatForDisplay(final String dateFormatForDisplay)
	{
		this.dateFormatForDisplay = dateFormatForDisplay;
	}

	protected YFormFacade getYformFacade()
	{
		return yformFacade;
	}

	@Required
	public void setYformFacade(final YFormFacade yformFacade)
	{
		this.yformFacade = yformFacade;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * @return the insuranceCartFacade
	 */
	public InsuranceCartFacade getInsuranceCartFacade()
	{
		return insuranceCartFacade;
	}

	/**
	 * @param insuranceCartFacade the insuranceCartFacade to set
	 */
	public void setInsuranceCartFacade(final InsuranceCartFacade insuranceCartFacade)
	{
		this.insuranceCartFacade = insuranceCartFacade;
	}
}
