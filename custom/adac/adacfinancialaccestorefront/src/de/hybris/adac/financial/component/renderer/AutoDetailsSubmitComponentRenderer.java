/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.component.renderer;

import de.hybris.platform.financialservices.constants.FinancialservicesConstants;
import de.hybris.platform.financialservices.model.components.CMSAutoDetailsSubmitComponentModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import org.w3c.dom.Document;

import javax.servlet.jsp.PageContext;
import javax.xml.xpath.XPath;


/**
 * The class of AutoDetailsSubmitComponentRenderer.
 */
public class AutoDetailsSubmitComponentRenderer<C extends CMSAutoDetailsSubmitComponentModel>
		extends AbstractFormSubmitComponentRenderer<C>
{

	@Override
	protected void setSessionAttributes(final String pString, final PageContext pPageContext, final C component)
	{
		final Document document = XmlUtilTransformer.createDocument(pString);
		final XPath xpath = XmlUtilTransformer.createXPath();

		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_DRIVER_DOB, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/section-1/driver-date-of-birth" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_COVER_START, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/section-1/coverage-start-date" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_VALUE,
				XmlUtilTransformer.getNodeList(xpath, document, "/form/section-1/vehicle-value" + XmlUtilTransformer
						.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_MAKE,
				XmlUtilTransformer.getNodeList(xpath, document, "/form/section-1/vehicle-make" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_MODEL,
				XmlUtilTransformer.getNodeList(xpath, document, "/form/section-1/vehicle-model" + XmlUtilTransformer
						.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_LICENSE, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/section-1/vehicle-license" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_YEAR, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/section-1/year-manufacture" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_STATE,
				XmlUtilTransformer.getNodeList(xpath, document, "/form/section-1/state" + XmlUtilTransformer.XPATH_TEXT));
	}
}
