/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.component.renderer;

import de.hybris.platform.financialservices.constants.FinancialservicesConstants;
import de.hybris.platform.financialservices.model.components.CMSLifeDetailsSubmitComponentModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import org.w3c.dom.Document;

import javax.servlet.jsp.PageContext;
import javax.xml.xpath.XPath;


/**
 * The class of LifeDetailsSubmitComponentRenderer.
 */
public class LifeDetailsSubmitComponentRenderer<C extends CMSLifeDetailsSubmitComponentModel>
		extends AbstractFormSubmitComponentRenderer<C>
{

	private static final String COVERAGE_INFO_SECTION = "/form/coverage-information-section/";

	@Override
	protected void setSessionAttributes(final String pString, final PageContext pPageContext, final C component)
	{
		final Document document = XmlUtilTransformer.createDocument(pString);
		final XPath xpath = XmlUtilTransformer.createXPath();

		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_WHO_COVERED, XmlUtilTransformer
				.getNodeList(xpath, document, COVERAGE_INFO_SECTION + "who-is-being-covered" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_COVERAGE_REQUIRE, XmlUtilTransformer
				.getNodeList(xpath, document,
						COVERAGE_INFO_SECTION + "how-much-coverage-require" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_COVERAGE_LAST, XmlUtilTransformer
				.getNodeList(xpath, document, COVERAGE_INFO_SECTION + "low-long-coverage-last" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_COVERAGE_START_DATE, XmlUtilTransformer
				.getNodeList(xpath, document, COVERAGE_INFO_SECTION + "coverage-start-date" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_MAIN_DOB, XmlUtilTransformer
				.getNodeList(xpath, document, COVERAGE_INFO_SECTION + "date-of-birth" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_MAIN_SMOKE, XmlUtilTransformer
				.getNodeList(xpath, document, COVERAGE_INFO_SECTION + "do-you-smoke" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_SECOND_DOB, XmlUtilTransformer
				.getNodeList(xpath, document,
						COVERAGE_INFO_SECTION + "second-persons-date-of-birth" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_SECOND_SMOKE, XmlUtilTransformer
				.getNodeList(xpath, document, COVERAGE_INFO_SECTION + "second-person-smoke" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.LIFE_RELATIONSHIP, XmlUtilTransformer
				.getNodeList(xpath, document,
						COVERAGE_INFO_SECTION + "relationship-to-second-person" + XmlUtilTransformer.XPATH_TEXT));
	}
}
