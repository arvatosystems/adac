/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.component.renderer;

import com.google.common.collect.Maps;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.financialservices.model.components.CMSPropertyDetailsSubmitComponentModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

import javax.servlet.jsp.PageContext;
import javax.xml.xpath.XPath;
import java.util.List;
import java.util.Map;


/**
 * The class of PropertyDetailsSubmitComponentRenderer.
 */
public class PropertyDetailsSubmitComponentRenderer<C extends CMSPropertyDetailsSubmitComponentModel>
		extends AbstractFormSubmitComponentRenderer<C>
{
	private static final Logger LOG = Logger.getLogger(PropertyDetailsSubmitComponentRenderer.class);

	private Map<String, List<String>> insurancePropertyDetailsKeysMap;

	protected String buildSessionFormDataId(final C component)
	{
		return getComponentCategoryCode(component) + AdacfinancialaccestorefrontConstants.PROPERTY_DETAILS_FORM_DATA_ID;
	}

	@Override
	protected void setSessionAttributes(final String pString, final PageContext pPageContext, final C component)
	{
		try
		{
			setQuotationAndAddressAttributes(pString, pPageContext);

			final Map<String, String> sessionMap = Maps.newLinkedHashMap();
			final String categoryCode = getComponentCategoryCode(component);
			if (StringUtils.isNotEmpty(categoryCode) && getInsurancePropertyDetailsKeysMap().containsKey(categoryCode))
			{
				for (final String formKey : getInsurancePropertyDetailsKeysMap().get(categoryCode))
				{
					setSessionAttribute(pString, component, formKey, sessionMap);
				}

				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.PROPERTY_FORM_SESSION_MAP,
						sessionMap);
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.PROPERTY_FORM_SESSION_MAP, sessionMap);
			}
		}
		catch (final DOMException e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	protected void setQuotationAndAddressAttributes(final String pString, final PageContext pPageContext)
	{
		final Document document = XmlUtilTransformer.createDocument(pString);
		final XPath xpath = XmlUtilTransformer.createXPath();

		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_DETAILS_VALUE,
				XmlUtilTransformer.getNodeList(xpath, document,
						"/form/property-details-section/property-value" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_DETAILS_TYPE,
				XmlUtilTransformer.getNodeList(xpath, document,
						"/form/property-details-section/property-type" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_DETAILS_REBUILD_COST,
				XmlUtilTransformer.getNodeList(xpath, document,
						"/form/property-details-section/rebuild-value-of-property" + XmlUtilTransformer.XPATH_TEXT));

		safelyStashNodeDataInSession(pPageContext,
				AdacfinancialaccestorefrontConstants.PROPERTY_DETAILS_IS_STANDARD_50000_CONTENT_COVER, XmlUtilTransformer
						.getNodeList(xpath, document,
								"/form/property-contents-cover-section/standard-50000-contents-cover" + XmlUtilTransformer
										.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext,
				AdacfinancialaccestorefrontConstants.PROPERTY_DETAILS_CONTENT_COVER_MULTIPLE_OF_10000, XmlUtilTransformer
						.getNodeList(xpath, document,
								"/form/property-contents-cover-section/amount-multiples-10000" + XmlUtilTransformer.XPATH_TEXT));

		// Address attributes
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_ADDRESS1, XmlUtilTransformer
				.getNodeList(xpath, document,
						"/form/property-address-section/property-address-line-1" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_ADDRESS2, XmlUtilTransformer
				.getNodeList(xpath, document,
						"/form/property-address-section/property-address-line-2" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_CITY, XmlUtilTransformer
				.getNodeList(xpath, document,
						"/form/property-address-section/property-address-city" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_POSTCODE, XmlUtilTransformer
				.getNodeList(xpath, document,
						"/form/property-address-section/property-address-postcode" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_COUNTRY, XmlUtilTransformer
				.getNodeList(xpath, document,
						"/form/property-address-section/property-address-country" + XmlUtilTransformer.XPATH_TEXT));

		// PDF generation attributes
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_DETAILS_COVER_REQUIRED,
				XmlUtilTransformer.getNodeList(xpath, document,
						"/form/property-coverage-details-section/property-cover-required" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, AdacfinancialaccestorefrontConstants.PROPERTY_DETAILS_START_DATE,
				XmlUtilTransformer.getNodeList(xpath, document,
						"/form/property-coverage-details-section/property-cover-start-date" + XmlUtilTransformer.XPATH_TEXT));
	}

	protected Map<String, List<String>> getInsurancePropertyDetailsKeysMap()
	{
		return insurancePropertyDetailsKeysMap;
	}

	@Required
	public void setInsurancePropertyDetailsKeysMap(final Map<String, List<String>> insurancePropertyDetailsKeysMap)
	{
		this.insurancePropertyDetailsKeysMap = insurancePropertyDetailsKeysMap;
	}
}
