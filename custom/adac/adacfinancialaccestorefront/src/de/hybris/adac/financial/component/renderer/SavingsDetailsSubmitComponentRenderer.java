/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.component.renderer;

import de.hybris.platform.financialservices.constants.FinancialservicesConstants;
import de.hybris.platform.financialservices.model.components.CMSSavingsDetailsSubmitComponentModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import org.w3c.dom.Document;

import javax.servlet.jsp.PageContext;
import javax.xml.xpath.XPath;


/**
 * The class of SavingsDetailsSubmitComponentRenderer.
 */
public class SavingsDetailsSubmitComponentRenderer<C extends CMSSavingsDetailsSubmitComponentModel>
		extends AbstractFormSubmitComponentRenderer<C>
{

	@Override
	protected void setSessionAttributes(final String pString, final PageContext pPageContext, final C component)
	{
		final Document document = XmlUtilTransformer.createDocument(pString);
		final XPath xpath = XmlUtilTransformer.createXPath();

		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.SAVINGS_CONTRIBUTION_FREQUENCY, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/choose-cover/contribution-frequency" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.SAVINGS_CONTRIBUTION, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/choose-cover/contribution" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.SAVINGS_ANNUAL_CONTRIBUTION_INCREASE, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/choose-cover/annual-contribution-increase"	+ XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.SAVINGS_START_DATE, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/choose-cover/start-date" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.SAVINGS_RETIREMENT_AGE, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/choose-cover/retirement-age" + XmlUtilTransformer.XPATH_TEXT));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.SAVINGS_DATE_OF_BIRTH, XmlUtilTransformer
				.getNodeList(xpath, document, "/form/choose-cover/date-of-birth" + XmlUtilTransformer.XPATH_TEXT));
	}
}
