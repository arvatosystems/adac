/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.component.renderer;

import com.google.common.collect.Lists;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.financialservices.model.components.CMSTripDetailsSubmitComponentModel;
import de.hybris.platform.financialservices.util.XmlUtilTransformer;
import de.hybris.platform.util.Config;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.servlet.jsp.PageContext;
import javax.xml.xpath.XPath;
import java.text.SimpleDateFormat;
import java.util.List;


/**
 * The class of TripDetailsSubmitComponentRenderer.
 */
public class TripDetailsSubmitComponentRenderer<C extends CMSTripDetailsSubmitComponentModel>
		extends AbstractFormSubmitComponentRenderer<C>
{

	private static final Logger LOG = Logger.getLogger(TripDetailsSubmitComponentRenderer.class);

	/**
	 * Method which extracts and sets all the trip information details to the session.
	 *
	 * @param pString      String
	 * @param pPageContext page context
	 */
	@Override
	public void setSessionAttributes(final String pString, final PageContext pPageContext, final C component)
	{
		try
		{
			final Document document = XmlUtilTransformer.createDocument(pString);
			final XPath xpath = XmlUtilTransformer.createXPath();

			NodeList nodes = XmlUtilTransformer.getNodeList(xpath, document,
					"/form/trip-details/destination" + XmlUtilTransformer.XPATH_TEXT);

			if (nodes.item(0) != null)
			{
				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_DESTINATION,
						XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_DESTINATION,
						XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
			}

			nodes = XmlUtilTransformer.getNodeList(xpath, document,
					"/form/trip-details/start-date" + XmlUtilTransformer.XPATH_TEXT);

			if (nodes.item(0) != null)
			{
				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_START_DATE,
						XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_START_DATE,
						XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
			}

			nodes = XmlUtilTransformer.getNodeList(xpath, document,
					"/form/trip-details/return-date" + XmlUtilTransformer.XPATH_TEXT);

			if (nodes.item(0) != null)
			{
				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_END_DATE,
						XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_END_DATE,
						XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_NO_OF_DAYS,
						calculateNumberOfDays(pPageContext));
			}
			else
			{
				final String numberOfDays = Config.getString("travel.quotation.default.noofdays", "31");

				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_END_DATE,
						AdacfinancialaccestorefrontConstants.NOT_APPLICABLE_TEXT);
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_END_DATE,
						AdacfinancialaccestorefrontConstants.NOT_APPLICABLE_TEXT);

				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_NO_OF_DAYS,
						numberOfDays);
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_NO_OF_DAYS, numberOfDays);
			}

			nodes = XmlUtilTransformer.getNodeList(xpath, document,
					"/form/trip-details/cost-of-trip" + XmlUtilTransformer.XPATH_TEXT);

			if (nodes.item(0) != null)
			{
				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_COST,

						XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_COST,
						XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
			}

			nodes = XmlUtilTransformer.getNodeList(xpath, document,
					"/form/trip-details/number-of-travellers" + XmlUtilTransformer.XPATH_TEXT);

			if (nodes.item(0) != null)
			{
				final int numberOfTravellers = Integer.parseInt(XmlUtilTransformer.safelyGetFirstNodeValue(nodes));
				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_NO_OF_TRAVELLERS,
						Integer.toString(numberOfTravellers));
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_NO_OF_TRAVELLERS,
						Integer.toString(numberOfTravellers));

				final List<String> travellerAges = Lists.newArrayList();
				for (int i = 1; i <= Integer.valueOf(XmlUtilTransformer.safelyGetFirstNodeValue(nodes)); i++)
				{
					final NodeList ageNodes = XmlUtilTransformer.getNodeList(xpath, document,
							"/form/age-of-travellers/age-" + i + XmlUtilTransformer.XPATH_TEXT);
					travellerAges.add(XmlUtilTransformer.safelyGetFirstNodeValue(ageNodes));
				}

				pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_TRAVELLER_AGES,
						travellerAges);
				getSessionService().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_TRAVELLER_AGES,
						travellerAges);
			}
		}
		catch (final DOMException e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Helper method used to calculates the number of days based on start and end date from the trip information details.
	 *
	 * @param pPageContext page context
	 * @return difference in days
	 */
	protected String calculateNumberOfDays(final PageContext pPageContext)
	{
		final DateTimeFormatter formatter = DateTimeFormat.forPattern(
				AdacfinancialaccestorefrontConstants.INSURANCE_GENERIC_DATE_FORMAT);

		final DateTime start = formatter.parseDateTime(
				(String) pPageContext.getSession().getAttribute(AdacfinancialaccestorefrontConstants
						.TRIP_DETAILS_START_DATE));
		final DateTime end = formatter.parseDateTime(
				(String) pPageContext.getSession().getAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_END_DATE));
		final int diffInDays = Days.daysBetween(start, end).getDays();
		pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_NO_OF_DAYS,
				Integer.toString(diffInDays));

		final SimpleDateFormat sdf = new SimpleDateFormat(getDateFormatForDisplay());
		pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_START_DATE,
				sdf.format(start.toDate()));
		pPageContext.getSession().setAttribute(AdacfinancialaccestorefrontConstants.TRIP_DETAILS_END_DATE,
				sdf.format(end.toDate()));
		return Integer.toString(diffInDays);
	}
}
