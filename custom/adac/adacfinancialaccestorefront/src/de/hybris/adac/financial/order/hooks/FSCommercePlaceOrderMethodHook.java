/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.order.hooks;

import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.adac.financial.order.strategies.FSAbstractPlaceOrderCleanUpStrategy;
import de.hybris.platform.order.InvalidCartException;

import java.util.List;


public class FSCommercePlaceOrderMethodHook implements CommercePlaceOrderMethodHook
{

	private List<FSAbstractPlaceOrderCleanUpStrategy> fsPlaceOrderCleanUpStrategies;

	@Override
	public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel)
			throws InvalidCartException
	{
		for (final FSAbstractPlaceOrderCleanUpStrategy strategy : fsPlaceOrderCleanUpStrategies)
		{
			strategy.handleAfterPlaceOrder(parameter, orderModel);
		}
	}

	@Override
	public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
	{
		//FS nothing to do here
	}

	@Override
	public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
			throws InvalidCartException
	{
		//FS nothing to do here
	}

	protected List<FSAbstractPlaceOrderCleanUpStrategy> getFsPlaceOrderCleanUpStrategies()
	{
		return fsPlaceOrderCleanUpStrategies;
	}

	public void setFsPlaceOrderCleanUpStrategies(
			final List<FSAbstractPlaceOrderCleanUpStrategy> fsPlaceOrderCleanUpStrategies)
	{
		this.fsPlaceOrderCleanUpStrategies = fsPlaceOrderCleanUpStrategies;
	}
}
