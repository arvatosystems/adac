/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.order.strategies;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Required;


/**
 * Abstract clean up strategy after placing order.
 */
public abstract class FSAbstractPlaceOrderCleanUpStrategy
{

	private SessionService sessionService;

	/**
	 * Handle for cleaning up after place order.
	 *
	 * @param parameter  the {@link CommerceCheckoutParameter} parameter
	 * @param orderModel the  {@link CommerceOrderResult} order model
	 */
	public abstract void handleAfterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel);

	/**
	 * Cheks if input parameters are valid to be processed. It checks for null pointer exceptions.
	 *
	 * @param parameter  the {@link CommerceCheckoutParameter} parameter
	 * @param orderModel the {@link CommerceOrderResult} order model
	 * @return the boolean
	 */
	public boolean areParametersValid(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel)
	{
		return isCommerceCheckoutParameterValid(parameter) && isCommerceOrderResultValid(orderModel);
	}

	private boolean isCommerceCheckoutParameterValid(final CommerceCheckoutParameter parameter)
	{
		return parameter != null && parameter.getCart() != null && parameter.getCart().getInsuranceQuote() != null && parameter
				.getCart().getInsuranceQuote().getConfigurable() != null;
	}

	private boolean isCommerceOrderResultValid(final CommerceOrderResult orderModel)
	{
		return orderModel.getOrder() != null && orderModel.getOrder().getEntries() != null;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
