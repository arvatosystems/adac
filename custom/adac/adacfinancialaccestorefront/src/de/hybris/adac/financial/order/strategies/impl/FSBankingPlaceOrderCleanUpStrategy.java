/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.order.strategies.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.adac.financial.order.strategies.FSAbstractPlaceOrderCleanUpStrategy;
import de.hybris.platform.financialfacades.constants.FinancialfacadesConstants;
import de.hybris.platform.product.ConfiguratorSettingsService;
import org.springframework.beans.factory.annotation.Required;


/**
 * The class of FSBankingPlaceOrderCleanUpStrategy.
 */
public class FSBankingPlaceOrderCleanUpStrategy extends FSAbstractPlaceOrderCleanUpStrategy
{

	private ConfiguratorSettingsService configuratorSettingsService;

	@Override
	public void handleAfterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel)
	{
		if (!areParametersValid(parameter, orderModel))
		{
			return;
		}

		if (parameter.getCart().getInsuranceQuote().getConfigurable())
		{
			for (final AbstractOrderEntryModel entry : orderModel.getOrder().getEntries())
			{
				if (entry.getProduct() == null)
				{
					continue;
				}

				getConfiguratorSettingsService().getConfiguratorSettingsForProduct(entry.getProduct()).forEach(cfg ->
						//deletes formId for banking configuration form
						getSessionService().removeAttribute(cfg.getId() + FinancialfacadesConstants.FORM_ID));
			}
		}
	}

	protected ConfiguratorSettingsService getConfiguratorSettingsService()
	{
		return configuratorSettingsService;
	}

	@Required
	public void setConfiguratorSettingsService(final ConfiguratorSettingsService configuratorSettingsService)
	{
		this.configuratorSettingsService = configuratorSettingsService;
	}
}
