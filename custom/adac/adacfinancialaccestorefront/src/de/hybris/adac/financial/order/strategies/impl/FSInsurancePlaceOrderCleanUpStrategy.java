/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.order.strategies.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.adac.financial.order.strategies.FSAbstractPlaceOrderCleanUpStrategy;
import de.hybris.platform.financialfacades.constants.FinancialfacadesConstants;


/**
 * The class of FSInsurancePlaceOrderCleanUpStrategy.
 */
public class FSInsurancePlaceOrderCleanUpStrategy extends FSAbstractPlaceOrderCleanUpStrategy
{

	private static final String CART = "cart";

	@Override
	public void handleAfterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel)
	{
		if (!areParametersValid(parameter, orderModel))
		{
			return;
		}

		for (final AbstractOrderEntryModel entry : orderModel.getOrder().getEntries())
		{
			if (entry.getProduct() == null || getSessionService().getAttribute(CART) == null
					|| ((CartModel) getSessionService().getAttribute(CART)).getInsuranceQuote() == null)
			{
				continue;
			}

			if (entry.getProduct().getDefaultCategory() != null)
			{
				final String categoryCode = entry.getProduct().getDefaultCategory().getCode();
				final String formIdKey = categoryCode + FinancialfacadesConstants.FORM_ID;
				final String formId = getSessionService().getAttribute(formIdKey);
				final String insuranceQuoteFormId = (String) ((CartModel) getSessionService().getAttribute(CART))
						.getInsuranceQuote().getProperties().get(FinancialfacadesConstants.FORM_ID);

				if (formId != null && insuranceQuoteFormId != null && formId.equals(insuranceQuoteFormId))
				{
					((CartModel) getSessionService().getAttribute(CART)).setInsuranceQuote(null);
					//deletes formId for insurance form
					getSessionService().removeAttribute(categoryCode + FinancialfacadesConstants.FORM_ID);
				}
			}
		}
	}
}
