/**
 *
 */
package de.hybris.adac.financial.payment.forms;

/**
 * @author MoleSoft-V
 *
 */
public class PaymentDetailsBankAccountForm
{


	private String iban;

	private String firstName;
	private String lastName;
	private String line1;
	private String line2;
	private String postcode;
	private String townCity;
	private String gender;
	private Boolean alternativeAccountHolder;


	public PaymentDetailsBankAccountForm()
	{
		super();
		// YTODO Auto-generated constructor stub
	}


	/**
	 * @return the iban
	 */
	public String getIban()
	{
		return iban;
	}


	/**
	 * @param iban
	 *           the iban to set
	 */
	public void setIban(final String iban)
	{
		this.iban = iban;
	}


	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}


	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}


	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}


	/**
	 * @return the line1
	 */
	public String getLine1()
	{
		return line1;
	}


	/**
	 * @param line1
	 *           the line1 to set
	 */
	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}


	/**
	 * @return the line2
	 */
	public String getLine2()
	{
		return line2;
	}


	/**
	 * @param line2
	 *           the line2 to set
	 */
	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}


	/**
	 * @return the postcode
	 */
	public String getPostcode()
	{
		return postcode;
	}


	/**
	 * @param postcode
	 *           the postcode to set
	 */
	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}


	/**
	 * @return the townCity
	 */
	public String getTownCity()
	{
		return townCity;
	}


	/**
	 * @param townCity
	 *           the townCity to set
	 */
	public void setTownCity(final String townCity)
	{
		this.townCity = townCity;
	}


	/**
	 * @return the gender
	 */
	public String getGender()
	{
		return gender;
	}


	/**
	 * @param gender
	 *           the gender to set
	 */
	public void setGender(final String gender)
	{
		this.gender = gender;
	}


	/**
	 * @return the alternativeAccountHolder
	 */
	public Boolean getAlternativeAccountHolder()
	{
		return alternativeAccountHolder;
	}


	/**
	 * @param alternativeAccountHolder
	 *           the alternativeAccountHolder to set
	 */
	public void setAlternativeAccountHolder(final Boolean alternativeAccountHolder)
	{
		this.alternativeAccountHolder = alternativeAccountHolder;
	}


}
