/**
 *
 */
package de.hybris.adac.financial.service;

import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


/**
 * @author MoleSoft-V
 *
 */
public interface ADACCustomerAccountService
{

	List<DebitPaymentInfoModel> getDebitCardPaymentInfos(CustomerModel customerModel, boolean saved);

}
