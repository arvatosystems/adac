/**
 *
 */
package de.hybris.adac.financial.service;

import de.hybris.adac.financial.payment.forms.PaymentDetailsBankAccountForm;

/**
 * @author MoleSoft-V
 *
 */
public interface DebitPaymentInfoService
{

	void saveDebitPaymentInfo(PaymentDetailsBankAccountForm form);

}
