/**
 *
 */
package de.hybris.adac.financial.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.adac.financial.service.ADACCustomerAccountService;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.ArrayList;
import java.util.List;


/**
 * @author MoleSoft-V
 *
 */
public class ADACCustomerAccountServiceImpl implements ADACCustomerAccountService
{

	@Override
	public List<DebitPaymentInfoModel> getDebitCardPaymentInfos(final CustomerModel customerModel, final boolean saved)
	{
		validateParameterNotNull(customerModel, "Customer model cannot be null");

		final List<DebitPaymentInfoModel> paymentModel = new ArrayList<DebitPaymentInfoModel>();

		for (final PaymentInfoModel paymentInfoModel : customerModel.getPaymentInfos())
		{

			if (paymentInfoModel instanceof DebitPaymentInfoModel)
			{
				paymentModel.add((DebitPaymentInfoModel) paymentInfoModel);
			}
		}

		return paymentModel;
	}

}
