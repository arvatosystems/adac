/**
 *
 */
package de.hybris.adac.financial.service.impl;


import de.hybris.adac.financial.payment.forms.PaymentDetailsBankAccountForm;
import de.hybris.adac.financial.service.DebitPaymentInfoService;
import de.hybris.adac.integrationservices.outbound.services.AdacOutboundIntegrationService;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.financialservices.model.InsurancePolicyModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import de.hybis.adac.financial.payment.IBANUtil;
import de.hybis.adac.financial.payment.IBANValidator;


/**
 * @author MoleSoft-V
 *
 */
public class DebitPaymetInfoServiceImpl implements DebitPaymentInfoService
{

	@Autowired
	protected ModelService modelService;

	@Autowired
	private UserService userService;

	@Resource(name = "adacOutboundDebitPaymentInfoIntegrationService")
	private AdacOutboundIntegrationService outboundIntegrationService;

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public void saveDebitPaymentInfo(final PaymentDetailsBankAccountForm form)
	{

		final DebitPaymentInfoModel debitPaymentModel = new DebitPaymentInfoModel();
		final CustomerModel currentUser = (CustomerModel) userService.getCurrentUser();
		final IBANValidator ibanVlidator = new IBANValidator();
		final AddressModel addressModel = new AddressModel();


		if (!form.getIban().isEmpty())
		{

			final String accountNumber = IBANUtil.getAccountNumber(form.getIban());
			final String bankAccountNumber = IBANUtil.getBankCode(form.getIban());
			final String bank = IBANUtil.getBban(form.getIban());
			final String countryCode = IBANUtil.getCountryCodeAndCheckDigit(form.getIban());

			debitPaymentModel.setAccountNumber(accountNumber);
			debitPaymentModel.setBankIDNumber(bankAccountNumber);
			debitPaymentModel.setBank(bank);
			debitPaymentModel.setCode(countryCode);

			if (currentUser != null)
			{
				debitPaymentModel.setBaOwner(currentUser.getName());
				debitPaymentModel.setUser(currentUser);
			}

			if (form.getAlternativeAccountHolder().equals(true))
			{

				addressModel.setGender(Gender.valueOf(form.getGender()));

				addressModel.setOwner(currentUser);
				addressModel.setFirstname(form.getFirstName());
				addressModel.setLastname(form.getLastName());
				addressModel.setLine1(form.getLine1());
				addressModel.setLine2(form.getLine2());
				addressModel.setPostalcode(form.getPostcode());
				addressModel.setTown(form.getTownCity());

				debitPaymentModel.setBillingAddress(addressModel);

			}
		}

		currentUser.setDefaultPaymentInfo(debitPaymentModel);
		modelService.save(debitPaymentModel);

		outboundIntegrationService.execute(debitPaymentModel);

		if (!currentUser.getInsurancePolicies().isEmpty())
		{
			getCurrentUserPolicies(currentUser, debitPaymentModel);
		}


	}

	private InsurancePolicyModel getCurrentUserPolicies(final CustomerModel currentUser,
			final DebitPaymentInfoModel debitPaymentInfoModel)
	{

		final Collection<InsurancePolicyModel> policiesModel = currentUser.getInsurancePolicies();

		if (!policiesModel.isEmpty())
		{
			for (final InsurancePolicyModel pModel : policiesModel)
			{
				if (StringUtils.equals(currentUser.getUid(), pModel.getUser().getUid()))
				{
					pModel.setPaymentMethod(debitPaymentInfoModel);
					modelService.save(pModel);
				}
			}
		}
		return null;
	}

}
