/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.financialservices.enums.FSClaimStatus;
import de.hybris.platform.financialservices.model.FSClaimModel;
import de.hybris.platform.financialservices.model.process.ClaimProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.log4j.Logger;


/**
 * The type Check claim action.
 */
public class CheckClaimAction extends AbstractSimpleDecisionAction<ClaimProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CheckClaimAction.class);

	@Override
	public Transition executeAction(final ClaimProcessModel process)
	{

		final FSClaimModel claim = process.getClaim();

		if (claim == null)
		{
			LOG.error("Missing the claim, exiting the process");
			return Transition.NOK;
		}

		if ((FSClaimStatus.PROCESSING).equals(claim.getClaimStatus()))
		{
			return Transition.OK;
		}
		else
		{
			LOG.error("Claim is not in processing status, exiting the process");
			return Transition.NOK;
		}
	}
}
