/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.financialservices.services.FSUserLinkingService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * The type Execute linking action.
 */
public class ExecuteUserLinkingAction extends AbstractSimpleDecisionAction<StoreFrontCustomerProcessModel>
{
	private static final Logger LOG = Logger.getLogger(ExecuteUserLinkingAction.class);
	private FSUserLinkingService fsUserLinkingService;
	private UserService userService;

	@Override
	public Transition executeAction(final StoreFrontCustomerProcessModel process)
	{
		UserModel user = process.getCustomer();

		if (user == null)
		{
			LOG.error("Missing the user, exiting the process");
			return AbstractSimpleDecisionAction.Transition.NOK;
		}

		if (!getFsUserLinkingService().isUserLinked(user))
		{
			user = getFsUserLinkingService().linkRegisteredUserWithExternalSystem(user);

			if (StringUtils.isEmpty(user.getExternalId()))
			{
				LOG.error("User linking is not successful, exiting the process");
				return AbstractSimpleDecisionAction.Transition.NOK;
			}
			LOG.info("User is successfully linked with external system");
			return AbstractSimpleDecisionAction.Transition.OK;
		}
		else
		{
			LOG.info("User is previously linked or linking is not enabled, exiting the process");
			return AbstractSimpleDecisionAction.Transition.NOK;
		}
	}

	protected FSUserLinkingService getFsUserLinkingService()
	{
		return fsUserLinkingService;
	}

	@Required
	public void setFsUserLinkingService(FSUserLinkingService fsUserLinkingService)
	{
		this.fsUserLinkingService = fsUserLinkingService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(UserService userService)
	{
		this.userService = userService;
	}
}
