/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.financialservices.enums.IdentificationType;
import de.hybris.platform.financialservices.events.OrderPendingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashSet;
import java.util.Set;


/**
 * The type Identify user action.
 */
public class IdentifyUserAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(IdentifyUserAction.class);

	private EventService eventService;

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	@Override
	public final String execute(final OrderProcessModel process) throws Exception
	{
		return executeAction(process).toString();
	}

	public Transition executeAction(final OrderProcessModel orderProcessModel) throws Exception
	{
		final OrderModel order = orderProcessModel.getOrder();
		IdentificationType identificationType;

		Transition transition = Transition.NOK;

		if (order != null)
		{
			order.setStatus(OrderStatus.WAIT_USER_IDENTIFICATION);

			identificationType = order.getIdentificationType();
			if (IdentificationType.VIDEO_IDENTIFICATION.equals(identificationType))
			{
				order.setStatus(OrderStatus.USER_VERIFIED);
				transition = Transition.OK;
			}
			else
			{
				eventService.publishEvent(new OrderPendingEvent(orderProcessModel));
				LOG.info("Process: " + orderProcessModel.getCode() + " in step " + getClass());
				transition = Transition.WAIT;
			}
			getModelService().save(order);
		}

		return transition;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	public enum Transition
	{
		OK, NOK, WAIT;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();
			for (final Transition transitions : Transition.values())
			{
				res.add(transitions.toString());
			}
			return res;
		}
	}
}
