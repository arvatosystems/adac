/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.task.RetryLaterException;


/**
 * The type Payment check action.
 */
public class PaymentCheckAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	ConfigurationService configurationService;

	@Override
	public Transition executeAction(final OrderProcessModel process) throws RetryLaterException
	{
		final String siteName = process.getOrder().getSite().getUid();

		final String propertyPaymentEnabled = AdacfinancialaccestorefrontConstants.FINANCIAL_STOREFRONT + "." + siteName + "" +
				"." + AdacfinancialaccestorefrontConstants.PAYMENT_ENABLED;

		final Boolean isPaymentEnabled = getConfigurationService().getConfiguration().getBoolean(propertyPaymentEnabled, false);

		return isPaymentEnabled ? Transition.OK : Transition.NOK;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
