/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.financialservices.model.process.ClaimProcessModel;
import de.hybris.platform.financialservices.events.FSClaimSubmittedEvent;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * The type Send claim status confirmation email action.
 */
public class SendClaimStatusConfirmationEmailAction extends AbstractProceduralAction<ClaimProcessModel>
{

	private static final Logger LOG = Logger.getLogger(SendClaimStatusConfirmationEmailAction.class);
	private EventService eventService;

	@Override
	public void executeAction(final ClaimProcessModel process)
	{
		getEventService().publishEvent(new FSClaimSubmittedEvent(process.getClaim(),process.getSite(), process.getStore(), process.getLanguage()));
		if (LOG.isInfoEnabled())
		{
			LOG.info("Process: " + process.getCode() + " in step " + getClass());
		}
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}

