/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.financialservices.events.UserRejectedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * The type Send rejection email action.
 */
public class SendRejectionEmailAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendRejectionEmailAction.class);

	private EventService eventService;

	@Override
	public void executeAction(final OrderProcessModel orderProcessModel) throws Exception
	{
		final OrderModel orderModel = orderProcessModel.getOrder();

		if (orderModel != null)
		{
			eventService.publishEvent(new UserRejectedEvent(orderProcessModel));
			LOG.info("Process: " + orderProcessModel.getCode() + " in step " + getClass());
		}
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}
}
