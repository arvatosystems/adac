/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.financialservices.services.impl.AbstractFSUnderwritingService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Map;


/**
 * The type Simple underwriting check order action.
 */
public class SimpleUnderwritingCheckOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private Map<String, List<AbstractFSUnderwritingService>> underwritingServices;

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel orderModel = process.getOrder();

		final List<AbstractOrderEntryModel> orderEntryModels = orderModel.getEntries();
		if (CollectionUtils.isEmpty(orderEntryModels))
		{
			setOrderStatus(orderModel, OrderStatus.APPLICATION_REJECTED);
			return Transition.NOK;
		}
		// Perform underwriting for every product from the order
		for (final AbstractOrderEntryModel orderEntryModel : orderEntryModels)
		{
			if (orderEntryModel.getProduct() == null)
			{
				setOrderStatus(orderModel, OrderStatus.APPLICATION_REJECTED);
				return Transition.NOK;
			}

			final String productCode = orderEntryModel.getProduct().getCode();

			if (getUnderwritingServices().containsKey(productCode))
			{
				final List<AbstractFSUnderwritingService> underwritingServiceList = getUnderwritingServices().get(productCode);
				for (final AbstractFSUnderwritingService underwritingService : underwritingServiceList)
				{
					final OrderStatus orderStatus = underwritingService.performUnderwriting(orderModel);
					setOrderStatus(orderModel, orderStatus);

					// If result for any product is negative, reject the application
					if (OrderStatus.APPLICATION_REJECTED.equals(orderModel.getStatus())) //NOSONAR
					{
						return Transition.NOK;
					}
				}
			}
		}
		return Transition.OK;
	}

	public Map<String, List<AbstractFSUnderwritingService>> getUnderwritingServices()
	{
		return underwritingServices;
	}

	public void setUnderwritingServices(
			Map<String, List<AbstractFSUnderwritingService>> underwritingServices)
	{
		this.underwritingServices = underwritingServices;
	}
}
