/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.financialservices.model.process.ClaimProcessModel;
import de.hybris.platform.financialfacades.facades.FSClaimSubmitFacade;
import de.hybris.platform.financialservices.model.FSClaimModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * The type Submit claim action.
 */
public class SubmitClaimAction extends AbstractSimpleDecisionAction<ClaimProcessModel>
{

	private static final Logger LOG = Logger.getLogger(SubmitClaimAction.class);
	private FSClaimSubmitFacade claimSubmitFacade;
	private CatalogVersionService catalogVersionService;

	@Override
	public Transition executeAction(final ClaimProcessModel process)
	{
		final FSClaimModel claim = process.getClaim();

		if (claim == null)
		{
			LOG.error("Missing the claim, exiting the process");
			return Transition.NOK;
		}
		else
		{
			getCatalogVersionService().setSessionCatalogVersions(process.getSessionCatalogVersions());
			getClaimSubmitFacade().submitClaim(claim.getClaimNumber());
			return Transition.OK;
		}
	}

	protected FSClaimSubmitFacade getClaimSubmitFacade()
	{
		return claimSubmitFacade;
	}

	@Required
	public void setClaimSubmitFacade(FSClaimSubmitFacade claimSubmitFacade)
	{
		this.claimSubmitFacade = claimSubmitFacade;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
