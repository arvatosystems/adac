/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.process.actions;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.log4j.Logger;


/**
 * The type Verify identification result action.
 */
public class VerifyIdentificationResultAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(VerifyIdentificationResultAction.class);

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel orderModel = process.getOrder();

		if (orderModel == null)
		{
			LOG.error("Missing the order - Trasition.NOK");
			return Transition.NOK;
		}

		if (OrderStatus.USER_VERIFIED.equals(orderModel.getStatus()))
		{
			return Transition.OK;
		}
		else if (OrderStatus.USER_REJECTED.equals(orderModel.getStatus()))
		{
			LOG.error("User is rejected - Trasition.NOK");
			return Transition.NOK;
		}

		return Transition.OK;
	}
}
