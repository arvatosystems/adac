/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCustomerNameStrategy;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storefront.form.data.FormDetailData;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Locale;
import java.util.Map;


/**
 * The class of InsurancePersonalDetailsTransformerYFormPreprocessorStrategyTest.
 */
@UnitTest
public class InsurancePersonalDetailsTransformerYFormPreprocessorStrategyTest
{
	@Mock
	private UserService userService;
	@Mock
	private CustomerModel customerModel;
	@Mock
	private TitleModel titleModel;
	@Mock
	private LanguageModel languageModel;
	@Mock
	private InsuranceYFormDataPreprocessorStrategy insuranceYFormDataPreprocessorStrategy;
	@Mock
	private DefaultCustomerNameStrategy defaultCustomerNameStrategy;
	@InjectMocks
	private InsurancePersonalDetailsTransformerYFormPreprocessorStrategy preprocessorStrategy;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testTransform() throws YFormProcessorException
	{
		final String title = "Mr.";
		final String firstName = "Name";
		final String lastName = "LastName";
		final String email = "test@email.com";
		final String[] fullName = { firstName, lastName };

		final String xmlContent =
				"<form><personal-details>"
						+ "<title/>"
						+ "<first-name/>"
						+ "<last-name/>"
						+ "<address-line1/>"
						+ "<address-line2/>"
						+ "<address-city/>"
						+ "<address-postcode/>"
						+ "<address-country/>"
						+ "<phone/>"
						+ "<email/>"
						+ "</personal-details>"
						+ "</form>";

		final String expectedXmlContent =
				"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><form><personal-details>"
						+ "<title>" + title + "</title>"
						+ "<first-name>" + firstName + "</first-name>"
						+ "<last-name>" + lastName + "</last-name>"
						+ "<address-line1/>"
						+ "<address-line2/>"
						+ "<address-city/>"
						+ "<address-postcode/>"
						+ "<address-country/>"
						+ "<phone/>"
						+ "<email>" + email + "</email>"
						+ "</personal-details>"
						+ "</form>";

		final FormDetailData data = new FormDetailData();
		data.setOrderEntryNumber(0);

		final Map<String, Object> params = Maps.newHashMap();
		params.put(InsuranceYFormDataPreprocessorStrategy.FORM_DETAIL_DATA, data);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);

		Mockito.when(insuranceYFormDataPreprocessorStrategy.getCustomerNameStrategy()).thenReturn(defaultCustomerNameStrategy);

		Mockito.when(insuranceYFormDataPreprocessorStrategy.getCustomerNameStrategy().splitName(customerModel.getName()))
				.thenReturn(fullName);

		Mockito.when(customerModel.getSessionLanguage()).thenReturn(languageModel);

		Mockito.when(languageModel.getIsocode()).thenReturn(Locale.US.toString());

		Mockito.when(customerModel.getTitle()).thenReturn(titleModel);

		Mockito.when(titleModel.getName(Matchers.any(Locale.class))).thenReturn(title);

		Mockito.when(customerModel.getContactEmail()).thenReturn(email);

		final String resultXml = preprocessorStrategy.transform(xmlContent, params);

		Assert.assertEquals(expectedXmlContent, resultXml);
	}
}
