/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCustomerNameStrategy;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storefront.form.data.FormDetailData;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * Tests for the {@link SavingsPersonalDetailsTransformerYFormPreprocessorStrategy}.
 */
@UnitTest
public class SavingsPersonalDetailsTransformerYFormPreprocessorStrategyTest
{
	@Mock
	private UserService userService;
	@Mock
	private CartService cartService;
	@Mock
	private CustomerModel customerModel;
	@Mock
	private TitleModel titleModel;
	@Mock
	private LanguageModel languageModel;
	@Mock
	private InsuranceYFormDataPreprocessorStrategy insuranceYFormDataPreprocessorStrategy;
	@Mock
	private DefaultCustomerNameStrategy defaultCustomerNameStrategy;
	@InjectMocks
	private SavingsPersonalDetailsTransformerYFormPreprocessorStrategy preprocessorStrategy;

	private static final String XML_CONTENT = "<form>"
			+ "<saving-survivor-pension/>"
			+ "<savings-dependent-children-pension/>"
			+ "<personal-details>"
			+ "<title/>"
			+ "<first-name/>"
			+ "<last-name/>"
			+ "<address-line1/>"
			+ "<address-line2/>"
			+ "<address-city/>"
			+ "<address-postcode/>"
			+ "<address-country/>"
			+ "<phone/>"
			+ "<email/>"
			+ "</personal-details>"
			+ "</form>";

	private static final String EXPECTED_SURVIVOR_PENSION_XML_CONTENT = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
			+ "<form><saving-survivor-pension>true</saving-survivor-pension>"
			+ "<savings-dependent-children-pension>false</savings-dependent-children-pension>"
			+ "<personal-details>"
			+ "<title>Mr.</title>"
			+ "<first-name>Name</first-name>"
			+ "<last-name>LastName</last-name>"
			+ "<address-line1/><address-line2/>"
			+ "<address-city/>"
			+ "<address-postcode/>"
			+ "<address-country/>"
			+ "<phone/>"
			+ "<email>test@email.com</email>"
			+ "</personal-details></form>";

	private static final String EXPECTED_DEPENDENT_CHILDREN_PENSION_XML_CONTENT = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
			+ "<form><saving-survivor-pension>false</saving-survivor-pension>"
			+ "<savings-dependent-children-pension>true</savings-dependent-children-pension>"
			+ "<personal-details>"
			+ "<title>Mr.</title>"
			+ "<first-name>Name</first-name>"
			+ "<last-name>LastName</last-name>"
			+ "<address-line1/><address-line2/>"
			+ "<address-city/>"
			+ "<address-postcode/>"
			+ "<address-country/>"
			+ "<phone/>"
			+ "<email>test@email.com</email>"
			+ "</personal-details></form>";

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testTransformWithSurvivorPension() throws YFormProcessorException
	{
		final CartModel cartModel = new CartModel();
		final AbstractOrderEntryModel abstractOrderEntryModel = new AbstractOrderEntryModel();
		final ProductModel productModel = new ProductModel();

		productModel.setCode("SAVINGS_SURVIVOR_PENSION");
		abstractOrderEntryModel.setProduct(productModel);

		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList<>();

		abstractOrderEntryModelList.add(abstractOrderEntryModel);
		cartModel.setEntries(abstractOrderEntryModelList);

		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);

		final String title = "Mr.";
		final String firstName = "Name";
		final String lastName = "LastName";
		final String email = "test@email.com";
		final String[] fullName = { firstName, lastName };

		final FormDetailData data = new FormDetailData();
		data.setOrderEntryNumber(0);

		final Map<String, Object> params = Maps.newHashMap();
		params.put(InsuranceYFormDataPreprocessorStrategy.FORM_DETAIL_DATA, data);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(insuranceYFormDataPreprocessorStrategy.getCustomerNameStrategy()).thenReturn(defaultCustomerNameStrategy);
		Mockito.when(insuranceYFormDataPreprocessorStrategy.getCustomerNameStrategy().splitName(customerModel.getName()))
				.thenReturn(fullName);
		Mockito.when(customerModel.getSessionLanguage()).thenReturn(languageModel);
		Mockito.when(languageModel.getIsocode()).thenReturn(Locale.US.toString());
		Mockito.when(customerModel.getTitle()).thenReturn(titleModel);
		Mockito.when(titleModel.getName(Matchers.any(Locale.class))).thenReturn(title);
		Mockito.when(customerModel.getContactEmail()).thenReturn(email);

		final String resultXml = preprocessorStrategy.transform(XML_CONTENT, params);

		Assert.assertEquals(EXPECTED_SURVIVOR_PENSION_XML_CONTENT, resultXml);
	}

	@Test
	public void testTransformWithDependentSavings() throws YFormProcessorException
	{
		final CartModel cartModel = new CartModel();
		final AbstractOrderEntryModel abstractOrderEntryModel = new AbstractOrderEntryModel();
		final ProductModel productModel = new ProductModel();

		productModel.setCode("SAVINGS_DEPENDENT_CHILDREN_PENSION");
		abstractOrderEntryModel.setProduct(productModel);

		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList<>();

		abstractOrderEntryModelList.add(abstractOrderEntryModel);
		cartModel.setEntries(abstractOrderEntryModelList);

		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);

		final String title = "Mr.";
		final String firstName = "Name";
		final String lastName = "LastName";
		final String email = "test@email.com";
		final String[] fullName = { firstName, lastName };



		final FormDetailData data = new FormDetailData();
		data.setOrderEntryNumber(0);

		final Map<String, Object> params = Maps.newHashMap();
		params.put(InsuranceYFormDataPreprocessorStrategy.FORM_DETAIL_DATA, data);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(insuranceYFormDataPreprocessorStrategy.getCustomerNameStrategy()).thenReturn(defaultCustomerNameStrategy);
		Mockito.when(insuranceYFormDataPreprocessorStrategy.getCustomerNameStrategy().splitName(customerModel.getName()))
				.thenReturn(fullName);
		Mockito.when(customerModel.getSessionLanguage()).thenReturn(languageModel);
		Mockito.when(languageModel.getIsocode()).thenReturn(Locale.US.toString());
		Mockito.when(customerModel.getTitle()).thenReturn(titleModel);
		Mockito.when(titleModel.getName(Matchers.any(Locale.class))).thenReturn(title);
		Mockito.when(customerModel.getContactEmail()).thenReturn(email);

		final String resultXml = preprocessorStrategy.transform(XML_CONTENT, params);

		Assert.assertEquals(EXPECTED_DEPENDENT_CHILDREN_PENSION_XML_CONTENT, resultXml);
	}
}
