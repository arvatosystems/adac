/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.order.strategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.adac.financial.order.strategies.impl.FSBankingPlaceOrderCleanUpStrategy;
import de.hybris.platform.financialfacades.constants.FinancialfacadesConstants;
import de.hybris.platform.financialservices.model.InsuranceQuoteModel;
import de.hybris.platform.product.ConfiguratorSettingsService;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.Mockito.when;


/**
 * Tests for the {@link FSBankingPlaceOrderCleanUpStrategy}.
 */
@UnitTest
public class FSBankingPlaceOrderCleanUpStrategyTest
{
	private static final String TEST_FORM_ID = "credit_card_premium-configFormId";

	@Mock
	private SessionService sessionService;
	@Mock
	private ConfiguratorSettingsService configuratorSettingsService;
	@Mock
	private ProductModel product;
	@Mock
	private AbstractOrderEntryModel entry;
	@Mock
	private OrderModel order;
	@Mock
	private CommerceOrderResult orderModel;
	@Mock
	private AbstractConfiguratorSettingModel cfg;
	@Mock
	private InsuranceQuoteModel insuranceQuote;
	@Mock
	private CartModel cart = new CartModel();
	@Mock
	private CommerceCheckoutParameter parameter;
	@InjectMocks
	private FSBankingPlaceOrderCleanUpStrategy fsBankingPlaceOrderCleanUpStrategy;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		when(configuratorSettingsService.getConfiguratorSettingsForProduct(product)).thenReturn(Collections.singletonList(cfg));
		when(cfg.getId()).thenReturn(TEST_FORM_ID);
		when(parameter.getCart()).thenReturn(cart);
		when(cart.getInsuranceQuote()).thenReturn(insuranceQuote);
		when(insuranceQuote.getConfigurable()).thenReturn(true);
		when(orderModel.getOrder()).thenReturn(order);
		when(order.getEntries()).thenReturn(Collections.singletonList(entry));
		when(entry.getProduct()).thenReturn(product);
	}

	@Test
	public void testDeleteFormIdInSession()
	{
		fsBankingPlaceOrderCleanUpStrategy.handleAfterPlaceOrder(parameter, orderModel);

		Mockito.verify(sessionService, Mockito.times(1)).removeAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
		Mockito.verifyNoMoreInteractions(sessionService);
	}

	@Test
	public void testFormIdInSessionNotDeletedWithNullParameter()
	{
		fsBankingPlaceOrderCleanUpStrategy.handleAfterPlaceOrder(null, orderModel);

		Mockito.verify(sessionService, Mockito.times(0)).removeAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
		Mockito.verifyNoMoreInteractions(sessionService);
	}

	@Test
	public void testFormIdInSessionNotDeletedWithNullOrder()
	{
		when(orderModel.getOrder()).thenReturn(null);

		fsBankingPlaceOrderCleanUpStrategy.handleAfterPlaceOrder(parameter, orderModel);

		Mockito.verify(sessionService, Mockito.times(0)).removeAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
		Mockito.verifyNoMoreInteractions(sessionService);
	}
}
