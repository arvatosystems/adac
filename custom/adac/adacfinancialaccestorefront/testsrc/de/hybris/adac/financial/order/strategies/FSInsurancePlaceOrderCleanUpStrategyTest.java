/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.order.strategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.adac.financial.order.strategies.impl.FSInsurancePlaceOrderCleanUpStrategy;
import de.hybris.platform.financialfacades.constants.FinancialfacadesConstants;
import de.hybris.platform.financialservices.model.InsuranceQuoteModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Map;

import static org.mockito.Mockito.when;


/**
 * Tests for the {@link FSInsurancePlaceOrderCleanUpStrategy}.
 */
@UnitTest
public class FSInsurancePlaceOrderCleanUpStrategyTest
{
	private static final String CART_STRING = "cart";
	private static final String TEST_FORM_ID = "insurances_auto";

	@Mock
	private SessionService sessionService;
	@Mock
	private CategoryModel category;
	@Mock
	private ProductModel product;
	@Mock
	private AbstractOrderEntryModel entry;
	@Mock
	private OrderModel order;
	@Mock
	private CommerceOrderResult orderModel;
	@Mock
	private InsuranceQuoteModel insuranceQuote;
	@Mock
	private CommerceCheckoutParameter parameter;
	@Mock
	private CartModel cart;
	@InjectMocks
	private FSInsurancePlaceOrderCleanUpStrategy fsInsurancePlaceOrderCleanUpStrategy;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		Map<String, Object> properties = Collections
				.singletonMap(FinancialfacadesConstants.FORM_ID, TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);

		when(parameter.getCart()).thenReturn(cart);
		when(parameter.getCart().getInsuranceQuote()).thenReturn(insuranceQuote);
		when(parameter.getCart().getInsuranceQuote().getConfigurable()).thenReturn(true);
		when(parameter.getCart().getInsuranceQuote().getProperties()).thenReturn(properties);
		when(sessionService.getAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID))
				.thenReturn(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
		when(sessionService.getAttribute(CART_STRING)).thenReturn(cart);
		when(orderModel.getOrder()).thenReturn(order);
		when(order.getEntries()).thenReturn(Collections.singletonList(entry));
		when(entry.getProduct()).thenReturn(product);
		when(product.getDefaultCategory()).thenReturn(category);
		when(product.getDefaultCategory().getCode()).thenReturn(TEST_FORM_ID);
	}

	@Test
	public void testDeleteFormIdInSession()
	{
		fsInsurancePlaceOrderCleanUpStrategy.handleAfterPlaceOrder(parameter, orderModel);

		Mockito.verify(sessionService, Mockito.times(1)).removeAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
	}

	@Test
	public void testFormIdInSessionNotDeletedWithNullParameter()
	{
		fsInsurancePlaceOrderCleanUpStrategy.handleAfterPlaceOrder(null, orderModel);

		Mockito.verify(sessionService, Mockito.times(0)).removeAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
		Mockito.verifyNoMoreInteractions(sessionService);
	}

	@Test
	public void testFormIdInSessionNotDeletedWithNullOrder()
	{
		when(orderModel.getOrder()).thenReturn(null);

		fsInsurancePlaceOrderCleanUpStrategy.handleAfterPlaceOrder(parameter, orderModel);

		Mockito.verify(sessionService, Mockito.times(0)).removeAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
		Mockito.verifyNoMoreInteractions(sessionService);
	}

	@Test
	public void testFormIdInSessionNotDeletedWithEntryProductNull()
	{
		when(entry.getProduct()).thenReturn(null);

		fsInsurancePlaceOrderCleanUpStrategy.handleAfterPlaceOrder(parameter, orderModel);

		Mockito.verify(sessionService, Mockito.times(0)).removeAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
		Mockito.verifyNoMoreInteractions(sessionService);
	}

	@Test
	public void testFormIdInSessionNotDeletedWithEntryProductDefaultCategoryNull()
	{
		when(product.getDefaultCategory()).thenReturn(null);

		fsInsurancePlaceOrderCleanUpStrategy.handleAfterPlaceOrder(parameter, orderModel);

		Mockito.verify(sessionService, Mockito.times(0)).removeAttribute(TEST_FORM_ID + FinancialfacadesConstants.FORM_ID);
	}
}
