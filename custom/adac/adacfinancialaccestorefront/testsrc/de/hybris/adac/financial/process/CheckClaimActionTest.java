/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.financialservices.enums.FSClaimStatus;
import de.hybris.platform.financialservices.model.FSClaimModel;
import de.hybris.platform.financialservices.model.process.ClaimProcessModel;
import de.hybris.platform.process.actions.CheckClaimAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;


/**
 * Tests for the {@link CheckClaimAction}.
 */
@UnitTest
public class CheckClaimActionTest
{
	private static final String CLAIM_NUMBER = "claimNumber";

	@Mock
	private ClaimProcessModel claimProcessModel;
	@Mock
	private FSClaimModel claimModel;
	@InjectMocks
	private CheckClaimAction checkClaimAction = new CheckClaimAction();

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCheckNullClaim()
	{
		when(claimProcessModel.getClaim()).thenReturn(null);
		Assertions.assertThat(checkClaimAction.executeAction(claimProcessModel))
				.isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);
	}

	@Test
	public void testCheckClaimStatusProcessing()
	{
		when(claimModel.getClaimStatus()).thenReturn(FSClaimStatus.PROCESSING);

		when(claimProcessModel.getClaim()).thenReturn(claimModel);
		when(claimProcessModel.getClaim().getClaimNumber()).thenReturn(CLAIM_NUMBER);

		Assertions.assertThat(checkClaimAction.executeAction(claimProcessModel))
				.isEqualTo(AbstractSimpleDecisionAction.Transition.OK);
	}

	@Test
	public void testCheckClaimOpenStatus()
	{
		when(claimModel.getClaimStatus()).thenReturn(FSClaimStatus.OPEN);

		when(claimProcessModel.getClaim()).thenReturn(claimModel);
		when(claimProcessModel.getClaim().getClaimNumber()).thenReturn(CLAIM_NUMBER);

		Assertions.assertThat(checkClaimAction.executeAction(claimProcessModel))
				.isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);
	}
}
