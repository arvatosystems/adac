/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.financialservices.services.FSUserLinkingService;
import de.hybris.platform.process.actions.ExecuteUserLinkingAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;


@UnitTest
public class ExecuteUserLinkingActionTest
{
	private static final String USER_EXTERNAL_LINKING_ENABLED = "user.external.linking.enabled";

	@InjectMocks
	private ExecuteUserLinkingAction executeUserLinkingAction;
	@Mock
	private UserService userService;
	@Mock
	private FSUserLinkingService fsUserLinkingService;

	private StoreFrontCustomerProcessModel processModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		processModel = new StoreFrontCustomerProcessModel();
	}

	@Test
	public void testExecuteNullUser()
	{
		Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK, executeUserLinkingAction.executeAction(processModel));
	}

	@Test
	public void testExecuteLinkedUser()
	{
		final CustomerModel customerModel = new CustomerModel();
		processModel.setCustomer(customerModel);

		given(fsUserLinkingService.isUserLinked(customerModel)).willReturn(Boolean.TRUE);

		Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK, executeUserLinkingAction.executeAction(processModel));
	}

	@Test
	public void testExecuteNotLinkedUser()
	{
		final CustomerModel customerModel = new CustomerModel();
		processModel.setCustomer(customerModel);

		given(fsUserLinkingService.isUserLinked(customerModel)).willReturn(Boolean.FALSE);

		final UserModel linkedUserModel = new UserModel();
		linkedUserModel.setExternalId("externalId");

		given(fsUserLinkingService.linkRegisteredUserWithExternalSystem(customerModel)).willReturn(linkedUserModel);

		final AbstractSimpleDecisionAction.Transition transitionState = executeUserLinkingAction.executeAction(processModel);
		verify(fsUserLinkingService).linkRegisteredUserWithExternalSystem(customerModel);
		Assert.assertEquals(AbstractSimpleDecisionAction.Transition.OK, transitionState);
	}

	@Test
	public void testExecuteNotLinkedUserWithIntegrationError()
	{
		final CustomerModel customerModel = new CustomerModel();
		processModel.setCustomer(customerModel);

		given(fsUserLinkingService.isUserLinked(customerModel)).willReturn(Boolean.FALSE);
		given(fsUserLinkingService.linkRegisteredUserWithExternalSystem(customerModel)).willReturn(customerModel);

		final AbstractSimpleDecisionAction.Transition transitionState = executeUserLinkingAction.executeAction(processModel);
		verify(fsUserLinkingService).linkRegisteredUserWithExternalSystem(customerModel);
		Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK, transitionState);
	}

}
