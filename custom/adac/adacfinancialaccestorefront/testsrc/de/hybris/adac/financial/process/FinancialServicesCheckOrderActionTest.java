/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.financialservices.services.FinancialServicesCheckOrderService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.process.actions.FinancialServicesCheckOrderAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.model.ModelService;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


/**
 * Tests for the {@link FinancialServicesCheckOrderAction}.
 */
@UnitTest
public class FinancialServicesCheckOrderActionTest
{
	@Mock
	private FinancialServicesCheckOrderService financialServicesCheckOrderService;
	@Mock
	private OrderProcessModel orderProcessModel;
	@Mock
	private OrderModel orderModel;
	@Mock
	private ModelService modelService;
	@InjectMocks
	private FinancialServicesCheckOrderAction financialServicesCheckOrderAction = new FinancialServicesCheckOrderAction();

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(orderProcessModel.getOrder()).thenReturn(orderModel);
		doNothing().when(modelService).save(any());
	}

	@Test
	public void testNullOrderAction()
	{
		when(orderProcessModel.getOrder()).thenReturn(null);
		Assertions.assertThat(financialServicesCheckOrderAction.executeAction(orderProcessModel))
				.isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);
	}

	@Test
	public void testCheckOrderStatusValid()
	{
		when(financialServicesCheckOrderService.check(any(OrderModel.class))).thenReturn(true);
		Assertions.assertThat(financialServicesCheckOrderAction.executeAction(orderProcessModel))
				.isEqualTo(AbstractSimpleDecisionAction.Transition.OK);
	}

	@Test
	public void testCheckOrderStatusInvalid()
	{
		when(financialServicesCheckOrderService.check(any(OrderModel.class))).thenReturn(false);

		Assertions.assertThat(financialServicesCheckOrderAction.executeAction(orderProcessModel))
				.isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);
	}

}
