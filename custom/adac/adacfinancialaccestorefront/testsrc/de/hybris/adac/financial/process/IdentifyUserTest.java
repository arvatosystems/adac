/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.financialservices.enums.IdentificationType;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.process.actions.IdentifyUserAction;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Tests for the {@link IdentifyUserAction}.
 */
@UnitTest
public class IdentifyUserTest
{
	@Mock
	private ModelService modelService;
	@Mock
	private EventService eventService;
	@InjectMocks
	private IdentifyUserAction identifyUserAction;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testExecuteActionWithWebIdIdentificationType() throws Exception // NOSONAR
	{
		final OrderProcessModel orderProcessModel = new OrderProcessModel();

		final OrderModel order = new OrderModel();
		orderProcessModel.setOrder(order);
		order.setIdentificationType(IdentificationType.VIDEO_IDENTIFICATION);

		identifyUserAction.execute(orderProcessModel);

		Assertions.assertThat(order.getStatus()).isEqualTo(OrderStatus.USER_VERIFIED);
	}

	@Test
	public void testExecuteActionWithOtherIdentificationType() throws Exception // NOSONAR
	{
		final OrderProcessModel orderProcessModel = new OrderProcessModel();

		final OrderModel order = new OrderModel();
		orderProcessModel.setOrder(order);
		order.setIdentificationType(IdentificationType.LEGAL_IDENTIFICATION);

		identifyUserAction.execute(orderProcessModel);

		Assertions.assertThat(order.getStatus()).isEqualTo(OrderStatus.WAIT_USER_IDENTIFICATION);
	}

	@Test
	public void testExecuteActionWithOrderNull() throws Exception // NOSONAR
	{
		final OrderProcessModel orderProcessModel = new OrderProcessModel();

		orderProcessModel.setOrder(null);

		identifyUserAction.execute(orderProcessModel);

		Assert.assertNull(orderProcessModel.getOrder());
		Assertions.assertThat(identifyUserAction.executeAction(orderProcessModel)).isEqualTo(IdentifyUserAction.Transition.NOK);
	}
}
