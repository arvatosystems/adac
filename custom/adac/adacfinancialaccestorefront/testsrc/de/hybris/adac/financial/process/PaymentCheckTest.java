/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.process.actions.PaymentCheckAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.Configuration;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.BDDMockito.given;


/**
 * Tests for the {@link PaymentCheckAction}.
 */
@UnitTest
public class PaymentCheckTest
{
	private String property;

	@Mock
	private OrderProcessModel process;
	@Mock
	private OrderModel order;
	@Mock
	private BaseSiteModel site;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	@InjectMocks
	private PaymentCheckAction paymentCheckAction;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		String insuranceSite = "insurance";
		Mockito.when(process.getOrder()).thenReturn(order);
		Mockito.when(site.getUid()).thenReturn(insuranceSite);
		Mockito.when(order.getSite()).thenReturn(site);

		property = AdacfinancialaccestorefrontConstants.FINANCIAL_STOREFRONT + ".insurance." +
				AdacfinancialaccestorefrontConstants.PAYMENT_ENABLED;
		given(configurationService.getConfiguration()).willReturn(configuration);
	}

	@Test
	public void testPaymentOK() throws Exception // NOSONAR
	{
		given(configuration.getBoolean(property, false)).willReturn(Boolean.TRUE);
		Assertions.assertThat(paymentCheckAction.executeAction(process)).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);
	}

	@Test
	public void testPaymentNOK() throws Exception // NOSONAR
	{
		given(configuration.getBoolean(property, false)).willReturn(Boolean.FALSE);
		Assertions.assertThat(paymentCheckAction.executeAction(process)).isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);
	}
}
