/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.financialservices.events.UserRejectedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.process.actions.SendRejectionEmailAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Tests for the {@link SendRejectionEmailAction}.
 */
@UnitTest
public class SendRejectionEmailTest
{
	@Mock
	private EventService eventService;
	@InjectMocks
	private SendRejectionEmailAction sendRejectionEmailAction;

	private BaseMatcher<UserRejectedEvent> matcherFactory(final OrderProcessModel process)
	{
		return new BaseMatcher<UserRejectedEvent>()
		{
			@Override
			public boolean matches(final Object item)
			{
				if (item instanceof UserRejectedEvent)
				{
					final UserRejectedEvent event = (UserRejectedEvent) item;
					return event.getProcess().equals(process);
				}
				return false;
			}

			@Override
			public void describeTo(final Description description)
			{
				//nothing to do here
			}
		};

	}

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * @throws Exception Test will be successful if {@link de.hybris.platform.financialservices.events.UserRejectedEvent} is
	 *                   published when order is not null
	 */
	@Test
	public void testExecuteActionIfEventIsPublishedWithOrder() throws Exception // NOSONAR
	{
		final OrderProcessModel process = new OrderProcessModel();
		process.setOrder(new OrderModel());
		sendRejectionEmailAction.executeAction(process);

		Mockito.verify(eventService).publishEvent(Mockito.argThat(matcherFactory(process)));
	}

	/**
	 * @throws Exception Test will be successful if {@link de.hybris.platform.financialservices.events.UserRejectedEvent} is not
	 *                   published because order is null
	 */
	@Test
	public void testExecuteActionIfEventIsNotPublishedWithNullOrder() throws Exception // NOSONAR
	{
		final OrderProcessModel process = new OrderProcessModel();
		sendRejectionEmailAction.executeAction(process);

		Mockito.verify(eventService, Mockito.never()).publishEvent(Mockito.argThat(matcherFactory(process)));
	}
}
