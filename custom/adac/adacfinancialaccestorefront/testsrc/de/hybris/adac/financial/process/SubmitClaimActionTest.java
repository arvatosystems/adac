/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.financialservices.model.process.ClaimProcessModel;
import de.hybris.platform.financialfacades.facades.FSClaimSubmitFacade;
import de.hybris.platform.financialservices.model.FSClaimModel;
import de.hybris.platform.process.actions.SubmitClaimAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.Mockito.when;


/**
 * Tests for the {@link SubmitClaimAction}.
 */
@UnitTest
public class SubmitClaimActionTest
{
	private static final String CLAIM_NUMBER = "claimNumber";

	@Mock
	private ClaimProcessModel claimProcessModel;
	@Mock
	private CatalogVersionModel catalogVersion;
	@Mock
	private FSClaimSubmitFacade fsClaimSubmitFacade;
	@Mock
	CatalogVersionService catalogVersionService;
	@Mock
	private FSClaimModel claimModel;
	@InjectMocks
	private SubmitClaimAction submitClaimAction;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSubmitNullClaim()
	{
		when(claimProcessModel.getClaim()).thenReturn(null);
		Assertions.assertThat(submitClaimAction.executeAction(claimProcessModel))
				.isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);
	}

	@Test
	public void testSubmitClaim()
	{
		claimProcessModel.setClaim(claimModel);
		when(claimProcessModel.getClaim()).thenReturn(claimModel);
		when(claimProcessModel.getClaim().getClaimNumber()).thenReturn(CLAIM_NUMBER);

		Assertions.assertThat(submitClaimAction.executeAction(claimProcessModel))
				.isEqualTo(AbstractSimpleDecisionAction.Transition.OK);
	}
}
