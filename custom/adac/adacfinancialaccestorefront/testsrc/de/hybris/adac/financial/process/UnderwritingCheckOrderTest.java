/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.constants.AdacfinancialaccestorefrontConstants;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.financialservices.constants.FinancialservicesConstants;
import de.hybris.platform.financialservices.services.impl.AbstractFSUnderwritingService;
import de.hybris.platform.financialservices.services.impl.MockIncomeAndExpensesFSUnderwritingService;
import de.hybris.platform.financialservices.services.impl.MockIncomeLimitFSUnderwritingService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.process.actions.SimpleUnderwritingCheckOrderAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.xyformsservices.model.YFormDataModel;
import org.apache.commons.configuration.Configuration;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;


@UnitTest
public class UnderwritingCheckOrderTest
{
	@InjectMocks
	private final SimpleUnderwritingCheckOrderAction underwritingCheckOrderAction = new SimpleUnderwritingCheckOrderAction();
	@InjectMocks
	private Map<String, List<AbstractFSUnderwritingService>> underwritingServices = new HashMap<>();
	@InjectMocks
	private MockIncomeLimitFSUnderwritingService incomeLimitUnderwritingService = new MockIncomeLimitFSUnderwritingService();
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	@Mock
	private ModelService modelService;
	@InjectMocks
	private MockIncomeAndExpensesFSUnderwritingService incomeAndExpensesUnderwritingService = new MockIncomeAndExpensesFSUnderwritingService();
	@Mock
	private AbstractOrderEntryModel orderEntry;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(configuration.getDouble(FinancialservicesConstants.INCOME_LIMIT, FinancialservicesConstants.MAX_INCOME_LIMIT))
				.thenReturn(
						(double) 500);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		incomeLimitUnderwritingService.setConfigurationService(configurationService);
		underwritingServices.put(AdacfinancialaccestorefrontConstants.PREMIUM_CURRENT_ACCOUNT,
				Collections.singletonList(incomeLimitUnderwritingService));
		underwritingServices.put(AdacfinancialaccestorefrontConstants.PERSONAL_LOAN,
				Collections.singletonList(incomeAndExpensesUnderwritingService));
		underwritingCheckOrderAction.setModelService(modelService);
		underwritingCheckOrderAction.setUnderwritingServices(underwritingServices);
	}

	@Test
	public void testCurrentAccountUnderwritingApplicationRejected()
	{
		double netIncomeAmount = 2000;
		String incomeFrequency = "yearly";
		String productCode = AdacfinancialaccestorefrontConstants.PREMIUM_CURRENT_ACCOUNT;
		String formContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<form xmlns:fr=\"http://orbeon.org/oxf/xml/form-runner\" fr:data-format-version=\"4.0.0\">"
				+ "   <number-of-applicants>1</number-of-applicants>"
				+ "   <personal-details>"
				+ "      <title>Mr</title>"
				+ "      <first-name>Test</first-name>"
				+ "      <last-name>Test</last-name>"
				+ "      <date-of-birth>1990-10-03</date-of-birth>"
				+ "      <marital-status>single</marital-status>"
				+ "      <number-of-financial-dependents>0</number-of-financial-dependents>"
				+ "      <resident-of-banks-country>true</resident-of-banks-country>"
				+ "      <us-citizen>false</us-citizen>"
				+ "   </personal-details>"
				+ "   <location-information>"
				+ "      <residential-status>renting</residential-status>"
				+ "      <residential-address>address</residential-address>"
				+ "      <when-did-you-move-to-this-address>2017-10-01</when-did-you-move-to-this-address>"
				+ "      <postal-address-same-as-residential>true</postal-address-same-as-residential>"
				+ "      <postal-address fr:relevant=\"false\" />"
				+ "   </location-information>"
				+ "   <employment-information>"
				+ "      <employment-status>full-time</employment-status>"
				+ "      <employers-name>name</employers-name>"
				+ "      <job-title>title</job-title>"
				+ "      <when-did-you-start-employment>2000-05-10</when-did-you-start-employment>"
				+ "   </employment-information>"
				+ "    <earnings>"
				+ "        <income-frequency>" + incomeFrequency + "</income-frequency>"
				+ "        <net-income-amount>" + netIncomeAmount + "</net-income-amount>"
				+ "    </earnings>"
				+ "</form>";

		OrderModel order = new OrderModel();
		order.setStatus(OrderStatus.USER_VERIFIED);
		ProductModel product = new ProductModel();
		product.setCode(productCode);

		given(orderEntry.getProduct()).willReturn(product);
		YFormDataModel yFormData = new YFormDataModel();
		yFormData.setContent(formContent);

		order.setEntries(Collections.singletonList(orderEntry));

		given(orderEntry.getYFormData()).willReturn(Collections.singletonList(yFormData));

		final OrderProcessModel orderProcessModel = new OrderProcessModel();
		orderProcessModel.setOrder(order);

		underwritingCheckOrderAction.executeAction(orderProcessModel);

		Assertions.assertThat(orderProcessModel.getOrder().getStatus()).isEqualTo(OrderStatus.APPLICATION_REJECTED);

		Assertions.assertThat(underwritingCheckOrderAction.executeAction(orderProcessModel)).isEqualTo(
				AbstractSimpleDecisionAction.Transition.NOK);
	}

	@Test
	public void testCurrentAccountUnderwritingApplicationOk()
	{
		double netIncomeAmount = 7000;
		String incomeFrequency = "yearly";
		String productCode = AdacfinancialaccestorefrontConstants.PREMIUM_CURRENT_ACCOUNT;
		String formContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<form xmlns:fr=\"http://orbeon.org/oxf/xml/form-runner\" fr:data-format-version=\"4.0.0\">"
				+ "   <number-of-applicants>1</number-of-applicants>"
				+ "   <personal-details>"
				+ "      <title>Mr</title>"
				+ "      <first-name>Test</first-name>"
				+ "      <last-name>Test</last-name>"
				+ "      <date-of-birth>1990-10-03</date-of-birth>"
				+ "      <marital-status>single</marital-status>"
				+ "      <number-of-financial-dependents>0</number-of-financial-dependents>"
				+ "      <resident-of-banks-country>true</resident-of-banks-country>"
				+ "      <us-citizen>false</us-citizen>"
				+ "   </personal-details>"
				+ "   <location-information>"
				+ "      <residential-status>renting</residential-status>"
				+ "      <residential-address>address</residential-address>"
				+ "      <when-did-you-move-to-this-address>2017-10-01</when-did-you-move-to-this-address>"
				+ "      <postal-address-same-as-residential>true</postal-address-same-as-residential>"
				+ "      <postal-address fr:relevant=\"false\" />"
				+ "   </location-information>"
				+ "   <employment-information>"
				+ "      <employment-status>full-time</employment-status>"
				+ "      <employers-name>name</employers-name>"
				+ "      <job-title>title</job-title>"
				+ "      <when-did-you-start-employment>2000-05-10</when-did-you-start-employment>"
				+ "   </employment-information>"
				+ "    <earnings>"
				+ "        <income-frequency>" + incomeFrequency + "</income-frequency>"
				+ "        <net-income-amount>" + netIncomeAmount + "</net-income-amount>"
				+ "    </earnings>"
				+ "</form>";

		OrderModel order = new OrderModel();
		order.setStatus(OrderStatus.USER_VERIFIED);
		ProductModel product = new ProductModel();
		product.setCode(productCode);

		given(orderEntry.getProduct()).willReturn(product);
		YFormDataModel yFormData = new YFormDataModel();
		yFormData.setContent(formContent);

		order.setEntries(Collections.singletonList(orderEntry));

		given(orderEntry.getYFormData()).willReturn(Collections.singletonList(yFormData));

		final OrderProcessModel orderProcessModel = new OrderProcessModel();
		orderProcessModel.setOrder(order);

		underwritingCheckOrderAction.executeAction(orderProcessModel);

		Assertions.assertThat(orderProcessModel.getOrder().getStatus()).isNotEqualTo(OrderStatus.APPLICATION_REJECTED);

		Assertions.assertThat(underwritingCheckOrderAction.executeAction(orderProcessModel)).isEqualTo(
				AbstractSimpleDecisionAction.Transition.OK);
	}

	@Test
	public void testPersonalLoanUnderwritingApplicationRejected()
	{
		double netIncomeAmount = 100;
		double totalExpenses = 600;
		String incomeFrequency = "weekly";
		String productCode = AdacfinancialaccestorefrontConstants.PERSONAL_LOAN;
		String formContent = "<?xml version =\"1.0\" encoding=\"UTF-8\"?>"
				+ "<form xmlns:fr=\"http://orbeon.org/oxf/xml/form-runner\" fr:data-format-version=\"4.0.0\">"
				+ "   <number-of-applicants>1</number-of-applicants>"
				+ "   <personal-details>"
				+ "      <title>Mr</title>"
				+ "      <first-name>Test</first-name>"
				+ "      <last-name>Test</last-name>"
				+ "      <date-of-birth>1980-03-05</date-of-birth>"
				+ "      <marital-status>separated</marital-status>"
				+ "      <number-of-financial-dependants>3</number-of-financial-dependants>"
				+ "      <resident-of-banks-country>true</resident-of-banks-country>"
				+ "      <us-citizen>false</us-citizen>"
				+ "   </personal-details>"
				+ "   <location-information>"
				+ "      <residential-status>renting</residential-status>"
				+ "      <residential-address>address</residential-address>"
				+ "      <when-did-you-move-to-this-address>2000-12-13</when-did-you-move-to-this-address>"
				+ "      <postal-address-same-as-residential>true</postal-address-same-as-residential>"
				+ "      <postal-address fr:relevant=\"false\" />"
				+ "   </location-information>"
				+ "   <employment-information>"
				+ "      <employment-status>full-time</employment-status>"
				+ "      <employers-name>name</employers-name>"
				+ "      <job-title>title</job-title>"
				+ "      <when-did-you-start-employment>2017-10-01</when-did-you-start-employment>"
				+ "   </employment-information>"
				+ "   <earnings>"
				+ "      <income-frequency>" + incomeFrequency + "</income-frequency>"
				+ "      <net-income-amount>" + netIncomeAmount + "</net-income-amount>"
				+ "      <any-other-income>false</any-other-income>"
				+ "      <type-of-income fr:relevant=\"false\" />"
				+ "      <second-job-income-frequency fr:relevant=\"false\">monthly</second-job-income-frequency>"
				+ "      <second-job-net-income-amount fr:relevant=\"false\" />"
				+ "      <working-overtime-income-frequency fr:relevant=\"false\">monthly</working-overtime-income-frequency>"
				+ "      <working-overtime-net-income-amount fr:relevant=\"false\" />"
				+ "      <other-income-income-frequency fr:relevant=\"false\">monthly</other-income-income-frequency>"
				+ "      <other-income-net-income-amount fr:relevant=\"false\" />"
				+ "      <financial-commitments>false</financial-commitments>"
				+ "      <financial-commitments-text-area fr:relevant=\"false\" />"
				+ "      <financial-obstacles>false</financial-obstacles>"
				+ "      <financial-obstacles-text-area fr:relevant=\"false\" />"
				+ "   </earnings>"
				+ "   <debts>"
				+ "      <any-debts>false</any-debts>"
				+ "      <your-debts fr:relevant=\"false\" />"
				+ "      <another-loan fr:relevant=\"false\" />"
				+ "      <another-loan-original-amount-1 fr:relevant=\"false\" />"
				+ "      <another-loan-remaining-debt-1 fr:relevant=\"false\" />"
				+ "      <another-loan-original-amount-2 fr:relevant=\"false\" />"
				+ "      <another-loan-remaining-debt-2 fr:relevant=\"false\" />"
				+ "      <another-loan-original-amount-3 fr:relevant=\"false\" />"
				+ "      <another-loan-remaining-debt-3 fr:relevant=\"false\" />"
				+ "      <another-loan-original-amount-4 fr:relevant=\"false\" />"
				+ "      <another-loan-remaining-debt-4 fr:relevant=\"false\" />"
				+ "      <credit-card fr:relevant=\"false\" />"
				+ "      <credit-card-limit-1 fr:relevant=\"false\" />"
				+ "      <credit-card-remaining-debt-1 fr:relevant=\"false\" />"
				+ "      <credit-card-limit-2 fr:relevant=\"false\" />"
				+ "      <credit-card-remaining-debt-2 fr:relevant=\"false\" />"
				+ "      <credit-card-limit-3 fr:relevant=\"false\" />"
				+ "      <credit-card-remaining-debt-3 fr:relevant=\"false\" />"
				+ "      <credit-card-limit-4 fr:relevant=\"false\" />"
				+ "      <credit-card-remaining-debt-4 fr:relevant=\"false\" />"
				+ "      <other-debt fr:relevant=\"false\" />"
				+ "      <other-debt-type-1 fr:relevant=\"false\" />"
				+ "      <other-debt-approximate-remaining-amount-1 fr:relevant=\"false\" />"
				+ "      <other-debt-type-2 fr:relevant=\"false\" />"
				+ "      <other-debt-approximate-remaining-amount-2 fr:relevant=\"false\" />"
				+ "      <other-debt-type-3 fr:relevant=\"false\" />"
				+ "      <other-debt-approximate-remaining-amount-3 fr:relevant=\"false\" />"
				+ "      <other-debt-type-4 fr:relevant=\"false\" />"
				+ "      <other-debt-approximate-remaining-amount-4 fr:relevant=\"false\" />"
				+ "      <total-other-monthly-expenses>" + totalExpenses + "</total-other-monthly-expenses>"
				+ "   </debts>"
				+ "</form>";

		OrderModel order = new OrderModel();
		order.setStatus(OrderStatus.USER_VERIFIED);
		ProductModel product = new ProductModel();
		product.setCode(productCode);

		given(orderEntry.getProduct()).willReturn(product);
		YFormDataModel yFormData = new YFormDataModel();
		yFormData.setContent(formContent);

		order.setEntries(Collections.singletonList(orderEntry));

		given(orderEntry.getYFormData()).willReturn(Collections.singletonList(yFormData));

		final OrderProcessModel orderProcessModel = new OrderProcessModel();
		orderProcessModel.setOrder(order);

		underwritingCheckOrderAction.executeAction(orderProcessModel);

		Assertions.assertThat(orderProcessModel.getOrder().getStatus()).isEqualTo(OrderStatus.APPLICATION_REJECTED);

		Assertions.assertThat(underwritingCheckOrderAction.executeAction(orderProcessModel)).isEqualTo(
				AbstractSimpleDecisionAction.Transition.NOK);
	}

	@Test
	public void testPersonalLoanUnderwritingApplicationOk()
	{
		double netIncomeAmount = 400;
		double totalExpenses = 600;
		String incomeFrequency = "weekly";
		String productCode = AdacfinancialaccestorefrontConstants.PERSONAL_LOAN;
		String formContent = "<?xml version =\"1.0\" encoding=\"UTF-8\"?>"
				+ "<form xmlns:fr=\"http://orbeon.org/oxf/xml/form-runner\" fr:data-format-version=\"4.0.0\">"
				+ "   <number-of-applicants>1</number-of-applicants>"
				+ "   <personal-details>"
				+ "      <title>Mr</title>"
				+ "      <first-name>Test</first-name>"
				+ "      <last-name>Test</last-name>"
				+ "      <date-of-birth>1980-03-05</date-of-birth>"
				+ "      <marital-status>separated</marital-status>"
				+ "      <number-of-financial-dependants>3</number-of-financial-dependants>"
				+ "      <resident-of-banks-country>true</resident-of-banks-country>"
				+ "      <us-citizen>false</us-citizen>"
				+ "   </personal-details>"
				+ "   <location-information>"
				+ "      <residential-status>renting</residential-status>"
				+ "      <residential-address>address</residential-address>"
				+ "      <when-did-you-move-to-this-address>2000-12-13</when-did-you-move-to-this-address>"
				+ "      <postal-address-same-as-residential>true</postal-address-same-as-residential>"
				+ "      <postal-address fr:relevant=\"false\" />"
				+ "   </location-information>"
				+ "   <employment-information>"
				+ "      <employment-status>full-time</employment-status>"
				+ "      <employers-name>name</employers-name>"
				+ "      <job-title>title</job-title>"
				+ "      <when-did-you-start-employment>2017-10-01</when-did-you-start-employment>"
				+ "   </employment-information>"
				+ "   <earnings>"
				+ "      <income-frequency>" + incomeFrequency + "</income-frequency>"
				+ "      <net-income-amount>" + netIncomeAmount + "</net-income-amount>"
				+ "      <any-other-income>false</any-other-income>"
				+ "      <type-of-income fr:relevant=\"false\" />"
				+ "      <second-job-income-frequency fr:relevant=\"false\">monthly</second-job-income-frequency>"
				+ "      <second-job-net-income-amount fr:relevant=\"false\" />"
				+ "      <working-overtime-income-frequency fr:relevant=\"false\">monthly</working-overtime-income-frequency>"
				+ "      <working-overtime-net-income-amount fr:relevant=\"false\" />"
				+ "      <other-income-income-frequency fr:relevant=\"false\">monthly</other-income-income-frequency>"
				+ "      <other-income-net-income-amount fr:relevant=\"false\" />"
				+ "      <financial-commitments>false</financial-commitments>"
				+ "      <financial-commitments-text-area fr:relevant=\"false\" />"
				+ "      <financial-obstacles>false</financial-obstacles>"
				+ "      <financial-obstacles-text-area fr:relevant=\"false\" />"
				+ "   </earnings>"
				+ "   <debts>"
				+ "      <any-debts>false</any-debts>"
				+ "      <your-debts fr:relevant=\"false\" />"
				+ "      <another-loan fr:relevant=\"false\" />"
				+ "      <another-loan-original-amount-1 fr:relevant=\"false\" />"
				+ "      <another-loan-remaining-debt-1 fr:relevant=\"false\" />"
				+ "      <another-loan-original-amount-2 fr:relevant=\"false\" />"
				+ "      <another-loan-remaining-debt-2 fr:relevant=\"false\" />"
				+ "      <another-loan-original-amount-3 fr:relevant=\"false\" />"
				+ "      <another-loan-remaining-debt-3 fr:relevant=\"false\" />"
				+ "      <another-loan-original-amount-4 fr:relevant=\"false\" />"
				+ "      <another-loan-remaining-debt-4 fr:relevant=\"false\" />"
				+ "      <credit-card fr:relevant=\"false\" />"
				+ "      <credit-card-limit-1 fr:relevant=\"false\" />"
				+ "      <credit-card-remaining-debt-1 fr:relevant=\"false\" />"
				+ "      <credit-card-limit-2 fr:relevant=\"false\" />"
				+ "      <credit-card-remaining-debt-2 fr:relevant=\"false\" />"
				+ "      <credit-card-limit-3 fr:relevant=\"false\" />"
				+ "      <credit-card-remaining-debt-3 fr:relevant=\"false\" />"
				+ "      <credit-card-limit-4 fr:relevant=\"false\" />"
				+ "      <credit-card-remaining-debt-4 fr:relevant=\"false\" />"
				+ "      <other-debt fr:relevant=\"false\" />"
				+ "      <other-debt-type-1 fr:relevant=\"false\" />"
				+ "      <other-debt-approximate-remaining-amount-1 fr:relevant=\"false\" />"
				+ "      <other-debt-type-2 fr:relevant=\"false\" />"
				+ "      <other-debt-approximate-remaining-amount-2 fr:relevant=\"false\" />"
				+ "      <other-debt-type-3 fr:relevant=\"false\" />"
				+ "      <other-debt-approximate-remaining-amount-3 fr:relevant=\"false\" />"
				+ "      <other-debt-type-4 fr:relevant=\"false\" />"
				+ "      <other-debt-approximate-remaining-amount-4 fr:relevant=\"false\" />"
				+ "      <total-other-monthly-expenses>" + totalExpenses + "</total-other-monthly-expenses>"
				+ "   </debts>"
				+ "</form>";

		OrderModel order = new OrderModel();
		order.setStatus(OrderStatus.USER_VERIFIED);
		ProductModel product = new ProductModel();
		product.setCode(productCode);

		given(orderEntry.getProduct()).willReturn(product);
		YFormDataModel yFormData = new YFormDataModel();
		yFormData.setContent(formContent);

		order.setEntries(Collections.singletonList(orderEntry));

		given(orderEntry.getYFormData()).willReturn(Collections.singletonList(yFormData));

		final OrderProcessModel orderProcessModel = new OrderProcessModel();
		orderProcessModel.setOrder(order);

		underwritingCheckOrderAction.executeAction(orderProcessModel);

		Assertions.assertThat(orderProcessModel.getOrder().getStatus()).isNotEqualTo(OrderStatus.APPLICATION_REJECTED);

		Assertions.assertThat(underwritingCheckOrderAction.executeAction(orderProcessModel)).isEqualTo(
				AbstractSimpleDecisionAction.Transition.OK);
	}
}
