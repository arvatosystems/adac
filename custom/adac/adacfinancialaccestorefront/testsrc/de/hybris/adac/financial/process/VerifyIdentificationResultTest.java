/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.adac.financial.process;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.process.actions.VerifyIdentificationResultAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;


/**
 * Tests for the {@link VerifyIdentificationResultAction}.
 */
@UnitTest
public class VerifyIdentificationResultTest
{
	@InjectMocks
	private VerifyIdentificationResultAction verifyIdentificationResultAction;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testExecuteActionWithNullOrderModel()
	{
		final OrderProcessModel orderProcessModel = new OrderProcessModel();

		Assertions.assertThat(verifyIdentificationResultAction.executeAction(orderProcessModel)).isEqualTo(
				AbstractSimpleDecisionAction.Transition.NOK);
	}

	@Test
	public void testExecuteActionWithOrderModelAndVerifiedStatus()
	{
		final OrderProcessModel orderProcessModel = new OrderProcessModel();
		final OrderModel orderModel = new OrderModel();
		orderModel.setStatus(OrderStatus.USER_VERIFIED);
		orderProcessModel.setOrder(orderModel);

		Assertions.assertThat(verifyIdentificationResultAction.executeAction(orderProcessModel)).isEqualTo(
				AbstractSimpleDecisionAction.Transition.OK);
	}

	@Test
	public void testExecuteActionWithOrderModelAndRejectedStatus()
	{
		final OrderProcessModel orderProcessModel = new OrderProcessModel();
		final OrderModel orderModel = new OrderModel();
		orderModel.setStatus(OrderStatus.USER_REJECTED);
		orderProcessModel.setOrder(orderModel);

		Assertions.assertThat(verifyIdentificationResultAction.executeAction(orderProcessModel)).isEqualTo(
				AbstractSimpleDecisionAction.Transition.NOK);
	}
}
