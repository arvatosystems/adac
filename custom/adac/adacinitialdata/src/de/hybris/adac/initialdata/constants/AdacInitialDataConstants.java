/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.adac.initialdata.constants;

/**
 * Global class for all AdacInitialData constants.
 */
public final class AdacInitialDataConstants extends GeneratedAdacInitialDataConstants
{
	public static final String EXTENSIONNAME = "adacinitialdata";

	private AdacInitialDataConstants()
	{
		//empty
	}
}
