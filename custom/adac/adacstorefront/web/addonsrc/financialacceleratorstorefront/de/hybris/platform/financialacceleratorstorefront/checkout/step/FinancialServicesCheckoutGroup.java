/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialacceleratorstorefront.checkout.step;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutGroup;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;


public class FinancialServicesCheckoutGroup extends CheckoutGroup
{
	private Map<String, FinancialServicesCheckoutStep> financialServicesCheckoutProgressBar;

	public Map<String, FinancialServicesCheckoutStep> getFinancialServicesCheckoutProgressBar()
	{
		return financialServicesCheckoutProgressBar;
	}

	@Required
	public void setFinancialServicesCheckoutProgressBar(
			final Map<String, FinancialServicesCheckoutStep> financialServicesCheckoutProgressBar)
	{
		this.financialServicesCheckoutProgressBar = financialServicesCheckoutProgressBar;
	}
}
