/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialacceleratorstorefront.controllers.pages;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.financialacceleratorstorefront.controllers.imported.AcceleratorCategoryPageController;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.annotation.Resource;


/**
 * Category Page Controller
 */
public class CategoryPageController extends AcceleratorCategoryPageController
{

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@ModelAttribute("cartData")
	public CartData getCartData()
	{
		return cartFacade.getSessionCart();
	}

}
