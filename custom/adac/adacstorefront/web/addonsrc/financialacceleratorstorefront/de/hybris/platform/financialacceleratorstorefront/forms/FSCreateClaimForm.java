/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialacceleratorstorefront.forms;

public class FSCreateClaimForm
{
	private String policyId;
	private boolean confirmation;

	public String getPolicyId()
	{
		return policyId;
	}

	public void setPolicyId(String policyId)
	{
		this.policyId = policyId;
	}

	public boolean getConfirmation()
	{
		return confirmation;
	}

	public void setConfirmation(boolean confirmation)
	{
		this.confirmation = confirmation;
	}

}
