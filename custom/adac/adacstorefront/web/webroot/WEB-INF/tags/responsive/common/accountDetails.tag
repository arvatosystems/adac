<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<c:url value="/my-account/payment-details-popup" var="bankAccountData" />
<form:form id="bankAccountForm" name="bankAccountForm" action="${bankAccountData}" method="get" commandName="bankAccountForm">
	<div class="popup-container">
		<header class="popup-header">
			<a href="" class="b-close"></a>
			<h2 class="popup-title">
				<spring:theme code="Ihre Bankverbindung" />
			</h2>
		</header>

		<div class="popup-body">
			<div class="form-grid center">
				<div class="ll-row ll-row--first">
					<div class="popContent">
						<div class="ll-error-msg"></div>
						<div class="ll-check"></div>
						<p><spring:theme code="add.payment.method.paragraph.one" /></p>
						<p><strong><spring:theme code="add.payment.method.tip" />:</strong><br>
							<spring:theme code="add.payment.method.paragraph.two" /></p>
						<p style="margin-bottom: 15px;"><spring:theme code="add.payment.method.paragraph.three" /></p>
					</div>
				</div>

			</div>
		</div>
		<div class="h-space-m"></div>
		<div class="addPaymentUserInput">
					<div class="aditionalInformation">
						<div class="form-row">
							<label class="row-label" for="bankdaten.Iban"><spring:theme code="payment.iban" /></label>
								<div class="form-group">
								<form:input path="iban" type="text" name="bankdaten.Iban" id="bankdaten.Iban" placeholder="IBAN" />
								</div>
							<label class="row-label full" for="TODO11"><spring:theme code="add.payment.method.alternative.owner" /></label>
						</div>
						
						<div class="form-group">
<!-- 								<input type="radio" id="paymentCase_1" class="js-show" name="alternativeAccountHolder" value="true"> -->
							<form:radiobutton id="paymentCase_1" class="js-show" path="alternativeAccountHolder" name="alternativeAccountHolder" value="true" />
							<label for="paymentCase_1" class="js-show"><spring:theme code="text.quote.yes.button.label" /></label>
						</div>
						<div class="form-group">
<!-- 								<input type="radio" id="paymentCase_2" class="js-hide" name="alternativeAccountHolder" value="false" checked="checked"> -->
							<form:radiobutton id="paymentCase_2" class="js-show" path="alternativeAccountHolder" name="alternativeAccountHolder" value="false" checked="checked" />
							<label for="paymentCase_2" class="js-hide"><spring:theme code="text.quote.no.button.label" /></label>
						</div>

						<div class="accountHolder">
							<div class="form-row">
								<label class="row-label full" for="TODO16"><spring:theme code="add.payment.method.other" /></label>
	
								<div class="form-group half">
									<form:radiobutton id="bankDataGender_2" path="gender" name="gender" data-validation="Anrede"  value="FEMALE"/>
									<label for="bankDataGender_2"><spring:theme code="gender.female" /></label>
								</div>
								
								<div class="form-group half">
									<form:radiobutton id="bankDataGender_1" path="gender" name="gender" data-validation="Anrede" class="is-first-radio" value="MALE"/>
									<label for="bankDataGender_1"><spring:theme code="gender.male"/></label>
								</div>
								
							</div>
							
						
							<div class="form-row">
									<label class="row-label" for="Bankdaten.AbwKontoInhaber.Vorname">
										<spring:theme code="address.firstName" />
									</label>
								<div class="form-group">
										<form:input path="firstName" data-t-name="BasicInputText" type="text" id="Bankdaten.AbwKontoInhaber.Vorname" name="Bankdaten.AbwKontoInhaber.Vorname"
										 value="" data-validation="Vorname" data-t-id="48"/>
								</div>
							</div>
							<div class="form-row">
									<label class="row-label" for="Bankdaten.AbwKontoInhaber.Name">
										<spring:theme code="address.surname" />
									</label>
								<div class="form-group">
										<form:input path="lastName" data-t-name="BasicInputText" type="text" id="Bankdaten.AbwKontoInhaber.Name" name="Bankdaten.AbwKontoInhaber.Name"
										 value="" data-validation="Nachname" data-t-id="49"/>
								</div>
							</div>
							<div class="form-row">
									<label class="row-label" for="Bankdaten.AbwKontoInhaber.Adresse.Ergaenzungszeile">
										<spring:theme code="add.address" />
									</label>
								<div class="form-group">
										<form:input path="line1" data-t-name="BasicInputText" type="text" id="Bankdaten.AbwKontoInhaber.Adresse.Ergaenzungszeile" name="Bankdaten.AbwKontoInhaber.Adresse.Ergaenzungszeile"
										 value="" placeholder="z.B. bei Mustermann" pattern="[a-zA-ZàáâäãåacceèéêëeiìíîïlnòóôöõøùúûüuuÿýzzñçcšžÀÁÂÄÃÅACCEEÈÉÊËÌÍÎÏILNÒÓÔÖÕØÙÚÛÜUUŸÝZZÑßÇŒÆCŠŽ?ð ,.'-|]+"
										 data-t-id="50"/>
								</div>
							</div>
							<div class="form-row">
									<label class="row-label" for="Bankdaten.AbwKontoInhaber.Adresse.StrasseHausnummer">
										<spring:theme code="address.str.number" />
									</label>
								<div class="form-group">
										<form:input path="line2" data-t-name="BasicInputText" type="text" id="Bankdaten.AbwKontoInhaber.Adresse.StrasseHausnummer" name="Bankdaten.AbwKontoInhaber.Adresse.StrasseHausnummer"
										 value="" data-validation="StrasseHausnummer" data-t-id="51"/>
								</div>
							</div>
							<div class="form-row">
									<label class="row-label" for="Bankdaten.AbwKontoInhaber.Adresse.Postleitzahl">
										<spring:theme code="address.plz.ort" />
									</label>
								<div class="form-group">
									<div class="pInput">
										<form:input path="postcode" data-t-name="BasicInputText" type="text" id="Bankdaten.AbwKontoInhaber.Adresse.Postleitzahl" name="Bankdaten.AbwKontoInhaber.Adresse.Postleitzahl"
										 value="" data-validation="Postleitzahl" pattern="[0-9]+" data-t-id="52"/>
									</div>
									<div class="pInput">
										<form:input path="townCity" data-t-name="BasicInputText" class="cstm" type="text" id="Bankdaten.AbwKontoInhaber.Adresse.Ort" name="Bankdaten.AbwKontoInhaber.Adresse.Ort"
										 value="" data-validation="Ort" pattern="[a-zA-ZäöüÄÖÜßéèêóòúù\-\/\.\s()]+" data-t-id="53"/>
									</div>
								</div>
							</div>
						</div>
			</div>
		</div>

		<div class="actionButtons">
			<a href="javascript:;" data-t-name="BasicBtn" class="btn-default js-mini-cart-close-button"><spring:theme code="basket.save.cart.action.cancel" /></a>

			<button type="submit" class="btn-primary">
				<span class="js-rate-not-zero"><spring:theme code="basket.save.cart.action.save" /></span>
			</button>

		</div>
	</div>
</form:form>

<script type="text/javascript">

function handleClick(clickedId)
{
   if(clickedId == "paymentCase_1")
     document.getElementById('paymentCase_1').value = "true";
   else
     document.getElementById('paymentCase_2').value = "false";
}

</script>
