<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/product" %>
<%@ taglib prefix="financialCart" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/cart" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<product:productComparison columns="4" initialRowsCount="${initialRowsCount}" searchPageData="${searchPageData}" hideOptionProducts="true" addToCartBtn_label_key="basket.add.to.basket.select" />

<financialCart:changePlanConfirmPopup confirmActionButtonId="addNewPlanConfirmButton" cartData="${cartData}"/>
