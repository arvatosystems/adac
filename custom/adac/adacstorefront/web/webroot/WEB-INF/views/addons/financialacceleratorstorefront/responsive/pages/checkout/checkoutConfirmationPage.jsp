<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="insuranceCheckout" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/checkout" %>
<%@ taglib prefix="insurancOrder" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/order" %>
<%@ taglib prefix="policy" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/checkout/policy" %>

<template:page pageTitle="${pageTitle}">
    <div class="boxed-content-wrapper">
        <div class="col-md-12 checkout-confirmation">
            <ycommerce:testId code="orderConfirmation_yourOrderResults_text">
                <div class="checkout-confirmation__covered"><spring:theme code="checkout.orderConfirmation.youAreCovered" /></div>
                <div class="checkout-confirmation__copy"><spring:theme code="checkout.orderConfirmation.saveToAccount"/></div>
            </ycommerce:testId>
        </div>
        <div class="col-md-8 checkout-confirmation checkout-confirmation__wrapper accordion">
            <div class="col-md-12 accordion-header section-header__heading section-header__heading--lean">
                <div class="row">
                    <c:set var="images" value="${orderData.entries[0].product.images}"></c:set>
                    <c:forEach items="${orderData.insurancePolicyResponses}" var="policyResponse">
                        <insuranceCheckout:policySummaryLine policyResponse="${policyResponse}" images="${images}" />
                    </c:forEach>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="checkoutContentPanel" class="accordion">
                <c:forEach items="${orderData.insurancePolicy}" var="insurancePolicy">
                    <div class="accordion-item clearfix">
                        <c:set var="openWhatsIncludedSection" value="true"/>
                        <c:if test="${orderData.insuranceQuote.quoteType ne 'EVENT'}">
                            <c:set var="openWhatsIncludedSection" value="false"/>
                            <div class="col-md-12 col-sm-12 col-xs-12 accordion-item__wrapper js-quotePlanInfoSection">
                                <policy:policyPlanInfoSection orderData="${orderData}"/>
                            </div>
                        </c:if>
                        <div class="col-md-12 col-sm-12 col-xs-12 accordion-item__wrapper">
                            <policy:policyWhatsIncluded policyData="${insurancePolicy}" openSection="${openWhatsIncludedSection}"/>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 accordion-item__wrapper">
                            <policy:policyOptionalExtras policyData="${insurancePolicy}"/>
                        </div>
                        <c:if test="${insurancePolicy.mainProduct.coverageProduct.investments.size() > 0}">
                            <div class="col-md-12 col-sm-12 col-xs-12 accordion-item__wrapper">
                                <policy:policyInvestments policyData="${insurancePolicy}"/>
                            </div>
                        </c:if>
                        <div class="col-md-12 col-sm-12 col-xs-12 accordion-item__wrapper">
                            <policy:policyDetails policyData="${insurancePolicy}"/>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="col-md-4">
            <insurancOrder:orderDetailsItem orderData="${orderData}"/>
            <cms:pageSlot position="SideContent" var="feature" element="div" class="span-24 side-content-slot cms_disp-img_slot">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
        </div>
    </div>
</template:page>
